package com.noe.to.web.backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


import com.noe.to.web.backend.database.DatabaseConnect;

@SpringBootTest
public class DatabaseConnectTest {

	@Test
	public void DatabaseTest() {
		
		DatabaseConnect dbConnect = new DatabaseConnect();
		
		String url = "jdbc:sqlserver://";
		String serverName = "noetoserver.database.windows.net";
		String portNumber = "1433";
		String dbName = "noetoDB";
		String userName = "ebase@noetoserver";
		String pwd = "Elite2021";
			
			
		String connectionUrl = url + serverName + ":" + portNumber
				+ ";database=" + dbName + ";user=" + userName + ";password=" + pwd +";"
				+ "encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database."
				+ "windows.net;loginTimeout=30;";
		
		Connection con = null;
		String query = "Select name From Hotels";
		
		
		
		try {
				con = DriverManager.getConnection(connectionUrl);
				Statement select = con.createStatement();
		        ResultSet result = select.executeQuery(query);
		        
				System.out.println("Connecting to SQL Server...");
				System.out.println(con);
				System.out.println(query);
				
				
				
				while (result.next()) {
					
					System.out.println(result.getString("name"));
							
				}
				
				System.out.println("Done");
				
		} catch (SQLException e) {
				System.err.println(e.getMessage());
			}

			
	}
}
