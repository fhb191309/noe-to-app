package com.noe.to.web.frontend;

import java.sql.SQLException;

import org.bouncycastle.operator.AADProcessor;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.html5.AddApplicationCache;
import org.springframework.boot.test.context.SpringBootTest;

import com.noe.to.web.backend.entity.MasterDataOverview;
import com.noe.to.web.backend.service.MasterDataService;
import com.noe.to.web.frontend.view.MasterData;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.testbench.TestBenchTestCase;
import com.vaadin.testbench.IPAddress;

@SpringBootTest
public class HotelOverviewTestIT extends AbstractIT{

//	DatabaseConnect dbConnect = new DatabaseConnect();
//	
//	String url = "jdbc:sqlserver://";
//	String serverName = "noetoserver.database.windows.net";
//	String portNumber = "1433";
//	String dbName = "noetoDB";
//	String userName = "ebase@noetoserver";
//	String pwd = "Elite2021";
//		
//		
//	String connectionUrl = url + serverName + ":" + portNumber
//			+ ";database=" + dbName + ";user=" + userName + ";password=" + pwd +";"
//			+ "encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database."
//			+ "windows.net;loginTimeout=30;";
//	
//	Connection con = null;
//	String query = "Select name From Hotels";
	
	MasterData masterdata;
	
	Grid<MasterDataOverview> masterDataOverviewGrid;
	MasterDataService masterDataService = new MasterDataService();
	
	
	
	@Test
	private  void configureHotelOverviewGrid() throws SQLException {
    	
        masterDataOverviewGrid.setItems(masterDataService.getMasterDataOverview());
        masterDataOverviewGrid.setColumns( "kategorie", "azBetriebe", "azZimmer", "azBetten", "kinderfreundlich", 
        		"hundefreundlich", "spa", "fitness");
        masterDataOverviewGrid.getColumns().forEach(col -> col.setAutoWidth(true));
        
  
    }
	
	
	@After
	public void tearDown() throws Exception {
		// close the browser instance when all tests are done
        getDriver().quit();
	}
}
