package com.noe.to.web;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.noe.to.web.NoeToAppApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = NoeToAppApplication.class)
@org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
public class DataJpaTest {

	@Autowired
	TestEntityManager entityManager;
	
	
}
