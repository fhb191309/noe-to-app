package com.noe.to.web;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration
@SpringBootTest
class NoeToAppApplicationTests {

	@Test
	void contextLoads() {
	}

}
