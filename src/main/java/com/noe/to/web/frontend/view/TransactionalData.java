package com.noe.to.web.frontend.view;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.WeakHashMap;




import com.noe.to.web.backend.entity.Occupancy;
import com.noe.to.web.backend.entity.TransDataOverview;
import com.noe.to.web.backend.service.HotelService;
import com.noe.to.web.backend.service.OccupancyService;
import com.noe.to.web.backend.service.TransDataService;
import com.noe.to.web.frontend.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("TransactionaData") 
@Route(value = "TransactionalData", layout = MainLayout.class)

public class TransactionalData extends VerticalLayout{

	Grid<Occupancy> grid = new Grid<>(Occupancy.class);
	OccupancyService occupancyService = new OccupancyService();
	
	Grid<TransDataOverview> transDataOverviewGrid = new Grid<>(TransDataOverview.class);

	Grid<Occupancy> table = new Grid<>();

	ArrayList<Occupancy> occupancies = OccupancyService.getAllOccupancies();
	
	Binder<Occupancy> binder = new Binder<>(Occupancy.class);
	Editor<Occupancy> editor = grid.getEditor();
	
	Integer i = null;

	boolean change = true;
	/**
	 * Konstrukter der Klasse TransactionalData
	 * fügt alle Komponenten der Seite hinzu
	 * @throws SQLException
	 */
	public TransactionalData() throws SQLException {

		add(getTitle());

		add(allgemein_ue());
		
		add(configureOccupancyOverviewGrid());

		add(hotel_ue());
		
		add(getButtons());
		add(changeButtons());

		
		
		Div content = new Div(grid);
        content.addClassName("content");
        content.setSizeFull();

		getDefaultHorizontalComponentAlignment();

		

	}
	
	private Component changeButtons() throws SQLException {
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		VerticalLayout verticalLayout = new VerticalLayout();

				verticalLayout.removeAll();
				verticalLayout.add(horizontalLayout, configureGrid());
				
		return verticalLayout;
	}

	/**
	 * creates the Header of the Page
	 * @return
	 */
	private Component hotel_ue() {
		
		H2 title = new H2("Hotelübersicht");
		
		return title;
	}


	/**
	 * creates the second header and the backup button
	 * @return
	 */
	private Component getTitle() {
		
		H1 TStamm = new H1("Transaktionsdaten");
		
		
		HorizontalLayout hori = new HorizontalLayout(TStamm);;
        hori.setWidthFull();

        hori.setAlignItems(FlexComponent.Alignment.CENTER);

        return hori;
	}
	/**
	 * creates the header for the overview and the send button for the master data
	 * @return
	 */
	private Component allgemein_ue() {
		H2 allg = new H2("Allgemeine Übersicht");
		
		
		HorizontalLayout hori = new HorizontalLayout(allg);
		        hori.addClassName("h2");
		        hori.setWidthFull();
		        hori.setAlignItems(FlexComponent.Alignment.CENTER);
		        return hori;
		}
	
	/**
	 * creates the Occupancy Overview, 4 ComboBoxes and 2 buttons
	 * has 2 click listeners on the button to change the appearance and meaning of the overview
	 * @return
	 * @throws SQLException
	 */
    private  Component configureOccupancyOverviewGrid() throws SQLException {
    	
    	VerticalLayout verticalLayout = new VerticalLayout();
    	HorizontalLayout horizontalLayout = new HorizontalLayout();
    	
    	
		ComboBox<Integer> idSelect = new ComboBox<>();
		idSelect.setLabel("Hotel ID");
		idSelect.setPlaceholder("Hotel ID");
	
		ArrayList<Integer> ids = HotelService.getHotelID();
		Collections.sort(ids);
		idSelect.setItems(ids);
		
		ComboBox<Integer> monthSelect = new ComboBox<>();
		monthSelect.setItems(OccupancyService.getAllOccupancyMonths());
		monthSelect.setLabel("Monat");

		
		ComboBox<Integer> yearSelect = new ComboBox<>();
		yearSelect.setItems(2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021);
		yearSelect.setLabel("Jahr");
		
		ComboBox<String> katSelect = new ComboBox<>();
		katSelect.setItems("*", "**", "***", "****", "*****");
		katSelect.setLabel("Kategorie");
		
		Button suchenButton = new Button("Suchen");
		suchenButton.setHeight("68px");
		Button resetButton = new Button("Zurücksetzen");
		resetButton.setHeight("68px");
		
		
		
		horizontalLayout.add(idSelect, monthSelect, yearSelect, katSelect, suchenButton, resetButton);
    	
   	 	transDataOverviewGrid.setItems(TransDataService.getTransDataOverview());
   	 	transDataOverviewGrid.setHeightByRows(true);
   	    transDataOverviewGrid.getColumns().forEach(col -> col.setAutoWidth(true));
		resetButton.addClickListener(Event -> {
			
			idSelect.setValue(null);
			monthSelect.setValue(null);
			yearSelect.setValue(null);
			katSelect.setValue(null);
			
			  transDataOverviewGrid.addClassName("overview-grid");
		        try { 
		        	 transDataOverviewGrid.setItems(TransDataService.getTransDataOverview());
		        } catch (SQLException e) {

					e.printStackTrace();
				}
		     
		       
		        transDataOverviewGrid.setColumns("kategorie", "azZimmer", "zimmerauslastung", "azBetten", "bettenauslastung", 
		        		"azNationalitäten", "azGenesen", "azGetestet", "azGeimpft");
		        transDataOverviewGrid.getColumns().forEach(col -> col.setAutoWidth(true));
		        transDataOverviewGrid.setMaxHeight("240px");
			
		});
		
		suchenButton.addClickListener(Event -> {
			
			if(idSelect.isEmpty() &&  monthSelect.isEmpty() && yearSelect.isEmpty()) {
		        transDataOverviewGrid.addClassName("overview-grid");
		        transDataOverviewGrid.getColumns().forEach(col -> col.setAutoWidth(true));
		        try {
					transDataOverviewGrid.setItems(TransDataService.getTransDataOverviewByCategory(katSelect.getValue()));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		        transDataOverviewGrid.setColumns("kategorie", "azZimmer", "zimmerauslastung", "azBetten", "bettenauslastung", 
		        		"azNationalitäten", "azGenesen", "azGetestet", "azGeimpft");
		        transDataOverviewGrid.getColumns().forEach(col -> col.setAutoWidth(true));
		        transDataOverviewGrid.setMaxHeight("130px");
				
		        //--------------------------------------------------------------------
		        
			} else if(katSelect.isEmpty() &&  monthSelect.isEmpty() && yearSelect.isEmpty() ) {
		        transDataOverviewGrid.addClassName("overview-grid");
		        try {
					transDataOverviewGrid.setItems(TransDataService.getTransDataOverviewByID(idSelect.getValue()));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		        transDataOverviewGrid.setColumns("kategorie", "azZimmer", "zimmerauslastung", "azBetten", "bettenauslastung", 
		        		"azNationalitäten", "azGenesen", "azGetestet", "azGeimpft");
		        transDataOverviewGrid.getColumns().forEach(col -> col.setAutoWidth(true));
		        transDataOverviewGrid.setMaxHeight("240px");
				
		        //--------------------------------------------------------------------

		        
			} else if(katSelect.isEmpty() &&  monthSelect.isEmpty()) {
	        transDataOverviewGrid.addClassName("overview-grid");
	        try {
				transDataOverviewGrid.setItems(TransDataService.getTransDataOverviewByHotelAndByYears(idSelect.getValue(), yearSelect.getValue()));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        transDataOverviewGrid.setColumns("kategorie", "azZimmer", "zimmerauslastung", "azBetten", "bettenauslastung", 
	        		"azNationalitäten", "azGenesen", "azGetestet", "azGeimpft");
	        transDataOverviewGrid.getColumns().forEach(col -> col.setAutoWidth(true));
	        transDataOverviewGrid.setMaxHeight("240px");
			
	        //--------------------------------------------------------------------

	        
			} else if(katSelect.isEmpty()) {
	        transDataOverviewGrid.addClassName("overview-grid");
	        try {
				transDataOverviewGrid.setItems(TransDataService.getTransDataOverviewByHotelAndByMonth(idSelect.getValue(), monthSelect.getValue(), yearSelect.getValue()));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        transDataOverviewGrid.setColumns("kategorie", "azZimmer", "zimmerauslastung", "azBetten", "bettenauslastung", 
	        		"azNationalitäten", "azGenesen", "azGetestet", "azGeimpft");
	        transDataOverviewGrid.getColumns().forEach(col -> col.setAutoWidth(true));
	        transDataOverviewGrid.setMaxHeight("240px");
			
	        //-------------------------------------------------------------------- 

	        
		} else if(idSelect.isEmpty()) {
	        transDataOverviewGrid.addClassName("overview-grid");
	        try {
				transDataOverviewGrid.setItems(TransDataService.getTransDataOverviewByCategoryAndByMonth(katSelect.getValue(), monthSelect.getValue(), yearSelect.getValue()));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        transDataOverviewGrid.setColumns("kategorie", "azZimmer", "zimmerauslastung", "azBetten", "bettenauslastung", 
	        		"azNationalitäten", "azGenesen", "azGetestet", "azGeimpft");
	        transDataOverviewGrid.getColumns().forEach(col -> col.setAutoWidth(true));
	        transDataOverviewGrid.setMaxHeight("240px");
			
			
		} else {//if(idSelect.isEmpty() &&  monthSelect.isEmpty() && yearSelect.isEmpty() && katSelect.isEmpty()) {
	        transDataOverviewGrid.addClassName("overview-grid");
	        try { 
	        	 transDataOverviewGrid.setItems(TransDataService.getTransDataOverview());
	        } catch (SQLException e) {

				e.printStackTrace();
			}
	     
	       
	        transDataOverviewGrid.setColumns("kategorie", "azZimmer", "zimmerauslastung", "azBetten", "bettenauslastung", 
	        		"azNationalitäten", "azGenesen", "azGetestet", "azGeimpft");
	        transDataOverviewGrid.getColumns().forEach(col -> col.setAutoWidth(true));
	        transDataOverviewGrid.setMaxHeight("240px");
   
			
			}
		});
    	

        
        verticalLayout.add(horizontalLayout, transDataOverviewGrid);
      return verticalLayout;
    }
	
    /**
     * erstellt das grid mit allen Transactional Data
     * durch eine Combobox mit allen hotelIds und deinem suchen Button wird das suchen ermöglich
     * @return
     * @throws SQLException
     */
    
    private  Component configureGrid() throws SQLException {
        
    	VerticalLayout verticalLayout = new VerticalLayout();
    	
    	
		ComboBox<Integer> comboBox = new ComboBox<>();
		comboBox.setLabel("Hotel ID"); 
		comboBox.setPlaceholder("Hotel ID");
		ArrayList<Integer> ids = HotelService.getHotelID();
		Collections.sort(ids);
		comboBox.setItems(ids);
		HorizontalLayout horizontalLayout = new HorizontalLayout(comboBox);

	grid.setItems(OccupancyService.getAllOccupancies());
	grid.getColumns().forEach(col -> col.setAutoWidth(true));
	grid.setColumns("hotelID", "month", "year", "noBeds", "usedBeds", "bettenauslastung", "noRooms" , "usedRooms", "zimmerauslastung", "azNationalitäten", "nationalitäten", "azGenesen", "azGetestet", "azGeimpft");
	grid.setWidthFull();
		
		
		Button searchButton = new Button("Suchen", event -> {
			//verticalLayout.remove(grid);
		i = comboBox.getValue();
	
    		System.out.println(i);
    	
    		if(i == null) {
    			try {
					grid.setItems(OccupancyService.getAllOccupancies());
					grid.getColumns().forEach(col -> col.setAutoWidth(true));
					
				} catch (SQLException e) {
	
					e.printStackTrace();
				}

    		} 
    		else {
    		try {
    			GridByID2(i);
				
			} catch (SQLException e) {
			
				e.printStackTrace();
			}
    		}
    	});
		searchButton.setHeight("68px");
    	horizontalLayout.add(searchButton);
    	table.getColumns().forEach(col -> col.setAutoWidth(true));
    	table.setWidthFull();
    	verticalLayout.add(horizontalLayout);
    
      return verticalLayout;

    }
    public Component GridByID2(int i) throws SQLException {
    	
    	ArrayList<Occupancy> hotels = OccupancyService.getOccupancyById(i);
    	table.setItems(hotels);
    	
    	table.setVerticalScrollingEnabled(true);
    	
    	Grid.Column<Occupancy> id = table.addColumn(Occupancy::getHotelID).setHeader("ID");
    	Grid.Column<Occupancy> noRooms = table.addColumn(Occupancy::getNoRooms).setHeader("Anzahl Zimmer");
    	Grid.Column<Occupancy> noBeds = table.addColumn(Occupancy::getNoBeds).setHeader("Anzahl Betten");
    	Grid.Column<Occupancy> year = table.addColumn(Occupancy::getYear).setHeader("Jahr");
    	Grid.Column<Occupancy> month = table.addColumn(Occupancy::getMonth).setHeader("Monat");
    	Grid.Column<Occupancy> usedRooms = table.addColumn(Occupancy::getUsedRooms).setHeader("belegte Zimmer");
    	Grid.Column<Occupancy> usedBeds = table.addColumn(Occupancy::getUsedBeds).setHeader("belegte Zimmer");
    	Grid.Column<Occupancy> zimmerauslastung = table.addColumn(Occupancy::getZimmerauslastung).setHeader("Zimmerauslastung");
    	Grid.Column<Occupancy> bettenauslastung = table.addColumn(Occupancy::getBettenauslastung).setHeader("Bettenauslastung");
    	Grid.Column<Occupancy> azNationalitäten = table.addColumn(Occupancy::getAzNationalitäten).setHeader("Anzahl Nationalitäten");
    	Grid.Column<Occupancy> nationalitäten = table.addColumn(Occupancy::getNationalitäten).setHeader("Nationalitäten");
    	Grid.Column<Occupancy> azGenesen = table.addColumn(Occupancy::getAzGenesen).setHeader("Anzahl Genesen");
    	Grid.Column<Occupancy> azGetestet = table.addColumn(Occupancy::getAzGetestet).setHeader("Anzahl Getestet");
    	Grid.Column<Occupancy> azGeimpft = table.addColumn(Occupancy::getAzGeimpft).setHeader("Anzahl Geimpft");
    	
    	
     	id.setSortable(true);
     	year.setSortable(true);
     	month.setSortable(true);
     	usedRooms.setSortable(true);
     	usedBeds.setSortable(true); 
     	zimmerauslastung.setSortable(true);
     	bettenauslastung.setSortable(true);
     	azNationalitäten.setSortable(true);
     	nationalitäten.setSortable(true);  
     	azGenesen.setSortable(true); 
     	azGeimpft.setSortable(true);
     	azGetestet.setSortable(true);
     	noRooms.setSortable(true); 
     	noBeds.setSortable(true); 
     	
     	
    	Binder<Occupancy> binder = new Binder<>(Occupancy.class);
    	Editor<Occupancy> editor = table.getEditor();
    	editor.setBinder(binder);
    	editor.setBuffered(true);
    	
         table.getColumns().forEach(col -> col.setAutoWidth(true));

    	Div validationStatus = new Div();
    	validationStatus.setId("validation");


    	IntegerField yearField = new IntegerField();
    	binder.forField(yearField).withStatusLabel(validationStatus).bind("year");
    	year.setEditorComponent(yearField);
    	
    	IntegerField monthField = new IntegerField();
    	binder.forField(monthField).withStatusLabel(validationStatus).bind("month");
    	month.setEditorComponent(monthField);
    	
    	IntegerField URoomsField = new IntegerField();
    	binder.forField(URoomsField).withStatusLabel(validationStatus).bind("usedRooms");
    	usedRooms.setEditorComponent(URoomsField);
    	
    	IntegerField UbedsField = new IntegerField();
    	binder.forField(UbedsField).withStatusLabel(validationStatus).bind("usedBeds");
    	usedBeds.setEditorComponent(UbedsField);
    	
    	IntegerField azNationalitätenField = new IntegerField();
    	binder.forField(azNationalitätenField ).withStatusLabel(validationStatus).bind("azNationalitäten");
    	azNationalitäten.setEditorComponent(azNationalitätenField );
    	
    	TextField NationalitätenField = new TextField();
    	binder.forField(NationalitätenField).withStatusLabel(validationStatus).bind("nationalitäten");
    	nationalitäten.setEditorComponent(NationalitätenField);
    	
    	IntegerField noRoomField= new IntegerField();
    	binder.forField(noRoomField ).withStatusLabel(validationStatus).bind("noRooms");
    	noRooms.setEditorComponent(noRoomField );
    	
    	IntegerField noBedField = new IntegerField();
    	binder.forField(noBedField ).withStatusLabel(validationStatus).bind("noBeds");
    	noBeds.setEditorComponent(noBedField );
    	
    	IntegerField azGenesenField = new IntegerField();
    	binder.forField(azGenesenField ).withStatusLabel(validationStatus).bind("azGenesen");
    	azGenesen.setEditorComponent(azGenesenField );
    	
    	IntegerField azGetestetField = new IntegerField();
    	binder.forField(azGetestetField ).withStatusLabel(validationStatus).bind("azGetestet");
    	azGetestet.setEditorComponent(azGetestetField );
    	
    	IntegerField azGeimpftField = new IntegerField();
    	binder.forField(azGeimpftField ).withStatusLabel(validationStatus).bind("azGeimpft");
    	azGeimpft.setEditorComponent(azGeimpftField );
    	
    
    	Collection<Button> editButtons = Collections
    	        .newSetFromMap(new WeakHashMap<>());

    	Grid.Column<Occupancy> editorColumn = table.addComponentColumn(event -> {
    	    Button edit = new Button("Edit");
    	    edit.setClassName("hilfeButtons");
    	    edit.addClassName("edit");
    	    edit.addClickListener(e -> {
    	        editor.editItem(event);
    	       // idField.focus();
    	    });
    	    edit.setEnabled(!editor.isOpen());
    	    editButtons.add(edit);
    	    return edit;
    	});

    	editor.addOpenListener(e -> editButtons.stream()
    	        .forEach(button -> button.setEnabled(!editor.isOpen())));
    	editor.addCloseListener(e -> editButtons.stream()
    	        .forEach(button -> button.setEnabled(!editor.isOpen())));

    	Button save = new Button("Save", e -> {
    		
    		double zimmerauslastungBerechnet = (URoomsField.getValue() / noRoomField.getValue());
    		double bettenauslastungBerechnet = (UbedsField.getValue() / noBedField.getValue());

    		Occupancy occupancy = new Occupancy(i, noRoomField.getValue(), URoomsField.getValue(), noBedField.getValue(), UbedsField.getValue(),
					yearField.getValue(), monthField.getValue(), zimmerauslastungBerechnet ,bettenauslastungBerechnet, azNationalitätenField.getValue(), NationalitätenField.getValue(),
					azGenesenField.getValue(), azGetestetField.getValue(), azGeimpftField.getValue());
			
    		try {
				OccupancyService.updateOccupancyByID(i, monthField.getValue(),yearField.getValue(), occupancy);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			editor.save();
			
			Notification.show("Hotel gespeichert");
    	});
    	save.setClassName("hilfeButtons");

    	Button cancel = new Button("Cancel", e -> editor.cancel());
    	cancel.addClassName("cancel");
    	cancel.setClassName("hilfeButtons");

    	table.getElement().addEventListener("keyup", event -> editor.cancel())
    	        .setFilter("event.key === 'Escape' || event.key === 'Esc'");

    	Div buttons = new Div(save, cancel);
    	editorColumn.setEditorComponent(buttons);

    
    	add(validationStatus, table);
    	
		return table;
    }
    
    /**
     * erstellt die Buttons Daten hinzufügen und setzt einen Clicklistener auf diesen
     * @return
     */
	private HorizontalLayout getButtons() {

		Button addButton = new Button("Daten hinzufügen");
		addButton.setClassName("hilfeButtons");
		Button editButton = new Button("Daten bearbeiten");
		editButton.setClassName("hilfeButtons");
		
        HorizontalLayout hori = new HorizontalLayout(addButton, editButton);
        hori.addClassName("buttons");
        
//        hori.getElement().getStyle().set("margin", "auto");
       
        
    	addButton.addClickListener(Event -> {
			try {
				dialogAddTransData();
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		}); 
    	
    	editButton.addClickListener(Event -> {
			try {
				dialogEditTransData();
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		}); 
    	
        return hori;
    }
	
	/**
	 * erstellt ein dialog fenster zum hinzufügen von transactionalData
	 * wird in der Methode getButtons() aufgerufen
	 * @return
	 * @throws SQLException
	 */
	public Component dialogAddTransData() throws SQLException {
		H1 hinzufuegenH1 = new H1("Daten hinzufügen");
		Dialog dialog = new Dialog();
		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);
		dialog.setWidth("600px");
		Span message = new Span();
		
		dialog.open();
		
		dialog.add(hinzufuegenH1);
		
		VerticalLayout verticalLayout = new VerticalLayout();
		 
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		
		Select<Integer> monthSelect = new Select<>();
		monthSelect.setItems(OccupancyService.getAllOccupancyMonths());
		monthSelect.setLabel("Monat");
		
		ComboBox<Integer> hotelIDBox = new ComboBox<>();
		hotelIDBox.setLabel("Hotel ID");
		hotelIDBox.setPlaceholder("Hotel ID");
	
		ArrayList<Integer> ids = HotelService.getHotelID();
		Collections.sort(ids);
		
		hotelIDBox.setItems(ids);

		
		Select<Integer> yearSelect = new Select<>();
		yearSelect.setItems(2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021);
		yearSelect.setLabel("Jahr");
		
		
		horizontalLayout.add(monthSelect, yearSelect);
		
		verticalLayout.add(horizontalLayout);
		
		
		HorizontalLayout horizontalLayout2= new HorizontalLayout();

		
			TextField HotelName = new TextField("Hotel Name");
//			horizontalLayout2.removeAll();
//			horizontalLayout2.add(leerField,yearSelect);
		
			horizontalLayout2.add(hotelIDBox);

		
			hotelIDBox.addValueChangeListener( event -> {
			try {
				int StorageID = hotelIDBox.getValue();
				horizontalLayout2.removeAll();
				horizontalLayout2.add(hotelIDBox, getHotelName(StorageID));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
				);

		verticalLayout.add(horizontalLayout2);
		
		
		NumberField anzZimmer = new NumberField("Anz. Zimmer");
		
		NumberField anzBelZim = new NumberField("Anz. belegter Zimmer");
		
		HorizontalLayout horizontalLayout3 = new HorizontalLayout();
		
		horizontalLayout3.add(anzZimmer, anzBelZim);
		verticalLayout.add(horizontalLayout3);
		
		
		NumberField anzBetten = new NumberField("Anz. Betten");

		NumberField anzBelBet = new NumberField("Anz. belegter Betten");

		
		HorizontalLayout horizontalLayout4 = new HorizontalLayout();
		
		horizontalLayout4.add(anzBetten, anzBelBet);
		verticalLayout.add(horizontalLayout4);
		
	
		NumberField anzNational = new NumberField("Anz. Nationalitäten");

		TextField nationalitaeten = new TextField("Nationalitäten");
		
		HorizontalLayout horizontalLayout5 = new HorizontalLayout();
		
		horizontalLayout5.add(anzNational, nationalitaeten);
		verticalLayout.add(horizontalLayout5);		
		
		NumberField anzGenesene = new NumberField("Anz. Genesene");

		NumberField anzGetestet = new NumberField("Anz. Getestete");
		
		NumberField anzGeimpfte = new NumberField("Anz. Geimpfte");
		
		HorizontalLayout horizontalLayout6 = new HorizontalLayout();
		
		horizontalLayout6.add(anzGenesene, anzGetestet, anzGeimpfte);
		verticalLayout.add(horizontalLayout6);		
		
		
		dialog.add(verticalLayout);
		
		Button addButton = new Button("Daten hinzufügen", Event -> {
			
			boolean check = false;
			int counter = 0;
			
			String ausgabe = "";
			
			
			if(!(monthSelect.isEmpty())) {
				int month = monthSelect.getValue();
				}
				else {
					counter ++;
					ausgabe += "Monat ausfüllen! \n";
				}
				
			if(!(yearSelect.isEmpty())) {
				int year = yearSelect.getValue();
				}
				else {
					counter++;
					ausgabe += "Jahr ausfüllen! \n";
				}
				
				if(!(anzBelZim.isEmpty())) {
					Double anzBelZimmer = anzBelZim.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Belegter Zimmer ausfüllen! \n";
					}

				if(!(anzZimmer.isEmpty())) {
					Double anzZimmer1 = anzZimmer.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Zimmer ausfüllen! \n";
					}

				if(!(anzBelBet.isEmpty())) {
					Double anzBelBetten = anzBelBet.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl belegter Betten ausfüllen! \n";
					}
				if(!(anzBetten.isEmpty())) {
					Double anzBetten1 = anzBetten.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Betten ausfüllen! \n";
					}
				
				if(!(anzNational.isEmpty())) {
					Double anzNati = anzNational.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Nationalitäten ausfüllen! \n";
					}
				
				if(!(nationalitaeten.isEmpty())) {
					String natio = nationalitaeten.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Nationalitäten ausfüllen! \n";
					}
				if(!(anzGenesene.isEmpty())) {
					Double genesen = anzGenesene.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Genesene ausfüllen! \n";
					}
				
				if(!(anzGetestet.isEmpty())) {
					Double genesen = anzGenesene.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Getestete ausfüllen! \n";
					}
				
				if(!(anzGeimpfte.isEmpty())) {
					Double genesen = anzGenesene.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Geimpfte ausfüllen! \n";
					}

			if (counter ==0)	{
				check = true;
				ausgabe = "";
				
			}
			
			
			
			if(check) {
				System.out.println("Hallo check = true");
				double zimmerauslastung = (anzBelZim.getValue() / anzZimmer.getValue());
				double bettenauslastung = (anzBelBet.getValue() / anzBetten.getValue());


					int month = monthSelect.getValue();
					
					int year = yearSelect.getValue();
					
					int id = hotelIDBox.getValue();
					
					Double zwischen4 = anzGenesene.getValue();
					int anzBelZimmer = (int) Math.round(zwischen4);
					
					Double zwischen5 = anzZimmer.getValue();
					int anzZimmer1 = (int) Math.round(zwischen5);
					
					Double zwischen6 = anzBelBet.getValue();
					int anzBelBetten = (int) Math.round(zwischen6);

					Double zwischen7 = anzBetten.getValue();
					int anzBetten1 = (int) Math.round(zwischen7);
					
					Double zwischen8 = anzNational.getValue();
					int anzNati = (int) Math.round(zwischen8);
					
					String natio = nationalitaeten.getValue();
					 
					Double zwischen1 = anzGenesene.getValue();
					int genesen = (int) Math.round(zwischen1);
					 	
					Double zwischen2 = anzGenesene.getValue();
					int getestet = (int) Math.round(zwischen2);
					
					Double zwischen3 = anzGenesene.getValue();
					int geimpft = (int) Math.round(zwischen3);
					 
					
	
						System.out.printf(id + " "+  anzZimmer1 + " "+  anzBelZimmer +  " "+ anzBetten1 +  " "+ 
								anzBelBetten +  " "+ year +  " "+ month +  " "+ zimmerauslastung +  " "+ bettenauslastung +  " "+ anzNati +  " "+ natio +
						 " "+ genesen + " "+  getestet + " "+ geimpft);
	
	
					Occupancy occupancy = new Occupancy(id, anzZimmer1, anzBelZimmer, anzBetten1, 
							anzBelBetten, year, month, zimmerauslastung, bettenauslastung, anzNati, natio,
					genesen, getestet, geimpft);
					
					try {
						OccupancyService.insertNewOccupancy(occupancy);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		
				
				
				Notification.show("Daten hinzugefügt");
				
						dialog.close();
						
			} else if(!check) {
				
				Notification.show("Bitte alle Daten ausfüllen");
				Notification.show(ausgabe);
				ausgabe = "";
			}
			
		});
		addButton.setClassName("hilfeButtons");
		Button closeButton = new Button("Abbrechen", Event -> {
			
			Notification.show("Abgebrochen");
				dialog.close();
});
		closeButton.setClassName("hilfeButtons");
		HorizontalLayout hori3 = new HorizontalLayout();
		
		hori3.add(addButton);
		
		hori3.add(closeButton);
		
		verticalLayout.add(hori3);
		
		dialog.add(verticalLayout);
		
		return dialog;
		
	}
	
	
	public Component dialogEditTransData() throws SQLException {
		H1 edit1 = new H1("Daten bearbeiten");
		Dialog dialog = new Dialog();
		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);
		dialog.setWidth("600px");
		Span message = new Span();
		
		dialog.open();
		
		dialog.add(edit1);
		
		VerticalLayout verticalLayout = new VerticalLayout();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		HorizontalLayout horizontalLayout2= new HorizontalLayout();
		HorizontalLayout horizontalLayout3 = new HorizontalLayout();
		HorizontalLayout horizontalLayout4 = new HorizontalLayout();
		HorizontalLayout horizontalLayout5 = new HorizontalLayout();
		HorizontalLayout horizontalLayout6 = new HorizontalLayout();

		
		Select<Integer> monthSelect = new Select<>();
		monthSelect.setItems(OccupancyService.getAllOccupancyMonths());
		monthSelect.setLabel("Monat");
		
		ComboBox<Integer> hotelIDBox = new ComboBox<>();
		hotelIDBox.setLabel("Hotel ID");
		hotelIDBox.setPlaceholder("Hotel ID");
	
		ArrayList<Integer> ids = HotelService.getHotelID();
		Collections.sort(ids);
		
		hotelIDBox.setItems(ids);

		
		Select<Integer> yearSelect = new Select<>();
		yearSelect.setItems(2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021);
		yearSelect.setLabel("Jahr");
	
		TextField HotelName = new TextField("Hotel Name");

		NumberField anzZimmer = new NumberField("Anz. Zimmer");
		
		NumberField anzBelZim = new NumberField("Anz. belegter Zimmer");
		
		NumberField anzBetten = new NumberField("Anz. Betten");
		
		NumberField anzBelBet = new NumberField("Anz. belegter Betten");
		
		NumberField anzNational = new NumberField("Anz. Nationalitäten");

		TextField nationalitaeten = new TextField("Nationalitäten");
		
		NumberField anzGenesene = new NumberField("Anz. Genesene");

		NumberField anzGetestet = new NumberField("Anz. Getestete");
		
		NumberField anzGeimpfte = new NumberField("Anz. Geimpfte");
		

		horizontalLayout.add(monthSelect, yearSelect);
		
		horizontalLayout2.add(hotelIDBox);
		
		horizontalLayout3.add(anzZimmer, anzBelZim);
		
		horizontalLayout4.add(anzBetten, anzBelBet);
		
		horizontalLayout5.add(anzNational, nationalitaeten);
		
		horizontalLayout6.add(anzGenesene, anzGetestet, anzGeimpfte);
		
		verticalLayout.add(horizontalLayout, horizontalLayout2, horizontalLayout3, horizontalLayout4, horizontalLayout5, horizontalLayout6);
		
		
		yearSelect.addValueChangeListener(Event -> {
			
			Notification.show("Zum Bearbeiten bitte Jahr, Monat und Hotel ID ausfüllen!");
			
			if(yearSelect.getValue() != null && monthSelect.getValue() != null) {
				if(hotelIDBox.getValue() != null && yearSelect.getValue() != null && monthSelect.getValue() != null) {
					try {
					int StorageID = hotelIDBox.getValue();
					horizontalLayout2.removeAll();
					horizontalLayout2.add(hotelIDBox, getHotelName(StorageID));
					Occupancy occupancyNew = OccupancyService.OccupancyById(hotelIDBox.getValue(), monthSelect.getValue(), yearSelect.getValue());
					anzZimmer.setValue((double) occupancyNew.getNoRooms());
					anzBelZim.setValue((double) occupancyNew.getUsedRooms());
					anzBetten.setValue((double) occupancyNew.getNoBeds());
					anzBelBet.setValue((double) occupancyNew.getUsedBeds());
					anzNational.setValue((double) occupancyNew.getAzNationalitäten());
					nationalitaeten.setValue( occupancyNew.getNationalitäten());
					anzGeimpfte.setValue((double) occupancyNew.getAzGeimpft());
					anzGetestet.setValue((double) occupancyNew.getAzGetestet());
					anzGenesene.setValue((double) occupancyNew.getAzGenesen());

				} catch (SQLException e) {
					e.printStackTrace();
				} }}
		});
		
		monthSelect.addValueChangeListener(Event -> {
				Notification.show("Zum Bearbeiten bitte Jahr, Monat und Hotel ID ausfüllen!");
	
				if(hotelIDBox.getValue() != null && yearSelect.getValue() != null && monthSelect.getValue() != null) {
					try {
					int StorageID = hotelIDBox.getValue();
					horizontalLayout2.removeAll();
					horizontalLayout2.add(hotelIDBox, getHotelName(StorageID));
					Occupancy occupancyNew = OccupancyService.OccupancyById(hotelIDBox.getValue(), monthSelect.getValue(), yearSelect.getValue());
					anzZimmer.setValue((double) occupancyNew.getNoRooms());
					anzBelZim.setValue((double) occupancyNew.getUsedRooms());
					anzBetten.setValue((double) occupancyNew.getNoBeds());
					anzBelBet.setValue((double) occupancyNew.getUsedBeds());
					anzNational.setValue((double) occupancyNew.getAzNationalitäten());
					nationalitaeten.setValue( occupancyNew.getNationalitäten());
					anzGeimpfte.setValue((double) occupancyNew.getAzGeimpft());
					anzGetestet.setValue((double) occupancyNew.getAzGetestet());
					anzGenesene.setValue((double) occupancyNew.getAzGenesen());

				} catch (SQLException e) {
					e.printStackTrace();
				} }
		});
		
		
		hotelIDBox.addValueChangeListener( event -> {
			
			Notification.show("Zum Bearbeiten bitte Jahr, Monat und Hotel ID ausfüllen!");
			
			if(hotelIDBox.getValue() != null && yearSelect.getValue() != null && monthSelect.getValue() != null) {
				try {
				int StorageID = hotelIDBox.getValue();
				horizontalLayout2.removeAll();
				horizontalLayout2.add(hotelIDBox, getHotelName(StorageID));
				Occupancy occupancyNew = OccupancyService.OccupancyById(hotelIDBox.getValue(), monthSelect.getValue(), yearSelect.getValue());
				anzZimmer.setValue((double) occupancyNew.getNoRooms());
				anzBelZim.setValue((double) occupancyNew.getUsedRooms());
				anzBetten.setValue((double) occupancyNew.getNoBeds());
				anzBelBet.setValue((double) occupancyNew.getUsedBeds());
				anzNational.setValue((double) occupancyNew.getAzNationalitäten());
				nationalitaeten.setValue( occupancyNew.getNationalitäten());
				anzGeimpfte.setValue((double) occupancyNew.getAzGeimpft());
				anzGetestet.setValue((double) occupancyNew.getAzGetestet());
				anzGenesene.setValue((double) occupancyNew.getAzGenesen());

			} catch (SQLException e) {
				e.printStackTrace();
			}
			} }
		);
		
		dialog.add(verticalLayout);
		
		Button addButton = new Button("Daten hinzufügen", Event -> {
			
			boolean check = false;
			int counter = 0;
			
			String ausgabe = "";
			
			if(!(monthSelect.isEmpty())) {
				int month = monthSelect.getValue();
				}
				else {
					counter ++;
					ausgabe += "Monat ausfüllen! \n";
				}
				
			if(!(yearSelect.isEmpty())) {
				int year = yearSelect.getValue();
				}
				else {
					counter++;
					ausgabe += "Jahr ausfüllen! \n";
				}
				
				if(!(anzBelZim.isEmpty())) {
					Double anzBelZimmer = anzBelZim.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Belegter Zimmer ausfüllen! \n";
					}

				if(!(anzZimmer.isEmpty())) {
					Double anzZimmer1 = anzZimmer.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Zimmer ausfüllen! \n";
					}

				if(!(anzBelBet.isEmpty())) {
					Double anzBelBetten = anzBelBet.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl belegter Betten ausfüllen! \n";
					}
				if(!(anzBetten.isEmpty())) {
					Double anzBetten1 = anzBetten.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Betten ausfüllen! \n";
					}
				
				if(!(anzNational.isEmpty())) {
					Double anzNati = anzNational.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Nationalitäten ausfüllen! \n";
					}
				
				if(!(nationalitaeten.isEmpty())) {
					String natio = nationalitaeten.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Nationalitäten ausfüllen! \n";
					}
				if(!(anzGenesene.isEmpty())) {
					Double genesen = anzGenesene.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Genesene ausfüllen! \n";
					}
				
				if(!(anzGetestet.isEmpty())) {
					Double genesen = anzGetestet.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Getestete ausfüllen! \n";
					}
				
				if(!(anzGeimpfte.isEmpty())) {
					Double genesen = anzGeimpfte.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Geimpfte ausfüllen! \n";
					}

			if (counter ==0)	{
				check = true;
				ausgabe = "";
				
			}
			
			
			if(check) {
				double zimmerauslastung = (anzBelZim.getValue() / anzZimmer.getValue());
				double bettenauslastung = (anzBelBet.getValue() / anzBetten.getValue());


					int month = monthSelect.getValue();
					
					int year = yearSelect.getValue();
					
					int id = hotelIDBox.getValue();
					
					Double zwischen4 = anzBelZim.getValue();
					int anzBelZimmer = (int) Math.round(zwischen4);
					
					Double zwischen5 = anzZimmer.getValue();
					int anzZimmer1 = (int) Math.round(zwischen5);
					
					Double zwischen6 = anzBelBet.getValue();
					int anzBelBetten = (int) Math.round(zwischen6);

					Double zwischen7 = anzBetten.getValue();
					int anzBetten1 = (int) Math.round(zwischen7);
					
					Double zwischen8 = anzNational.getValue();
					int anzNati = (int) Math.round(zwischen8);
					
					String natio = nationalitaeten.getValue();
					 
					Double zwischen1 = anzGenesene.getValue();
					int genesen = (int) Math.round(zwischen1);
					 	
					Double zwischen2 = anzGetestet.getValue();
					int getestet = (int) Math.round(zwischen2);
					
					Double zwischen3 = anzGeimpfte.getValue();
					int geimpft = (int) Math.round(zwischen3);
					 
	
					System.out.printf(id + " "+  anzZimmer1 + " "+  anzBelZimmer +  " "+ anzBetten1 +  " "+ 
						anzBelBetten +  " "+ year +  " "+ month +  " "+ zimmerauslastung +  " "+ bettenauslastung +  " "+ anzNati +  " "+ natio +
						" "+ genesen + " "+  getestet + " "+ geimpft);
					
	
					Occupancy occupancy = new Occupancy(id, anzZimmer1, anzBelZimmer, anzBetten1, 
							anzBelBetten, year, month, zimmerauslastung, bettenauslastung, anzNati, natio,
					genesen, getestet, geimpft);
					
					try {
						System.out.println("UPDATING");
						OccupancyService.updateOccupancyByID(id, month, year, occupancy);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		
				
				
				Notification.show("Transaktionsdaten bearbeitet");
				
						dialog.close();
						
			} else if(!check) {
				
				Notification.show("Bitte alle Daten ausfüllen");
				Notification.show(ausgabe);
				ausgabe = "";
			}
			
		});
		addButton.setClassName("hilfeButtons");
		Button closeButton = new Button("Abbrechen", Event -> {
			
			Notification.show("Abgebrochen");
				dialog.close();
});
		closeButton.setClassName("hilfeButtons");
		HorizontalLayout hori3 = new HorizontalLayout();
		
		hori3.add(addButton, closeButton);
		
		verticalLayout.add(hori3);
		
		dialog.add(verticalLayout);
		
		return dialog;
		
	}

	/**
	 * erstellt eine comboBox mit allen hotelIds
	 * @return
	 * @throws SQLException
	 */
	public static ComboBox<Integer> getHotelID() throws SQLException {
		
		ComboBox<Integer> comboBox = new ComboBox<>();
		comboBox.setLabel("Hotel ID");
		comboBox.setPlaceholder("Hotel ID");
	
		ArrayList<Integer> ids = HotelService.getHotelID();
		Collections.sort(ids);
		
		comboBox.setItems(ids);

		return comboBox;
	}
	
	/**
	 * erstellt ein TextField mit einem hotelnamen, der der übergebenen hotelID zugeordnet ist
	 * @param i
	 * @return
	 * @throws SQLException
	 */
	public static TextField getHotelName(int i) throws SQLException {
	
		String name = "";
		
		name = HotelService.getHotelNameById(i); 
		System.out.println(name);
		TextField hotelnameField = new TextField(name);
		hotelnameField.setLabel("Hotel Name");
		hotelnameField.setValue(name);
		
		
		return hotelnameField;
	}
} 

