package com.noe.to.web.frontend.view.HotelOwnerViews;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import com.noe.to.web.backend.entity.Hotel;
import com.noe.to.web.backend.entity.MasterDataOverview;
import com.noe.to.web.backend.entity.Occupancy;
import com.noe.to.web.backend.entity.TransDataOverview;
import com.noe.to.web.backend.service.HotelService;
import com.noe.to.web.backend.service.OccupancyService;
import com.noe.to.web.backend.service.TransDataService;
import com.noe.to.web.frontend.FrontendUtils;
import com.noe.to.web.frontend.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import antlr.debug.Event;

@PageTitle("TransactionaDataOwner") 
@Route(value = "TransactionalDataOwner", layout = MainLayoutOwner.class)

public class TransactionalDataOwner extends VerticalLayout{

	Grid<Occupancy> grid = new Grid<>(Occupancy.class);
	OccupancyService occupancyService = new OccupancyService();
	
	Grid<TransDataOverview> transDataOverviewGrid = new Grid<>(TransDataOverview.class);

	
	Integer i = null;

	/**
	 * Konstrukter der Klasse TransactionalData
	 * fügt alle Komponenten der Seite hinzu
	 * @throws SQLException
	 */
	public TransactionalDataOwner() throws SQLException {

		add(getTitle());

		add(allgemein_ue());
		
		add(configureOccupancyOverviewGrid());

		add(hotel_ue());

		Div content = new Div(grid);
        content.addClassName("content");
        content.setSizeFull();

		getDefaultHorizontalComponentAlignment();

		add(configureGrid());

		add(getButtons());

	}
	
	/**
	 * creates the Header of the Page
	 * @return
	 */
	private Component hotel_ue() {
		
		H2 title = new H2("Hotelübersicht");
		
		return title;
	}


	/**
	 * creates the second header and the backup button
	 * @return
	 */
	private Component getTitle() {
		
		H1 TStamm = new H1("Transaktionsdaten");
		
		HorizontalLayout hori = new HorizontalLayout(TStamm);;
        hori.setWidthFull();

        hori.setAlignItems(FlexComponent.Alignment.CENTER);

        return hori;
	}
	/**
	 * creates the header for the overview and the send button for the master data
	 * @return
	 */
	private Component allgemein_ue() {
		H2 allg = new H2("Allgemeine Übersicht");
		
		HorizontalLayout hori = new HorizontalLayout(allg);
		        hori.addClassName("h2");
		        hori.setWidthFull();
		        hori.setAlignItems(FlexComponent.Alignment.CENTER);
		        return hori;
		}
	
	/**
	 * creates the Occupancy Overview, 4 ComboBoxes and 2 buttons
	 * has 2 click listeners on the button to change the appearance and meaning of the overview
	 * @return
	 * @throws SQLException
	 */
    private  Component configureOccupancyOverviewGrid() throws SQLException {
    	
   	 transDataOverviewGrid.setItems(TransDataService.getTransDataOverviewByID(FrontendUtils.getSessionID()));
   	 transDataOverviewGrid.setHeightByRows(true);
		
      return transDataOverviewGrid;
    }
	
    /**
     * erstellt das grid mit allen Transactional Data
     * durch eine Combobox mit allen hotelIds und deinem suchen Button wird das suchen ermöglich
     * @return
     * @throws SQLException
     */
    private  Component configureGrid() throws SQLException {
        
    	
    	

		grid.setItems(OccupancyService.getOccupancyById(FrontendUtils.getSessionID()));
	
		grid.getColumns().forEach(col -> col.setAutoWidth(true));
		grid.setColumns("hotelID", "month", "year", "noBeds", "usedBeds", "bettenauslastung", "noRooms" , "usedRooms", "zimmerauslastung", "azNationalitäten", "nationalitäten", "azGenesen", "azGetestet", "azGeimpft");
    
      return grid;

    }
    
    /**
     * erstellt das Grid für die Methode configureOccupancyOverviewGrid mit der übergebenen ID i
     * @param i
     * @return
     * @throws SQLException
     */
    public Component GridByID(int i) throws SQLException {
    
    	grid.setItems(OccupancyService.getOccupancyById(i));
    	grid.getColumns().forEach(col -> col.setAutoWidth(true));
    	return grid;
    }
    
    /**
     * erstellt die Buttons Daten hinzufügen und setzt einen Clicklistener auf diesen
     * @return
     */
	private HorizontalLayout getButtons() {

		Button addButton = new Button("Daten hinzufügen");
		addButton.setClassName("hilfeButtons");
		Button editButton = new Button("Daten bearbeiten");
		editButton.setClassName("hilfeButtons");
		
        HorizontalLayout hori = new HorizontalLayout(addButton, editButton);
        hori.addClassName("buttons");
        hori.getStyle().set("margin", "auto");
        
    	addButton.addClickListener(Event -> {
			try {
				dialogAddTransData();
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		}); 
    	
    	editButton.addClickListener(Event -> {
			try {
				dialogEditTransData();
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		}); 
    	
        return hori;
    }
	
	/**
	 * erstellt ein dialog fenster zum hinzufügen von transactionalData
	 * wird in der Methode getButtons() aufgerufen
	 * @return
	 * @throws SQLException
	 */
	public Component dialogAddTransData() throws SQLException {
		H1 hinzufuegenH1 = new H1("Daten hinzufügen");
		Dialog dialog = new Dialog();
		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);
		dialog.setWidth("600px");
		Span message = new Span();
		
		dialog.open();
		
		dialog.add(hinzufuegenH1);
		
		VerticalLayout verticalLayout = new VerticalLayout();
		 
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		
		ArrayList <Hotel> hotelVonUser = HotelService.getHotelByUserId(FrontendUtils.getSessionID());
		
		Select<Integer> monthSelect = new Select<>();
		monthSelect.setItems(OccupancyService.getAllOccupancyMonths());
		monthSelect.setLabel("Monat");
		
		IntegerField hotelIDBox = new IntegerField();
		hotelIDBox.setValue(hotelVonUser.get(0).getHotelID());
		hotelIDBox.setLabel("HotelID");
		hotelIDBox.setEnabled(false);

		TextField hotelNameBox = new TextField();
		hotelNameBox.setValue(hotelVonUser.get(0).getHotelName());
		hotelNameBox.setLabel("Hotelname");
		hotelNameBox.setEnabled(false);
		
		Select<Integer> yearSelect = new Select<>();
		yearSelect.setItems(2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021);
		yearSelect.setLabel("Jahr");
		
		
		horizontalLayout.add(monthSelect, yearSelect);
		
		verticalLayout.add(horizontalLayout);
		
		
		HorizontalLayout horizontalLayout2= new HorizontalLayout();
		
			horizontalLayout2.add(hotelIDBox, hotelNameBox);

		verticalLayout.add(horizontalLayout2);
		
		
		NumberField anzZimmer = new NumberField("Anz. Zimmer");
		
		NumberField anzBelZim = new NumberField("Anz. belegter Zimmer");
		
		HorizontalLayout horizontalLayout3 = new HorizontalLayout();
		
		horizontalLayout3.add(anzZimmer, anzBelZim);
		verticalLayout.add(horizontalLayout3);
		
		
		NumberField anzBetten = new NumberField("Anz. Betten");

		NumberField anzBelBet = new NumberField("Anz. belegter Betten");

		
		HorizontalLayout horizontalLayout4 = new HorizontalLayout();
		
		horizontalLayout4.add(anzBetten, anzBelBet);
		verticalLayout.add(horizontalLayout4);
		
	
		NumberField anzNational = new NumberField("Anz. Nationalitäten");

		TextField nationalitaeten = new TextField("Nationalitäten");
		
		HorizontalLayout horizontalLayout5 = new HorizontalLayout();
		
		horizontalLayout5.add(anzNational, nationalitaeten);
		verticalLayout.add(horizontalLayout5);		
		
		NumberField anzGenesene = new NumberField("Anz. Genesene");

		NumberField anzGetestet = new NumberField("Anz. Getestete");
		
		NumberField anzGeimpfte = new NumberField("Anz. Geimpfte");
		
		HorizontalLayout horizontalLayout6 = new HorizontalLayout();
		
		Checkbox sicherCheckbox = new Checkbox();
		sicherCheckbox.setLabel("Ich stimme den Änderungen zu");
		sicherCheckbox.setValue(false);
		
		horizontalLayout6.add(anzGenesene, anzGetestet, anzGeimpfte);
		verticalLayout.add(horizontalLayout6,sicherCheckbox);		
		
		
		
		dialog.add(verticalLayout);
		
		Button addButton = new Button("Daten bearbeiten", Event -> {
			
			boolean check = false;
			int counter = 0;
			
			String ausgabe = "";
			
			
			if(!(monthSelect.isEmpty())) {
				int month = monthSelect.getValue();
				}
				else {
					counter ++;
					ausgabe += "Monat ausfüllen! \n";
				}
				
			if(!(yearSelect.isEmpty())) {
				int year = yearSelect.getValue();
				}
				else {
					counter++;
					ausgabe += "Jahr ausfüllen! \n";
				}
				
				if(!(anzBelZim.isEmpty())) {
					Double anzBelZimmer = anzBelZim.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Belegter Zimmer ausfüllen! \n";
					}

				if(!(anzZimmer.isEmpty())) {
					Double anzZimmer1 = anzZimmer.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Zimmer ausfüllen! \n";
					}

				if(!(anzBelBet.isEmpty())) {
					Double anzBelBetten = anzBelBet.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl belegter Betten ausfüllen! \n";
					}
				if(!(anzBetten.isEmpty())) {
					Double anzBetten1 = anzBetten.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Betten ausfüllen! \n";
					}
				
				if(!(anzNational.isEmpty())) {
					Double anzNati = anzNational.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Nationalitäten ausfüllen! \n";
					}
				
				if(!(nationalitaeten.isEmpty())) {
					String natio = nationalitaeten.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Nationalitäten ausfüllen! \n";
					}
				if(!(anzGenesene.isEmpty())) {
					Double genesen = anzGenesene.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Genesene ausfüllen! \n";
					}
				
				if(!(anzGetestet.isEmpty())) {
					Double genesen = anzGenesene.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Getestete ausfüllen! \n";
					}
				
				if(!(anzGeimpfte.isEmpty())) {
					Double genesen = anzGenesene.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Geimpfte ausfüllen! \n";
					}
				if (sicherCheckbox.getValue()== false) {
					counter++;
					ausgabe+= "Bestätigen Sie die Angaben";
				}

			if (counter ==0)	{
				check = true;
				ausgabe = "";
				
			}
			
			
			
			if(check) {
				System.out.println("Hallo check = true");
				double zimmerauslastung = (anzBelZim.getValue() / anzZimmer.getValue());
				double bettenauslastung = (anzBelBet.getValue() / anzBetten.getValue());


					int month = monthSelect.getValue();
					
					int year = yearSelect.getValue();
					
					int id = hotelIDBox.getValue();
					
					Double zwischen4 = anzGenesene.getValue();
					int anzBelZimmer = (int) Math.round(zwischen4);
					
					Double zwischen5 = anzZimmer.getValue();
					int anzZimmer1 = (int) Math.round(zwischen5);
					
					Double zwischen6 = anzBelBet.getValue();
					int anzBelBetten = (int) Math.round(zwischen6);

					Double zwischen7 = anzBetten.getValue();
					int anzBetten1 = (int) Math.round(zwischen7);
					
					Double zwischen8 = anzNational.getValue();
					int anzNati = (int) Math.round(zwischen8);
					
					String natio = nationalitaeten.getValue();
					 
					Double zwischen1 = anzGenesene.getValue();
					int genesen = (int) Math.round(zwischen1);
					 	
					Double zwischen2 = anzGenesene.getValue();
					int getestet = (int) Math.round(zwischen2);
					
					Double zwischen3 = anzGenesene.getValue();
					int geimpft = (int) Math.round(zwischen3);
					 
					
	
						System.out.printf(id + " "+  anzZimmer1 + " "+  anzBelZimmer +  " "+ anzBetten1 +  " "+ 
								anzBelBetten +  " "+ year +  " "+ month +  " "+ zimmerauslastung +  " "+ bettenauslastung +  " "+ anzNati +  " "+ natio +
						 " "+ genesen + " "+  getestet + " "+ geimpft);
	
	
					Occupancy occupancy = new Occupancy(id, anzZimmer1, anzBelZimmer, anzBetten1, 
							anzBelBetten, year, month, zimmerauslastung, bettenauslastung, anzNati, natio,
					genesen, getestet, geimpft);
					
					try {
						OccupancyService.insertNewOccupancy(occupancy);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		
				
				
				Notification.show("Hotel hinzugefügt");
				
						dialog.close();
						
			} else if(!check) {
				
				Notification.show("Bitte alle Daten ausfüllen");
				Notification.show(ausgabe);
				ausgabe = "";
			}
			
		});
		addButton.setClassName("hilfeButtons");
		
		Button closeButton = new Button("Abbrechen", Event -> {
			
			Notification.show("Abgebrochen");
				dialog.close();
});
		closeButton.setClassName("hilfeButtons");
		HorizontalLayout hori3 = new HorizontalLayout();
		
		hori3.add(addButton);
		
		hori3.add(closeButton);
		
		verticalLayout.add(hori3);
		
		dialog.add(verticalLayout);
		
		return dialog;
		
	}
	
	public Component dialogEditTransData() throws SQLException {
		H1 edit1 = new H1("Daten hinzufügen");
		Dialog dialog = new Dialog();
		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);
		dialog.setWidth("600px");
		Span message = new Span();
		
		dialog.open();
		
		dialog.add(edit1);
		
		VerticalLayout verticalLayout = new VerticalLayout();
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		HorizontalLayout horizontalLayout2= new HorizontalLayout();
		HorizontalLayout horizontalLayout3 = new HorizontalLayout();
		HorizontalLayout horizontalLayout4 = new HorizontalLayout();
		HorizontalLayout horizontalLayout5 = new HorizontalLayout();
		HorizontalLayout horizontalLayout6 = new HorizontalLayout();

		ArrayList <Hotel> hotelVonUser = HotelService.getHotelByUserId(FrontendUtils.getSessionID());
		IntegerField hotelIDBox = new IntegerField();
		hotelIDBox.setValue(hotelVonUser.get(0).getHotelID());
		hotelIDBox.setLabel("HotelID");
		hotelIDBox.setEnabled(false);

		TextField hotelNameBox = new TextField();
		hotelNameBox.setValue(hotelVonUser.get(0).getHotelName());
		hotelNameBox.setLabel("Hotelname");
		hotelNameBox.setEnabled(false);
		
		
		Select<Integer> monthSelect = new Select<>();
		monthSelect.setItems(OccupancyService.getAllOccupancyMonths());
		monthSelect.setLabel("Monat");
		
		
		Select<Integer> yearSelect = new Select<>();
		yearSelect.setItems(2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021);
		yearSelect.setLabel("Jahr");
	
		TextField HotelName = new TextField("Hotel Name");

		NumberField anzZimmer = new NumberField("Anz. Zimmer");
		
		NumberField anzBelZim = new NumberField("Anz. belegter Zimmer");
		
		NumberField anzBetten = new NumberField("Anz. Betten");
		
		NumberField anzBelBet = new NumberField("Anz. belegter Betten");
		
		NumberField anzNational = new NumberField("Anz. Nationalitäten");

		TextField nationalitaeten = new TextField("Nationalitäten");
		
		NumberField anzGenesene = new NumberField("Anz. Genesene");

		NumberField anzGetestet = new NumberField("Anz. Getestete");
		
		NumberField anzGeimpfte = new NumberField("Anz. Geimpfte");
		
		Checkbox sicherCheckbox = new Checkbox();
		sicherCheckbox.setLabel("Ich stimme den Änderungen zu");
		sicherCheckbox.setValue(false);
		

		horizontalLayout.add(monthSelect, yearSelect);
		
		horizontalLayout2.add(hotelIDBox,hotelNameBox);
		
		horizontalLayout3.add(anzZimmer, anzBelZim);
		
		horizontalLayout4.add(anzBetten, anzBelBet);
		
		horizontalLayout5.add(anzNational, nationalitaeten);
		
		horizontalLayout6.add(anzGenesene, anzGetestet, anzGeimpfte);
		
		verticalLayout.add(horizontalLayout, horizontalLayout2, horizontalLayout3, horizontalLayout4, horizontalLayout5, horizontalLayout6, sicherCheckbox);
		
		
		yearSelect.addValueChangeListener(Event -> {
			
			if(yearSelect.getValue() != null && monthSelect.getValue() != null) {
				if(hotelIDBox != null && yearSelect.getValue() != null && monthSelect.getValue() != null) {
					try {
					int StorageID = hotelIDBox.getValue();
					horizontalLayout2.removeAll();
					horizontalLayout2.add(hotelIDBox, getHotelName(StorageID));
					Occupancy occupancyNew = OccupancyService.OccupancyById(hotelIDBox.getValue(), monthSelect.getValue(), yearSelect.getValue());
					anzZimmer.setValue((double) occupancyNew.getNoRooms());
					anzBelZim.setValue((double) occupancyNew.getUsedRooms());
					anzBetten.setValue((double) occupancyNew.getNoBeds());
					anzBelBet.setValue((double) occupancyNew.getUsedBeds());
					anzNational.setValue((double) occupancyNew.getAzNationalitäten());
					nationalitaeten.setValue( occupancyNew.getNationalitäten());
					anzGeimpfte.setValue((double) occupancyNew.getAzGeimpft());
					anzGetestet.setValue((double) occupancyNew.getAzGetestet());
					anzGenesene.setValue((double) occupancyNew.getAzGenesen());

				} catch (SQLException e) {
					e.printStackTrace();
				}}else {
					Notification.show("Bitte Jahr und Monat ausfüllen");
				}}
		});
		
		monthSelect.addValueChangeListener(Event -> {
			
			if(yearSelect.getValue() != null && monthSelect.getValue() != null) {
				if(hotelIDBox != null && yearSelect.getValue() != null && monthSelect.getValue() != null) {
					try {
					int StorageID = hotelIDBox.getValue();
					horizontalLayout2.removeAll();
					horizontalLayout2.add(hotelIDBox, getHotelName(StorageID));
					Occupancy occupancyNew = OccupancyService.OccupancyById(hotelIDBox.getValue(), monthSelect.getValue(), yearSelect.getValue());
					anzZimmer.setValue((double) occupancyNew.getNoRooms());
					anzBelZim.setValue((double) occupancyNew.getUsedRooms());
					anzBetten.setValue((double) occupancyNew.getNoBeds());
					anzBelBet.setValue((double) occupancyNew.getUsedBeds());
					anzNational.setValue((double) occupancyNew.getAzNationalitäten());
					nationalitaeten.setValue( occupancyNew.getNationalitäten());
					anzGeimpfte.setValue((double) occupancyNew.getAzGeimpft());
					anzGetestet.setValue((double) occupancyNew.getAzGetestet());
					anzGenesene.setValue((double) occupancyNew.getAzGenesen());

				} catch (SQLException e) {
					e.printStackTrace();
				}} else {
						Notification.show("Bitte ID und Jahr ausfüllen");
					}}
		});
		
		
		hotelIDBox.addValueChangeListener( event -> {
			
			if(hotelIDBox != null && yearSelect.getValue() != null && monthSelect.getValue() != null) {
				try {
				int StorageID = hotelIDBox.getValue();
				horizontalLayout2.removeAll();
				horizontalLayout2.add(hotelIDBox, getHotelName(StorageID));
				Occupancy occupancyNew = OccupancyService.OccupancyById(hotelIDBox.getValue(), monthSelect.getValue(), yearSelect.getValue());
				anzZimmer.setValue((double) occupancyNew.getNoRooms());
				anzBelZim.setValue((double) occupancyNew.getUsedRooms());
				anzBetten.setValue((double) occupancyNew.getNoBeds());
				anzBelBet.setValue((double) occupancyNew.getUsedBeds());
				anzNational.setValue((double) occupancyNew.getAzNationalitäten());
				nationalitaeten.setValue( occupancyNew.getNationalitäten());
				anzGeimpfte.setValue((double) occupancyNew.getAzGeimpft());
				anzGetestet.setValue((double) occupancyNew.getAzGetestet());
				anzGenesene.setValue((double) occupancyNew.getAzGenesen());

			} catch (SQLException e) {
				e.printStackTrace();
			}
			} else {
				Notification.show("Bitte Jahr und Monat ausfüllen");
			}}
		);
		
		dialog.add(verticalLayout);
		
		Button addButton = new Button("Daten bearbeiten", Event -> {
			
			boolean check = false;
			int counter = 0;
			
			String ausgabe = "";
			
			if(!(monthSelect.isEmpty())) {
				int month = monthSelect.getValue();
				}
				else {
					counter ++;
					ausgabe += "Monat ausfüllen! \n";
				}
				
			if(!(yearSelect.isEmpty())) {
				int year = yearSelect.getValue();
				}
				else {
					counter++;
					ausgabe += "Jahr ausfüllen! \n";
				}
				
				if(!(anzBelZim.isEmpty())) {
					Double anzBelZimmer = anzBelZim.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Belegter Zimmer ausfüllen! \n";
					}

				if(!(anzZimmer.isEmpty())) {
					Double anzZimmer1 = anzZimmer.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Zimmer ausfüllen! \n";
					}

				if(!(anzBelBet.isEmpty())) {
					Double anzBelBetten = anzBelBet.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl belegter Betten ausfüllen! \n";
					}
				if(!(anzBetten.isEmpty())) {
					Double anzBetten1 = anzBetten.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Betten ausfüllen! \n";
					}
				
				if(!(anzNational.isEmpty())) {
					Double anzNati = anzNational.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Nationalitäten ausfüllen! \n";
					}
				
				if(!(nationalitaeten.isEmpty())) {
					String natio = nationalitaeten.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Nationalitäten ausfüllen! \n";
					}
				if(!(anzGenesene.isEmpty())) {
					Double genesen = anzGenesene.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Genesene ausfüllen! \n";
					}
				
				if(!(anzGetestet.isEmpty())) {
					Double genesen = anzGetestet.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Getestete ausfüllen! \n";
					}
				
				if(!(anzGeimpfte.isEmpty())) {
					Double genesen = anzGeimpfte.getValue();
				 	}	
					else {
						counter++;
						ausgabe += "Anzahl Geimpfte ausfüllen! \n";
					}
				if (sicherCheckbox.getValue()== false) {
					counter++;
					ausgabe+= "Bestätigen Sie die Angaben"; }

			if (counter ==0)	{
				check = true;
				ausgabe = "";
				
			}
			
			
			if(check) {
				double zimmerauslastung = (anzBelZim.getValue() / anzZimmer.getValue());
				double bettenauslastung = (anzBelBet.getValue() / anzBetten.getValue());


				int month = monthSelect.getValue();
				
				int year = yearSelect.getValue();
				
				int id = hotelIDBox.getValue();
				
				Double zwischen4 = anzBelZim.getValue();
				int anzBelZimmer = (int) Math.round(zwischen4);
				
				Double zwischen5 = anzZimmer.getValue();
				int anzZimmer1 = (int) Math.round(zwischen5);
				
				Double zwischen6 = anzBelBet.getValue();
				int anzBelBetten = (int) Math.round(zwischen6);

				Double zwischen7 = anzBetten.getValue();
				int anzBetten1 = (int) Math.round(zwischen7);
				
				Double zwischen8 = anzNational.getValue();
				int anzNati = (int) Math.round(zwischen8);
				
				String natio = nationalitaeten.getValue();
				 
				Double zwischen1 = anzGenesene.getValue();
				int genesen = (int) Math.round(zwischen1);
				 	
				Double zwischen2 = anzGetestet.getValue();
				int getestet = (int) Math.round(zwischen2);
				
				Double zwischen3 = anzGeimpfte.getValue();
				int geimpft = (int) Math.round(zwischen3);
				 

				System.out.printf(id + " "+  anzZimmer1 + " "+  anzBelZimmer +  " "+ anzBetten1 +  " "+ 
					anzBelBetten +  " "+ year +  " "+ month +  " "+ zimmerauslastung +  " "+ bettenauslastung +  " "+ anzNati +  " "+ natio +
					" "+ genesen + " "+  getestet + " "+ geimpft);
				

				Occupancy occupancy = new Occupancy(id, anzZimmer1, anzBelZimmer, anzBetten1, 
						anzBelBetten, year, month, zimmerauslastung, bettenauslastung, anzNati, natio,
						genesen, getestet, geimpft);
				
				try {
					System.out.println("UPDATING");
					OccupancyService.updateOccupancyByID(id, month, year, occupancy);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				
				
				Notification.show("Transaktionsdaten bearbeitet");
				
						dialog.close();
						
			} else if(!check) {
				
				Notification.show("Bitte alle Daten ausfüllen");
				Notification.show(ausgabe);
				ausgabe = "";
			}
			
		});
		addButton.setClassName("hilfeButtons");
		Button closeButton = new Button("Abbrechen", Event -> {
			
			Notification.show("Abgebrochen");
				dialog.close();
});
		closeButton.setClassName("hilfeButtons");
		HorizontalLayout hori3 = new HorizontalLayout();
		
		hori3.add(addButton, closeButton);
		
		verticalLayout.add(hori3);
		
		dialog.add(verticalLayout);
		
		return dialog;
		
	}

	/**
	 * erstellt eine comboBox mit allen hotelIds
	 * @return
	 * @throws SQLException
	 */
	public ComboBox<Integer> getHotelID() throws SQLException {
		
		ComboBox<Integer> comboBox = new ComboBox<>();
		comboBox.setLabel("Hotel ID");
		comboBox.setPlaceholder("Hotel ID");
	
		ArrayList<Integer> ids = HotelService.getHotelID();
		Collections.sort(ids);
		
		comboBox.setItems(ids);

		return comboBox;
	}
	
	/**
	 * erstellt ein TextField mit einem hotelnamen, der der übergebenen hotelID zugeordnet ist
	 * @param i
	 * @return
	 * @throws SQLException
	 */
	static TextField getHotelName(int i) throws SQLException {
	
		String name = "";
		
		name = HotelService.getHotelNameById(i); 
		System.out.println(name);
		TextField hotelnameField = new TextField(name);
		hotelnameField.setLabel("Hotel Name");
		hotelnameField.setValue(name);
		
		
		return hotelnameField;
	}
} 

