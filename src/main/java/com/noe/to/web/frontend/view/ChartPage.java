package com.noe.to.web.frontend.view;


import java.sql.SQLException;

import com.noe.to.web.backend.service.OccupancyService;
import com.noe.to.web.frontend.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;


@PageTitle("chartPage")
@Route(value = "ChartPage", layout = MainLayout.class)

public class ChartPage extends VerticalLayout{
	
	int monat= 1;
	int jahr= OccupancyService.getAllOccupancyYears().get(0);
	
	/**
	 * adds the Charts and the select to the Page
	 * @throws SQLException
	 */
	public ChartPage() throws SQLException {
	
		add(getChartTitel());
		add(setMonth());
		
	}

	/**
	 * die Überschrift der Seite wird erstellt
	 * @return
	 */
	private Component getChartTitel() {
		H1 Histo = new H1("Histogramm");
		return Histo;
	}

	/** 
	 * Die Progress Bar mit der Bettenauslastung wird erstellt
	 * @return
	 * @throws SQLException
	 */
	private Component getChart1 () throws SQLException {
		double bettenal = OccupancyService.getBedOccupancyPerMonth(jahr,monat);

		ProgressBar progressBar1 = new ProgressBar();
			progressBar1.setValue(bettenal);
			progressBar1.setHeight("2.5em");
			progressBar1.setWidth("15em");
		
		Label labelC1 = new Label(bettenal*100+"% Bettenauslastung");
		labelC1.addClassName("beschriftung");
		
		HorizontalLayout chart1 = new HorizontalLayout(progressBar1, labelC1);
		
		return chart1;
		}

	/**
	 * Die Progress bar mit der Zimmerauslastung wird erstellt
	 * @return
	 * @throws SQLException
	 */
	private Component getChart2 () throws SQLException {
		double zimmeral = OccupancyService.getRoomOccupancyPerMonth(jahr,monat);
	
		ProgressBar progressBar2 = new ProgressBar();
			progressBar2.setValue(zimmeral);
			progressBar2.setHeight("2.5em");
			progressBar2.setWidth("15em");
		
		Label labelC2 = new Label(zimmeral*100+"% Zimmerauslastung");
		labelC2.addClassName("beschriftung");
		
		HorizontalLayout chart2 = new HorizontalLayout(progressBar2, labelC2);
		
		return chart2;
		}

	/**
	 * zwei Selects werden erstellt für die auswahl des Monat und des Jahres
	 * @return
	 */
	public Component setMonth () {
		HorizontalLayout bettenHorizontalLayout = new HorizontalLayout();
		HorizontalLayout zimmerHorizontalLayout = new HorizontalLayout();
	
		Select<Integer> select = new Select<>();
		select.setLabel("Monat");
			
		try {
			select.setItems(OccupancyService.getAllOccupancyMonths());
		} catch (SQLException e) {
			e.printStackTrace();
		}

		select.addValueChangeListener (event -> {
			try {
				bettenHorizontalLayout.removeAll();
				zimmerHorizontalLayout.removeAll();
				bettenHorizontalLayout.add(getChart2());
				zimmerHorizontalLayout.add(getChart1());
		    
				monat = event.getValue();
		    
				add(bettenHorizontalLayout);
			
				add(zimmerHorizontalLayout);
			
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
		 });

		Select<Integer> select2 = new Select<>();
		select2.setLabel("Jahr");

		try {
			select2.setItems(OccupancyService.getAllOccupancyYears());
		} catch (SQLException f) {
			f.printStackTrace();
		}

		 select2.addValueChangeListener (event2 -> {
		    
		    try {
				bettenHorizontalLayout.removeAll();
				zimmerHorizontalLayout.removeAll();
				bettenHorizontalLayout.add(getChart2());
				zimmerHorizontalLayout.add(getChart1());
			    
				jahr = event2.getValue();
			    
				add(bettenHorizontalLayout);
				add(zimmerHorizontalLayout);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			 });
		 
	 HorizontalLayout monthChoosing = new HorizontalLayout(select, select2);
	 
	 return monthChoosing;

	}
	}
