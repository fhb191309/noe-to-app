package com.noe.to.web.frontend.view;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import com.noe.to.web.backend.entity.CombinedOverview;
import com.noe.to.web.backend.entity.Occupancy;
import com.noe.to.web.backend.service.CombinedOverviewService;
import com.noe.to.web.backend.service.HotelService;
import com.noe.to.web.backend.service.OccupancyService;
import com.noe.to.web.backend.service.TransDataService;
import com.noe.to.web.frontend.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;


@PageTitle("HomePage")
@Route(value = "HomePage", layout = MainLayout.class)

public class HomePage extends VerticalLayout{ 
	Grid<CombinedOverview> grid = new Grid<>(CombinedOverview.class);
	/**
	 * Konstuktor der Class HomePage
	 * fügt alle Komponenten der Seite hinzu
	 * @throws SQLException 
	 */
	public HomePage() throws SQLException{
		
		addClassName("HomePage");
		
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);

		add(getImage());
		
		add(getnews());
	
		Paragraph willkommenField = new Paragraph("Willkommen auf der Webseite der Niederösterreichischen "
				+ " Touristeninformation! Dem Land Niederösterreich ist es sehr wichtig, den Tourismus am Laufen"
				+ " zu halten. Hierfür nimmt es sich statistische Daten zur Hilfe, um mögliche Veränderungen "
				+ " vorauszusagen und aus der Vergangenheit Informationen zu sammeln, um die Zukunft zu verbessern."
				+ " Im Namen des Landes wird allen NutzerInnen gedankt, dass Sie dabei helfen Daten zu sammeln.");
		add(willkommenField);
		
		add(overviewGridComponent());
		add(addButtons());
	}	
	
	/**
	 * erstellt das Bild für die HomePage
	 * @return
	 */
	private Component getImage() {
		Image image1 = new Image("images/Weingarten_1.png", "Foto Weingarten");
		image1.setHeight("200px");
		
		return image1;
	}
	/**
	 * erstellt eine zweite Überschrift
	 * @return
	 */
	private VerticalLayout getnews() {
		H2 news = new H2("Neuigkeiten");
		
		 VerticalLayout vert = new VerticalLayout(news);
	        vert.addClassName("news");
	        return vert;
	}
	
	//Adds Buttons
	/**
	 * erstellt die buttons Stammdaten, Transaktionsdaen, Histogram und Recht vergeben
	 * fügt jeden dieser Buttons einen ClickListener hinzu, die auf die jeweilige Seite führen
	 * @return
	 */
	private HorizontalLayout addButtons() {

		Button BStamm = new Button("Stammdaten");
		BStamm.setClassName("hilfeButtons");
		Button BTrans = new Button("Transaktionsdaten");
		BTrans.setClassName("hilfeButtons");
		Button BHisto = new Button("Histogramm");
		BHisto.setClassName("hilfeButtons");

		HorizontalLayout hori = new HorizontalLayout(BStamm, BTrans, BHisto);
		
		hori.addClassName("toolbar");
		
	     BStamm.addClickListener(event -> {
	        	UI.getCurrent().navigate(MasterData.class);
	        });
	     
	     BTrans.addClickListener(event -> {
	        	UI.getCurrent().navigate(TransactionalData.class);
	        });
	     
	     BHisto.addClickListener(event -> {
	        	UI.getCurrent().navigate(ChartPage.class);
	        });
		
//	     BRecht.addClickListener(event -> {
//	        	
//	        });
	     
		return hori;
    }
	
	public Component overviewGridComponent () throws SQLException {
	   	
    	VerticalLayout verticalLayout = new VerticalLayout();
    	HorizontalLayout horizontalLayout = new HorizontalLayout();
    	
    	
		ComboBox<Integer> idSelect = new ComboBox<>();
		idSelect.setLabel("Hotel ID");
		idSelect.setPlaceholder("Hotel ID");
	
		ArrayList<Integer> ids = HotelService.getHotelID();
		Collections.sort(ids);
		idSelect.setItems(ids);
		
		ComboBox<Integer> monthSelect = new ComboBox<>();
		monthSelect.setItems(OccupancyService.getAllOccupancyMonths());
		monthSelect.setLabel("Monat");

		
		ComboBox<Integer> yearSelect = new ComboBox<>();
		yearSelect.setItems(2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021);
		yearSelect.setLabel("Jahr");
		
		ComboBox<String> katSelect = new ComboBox<>();
		katSelect.setItems("*", "**", "***", "****", "*****");
		katSelect.setLabel("Kategorie");
		
		Button suchenButton = new Button("Suchen");
		suchenButton.setHeight("68px");
		Button resetButton = new Button("Zurücksetzen");
		resetButton.setHeight("68px");
		
		grid.setItems(CombinedOverviewService.getCombinedOverview());
		
		horizontalLayout.add(idSelect, monthSelect, yearSelect, katSelect, suchenButton, resetButton);
    	
		resetButton.addClickListener(Event -> {
			
			idSelect.setValue(null);
			monthSelect.setValue(null);
			yearSelect.setValue(null);
			katSelect.setValue(null);
			
			grid.addClassName("overview-grid");
		        try { 
		        	grid.setItems(CombinedOverviewService.getCombinedOverview());
		        } catch (SQLException e) {

					e.printStackTrace();
				}
		    	int hotelID;
		    	String category;
		    	String hotelName;
		    	String hotelOwner;
		    	String contact;
		    	String address;
		    	String city;
		    	String cityCode;
		    	String phone;
		    	int noRooms;
		    	int noBeds;
		    	String kinderfreundlich;
		    	String hundefreundlich;
		    	String spa;
		    	String fitness;
		    	
		
		       
		        grid.setColumns("hotelID", "category", "hotelName", "hotelOwner", "contact", 
		        		"address", "city", "cityCode", "phone", "noRooms", "noBeds", 
		        		"kinderfreundlich", "hundefreundlich", "spa", "fitness",
		        		"hotelID2", "noRooms2", "usedRooms", "noBeds2", "usedBeds",
		        		"year", "month", "zimmerauslastung", "bettenauslastung",
		        		"azNationalitäten", "nationalitäten", "azGenesen", "azGetestet", "azGeimpft" );
		        grid.getColumns().forEach(col -> col.setAutoWidth(true));
		        grid.setMaxHeight("240px");
			
		});
		
		suchenButton.addClickListener(Event -> {
			
			if(idSelect.isEmpty() &&  monthSelect.isEmpty() && yearSelect.isEmpty()) {
		        grid.addClassName("overview-grid");
		        try {
					grid.setItems(CombinedOverviewService.getCombinedOverviewByCategory(katSelect.getValue()));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		        grid.setColumns("hotelID", "category", "hotelName", "hotelOwner", "contact", 
		        		"address", "city", "cityCode", "phone", "noRooms", "noBeds", 
		        		"kinderfreundlich", "hundefreundlich", "spa", "fitness",
		        		"hotelID2", "noRooms2", "usedRooms", "noBeds2", "usedBeds",
		        		"year", "month", "zimmerauslastung", "bettenauslastung",
		        		"azNationalitäten", "nationalitäten", "azGenesen", "azGetestet", "azGeimpft" );
		        grid.getColumns().forEach(col -> col.setAutoWidth(true));
		        grid.setMaxHeight("130px");
				
		        //--------------------------------------------------------------------
		        
			} else if(katSelect.isEmpty() &&  monthSelect.isEmpty() && yearSelect.isEmpty() ) {
		        grid.addClassName("overview-grid");
		        try {
					grid.setItems(CombinedOverviewService.getCombinedOverviewByID(idSelect.getValue()));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		        grid.setColumns("hotelID", "category", "hotelName", "hotelOwner", "contact", 
		        		"address", "city", "cityCode", "phone", "noRooms", "noBeds", 
		        		"kinderfreundlich", "hundefreundlich", "spa", "fitness",
		        		"hotelID2", "noRooms2", "usedRooms", "noBeds2", "usedBeds",
		        		"year", "month", "zimmerauslastung", "bettenauslastung",
		        		"azNationalitäten", "nationalitäten", "azGenesen", "azGetestet", "azGeimpft" );
		      
		        grid.getColumns().forEach(col -> col.setAutoWidth(true));
		        grid.setMaxHeight("240px");
				
		        //--------------------------------------------------------------------

		        
			} else if(katSelect.isEmpty() &&  monthSelect.isEmpty()) {
	        grid.addClassName("overview-grid");
	        try {
				grid.setItems(CombinedOverviewService.getCombinedOverviewByIdAndYear(idSelect.getValue(), yearSelect.getValue()));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        grid.setColumns("hotelID", "category", "hotelName", "hotelOwner", "contact", 
	        		"address", "city", "cityCode", "phone", "noRooms", "noBeds", 
	        		"kinderfreundlich", "hundefreundlich", "spa", "fitness",
	        		"hotelID2", "noRooms2", "usedRooms", "noBeds2", "usedBeds",
	        		"year", "month", "zimmerauslastung", "bettenauslastung",
	        		"azNationalitäten", "nationalitäten", "azGenesen", "azGetestet", "azGeimpft" );
	      
	        grid.getColumns().forEach(col -> col.setAutoWidth(true));
	        grid.setMaxHeight("240px");
			
	        //--------------------------------------------------------------------

	        
			} else if(katSelect.isEmpty()) {
	        grid.addClassName("overview-grid");
	        try {
				grid.setItems(CombinedOverviewService.getCombinedOverviewByIdAndMonth(idSelect.getValue(), monthSelect.getValue(), yearSelect.getValue()));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        grid.setColumns("hotelID", "category", "hotelName", "hotelOwner", "contact", 
	        		"address", "city", "cityCode", "phone", "noRooms", "noBeds", 
	        		"kinderfreundlich", "hundefreundlich", "spa", "fitness",
	        		"hotelID2", "noRooms2", "usedRooms", "noBeds2", "usedBeds",
	        		"year", "month", "zimmerauslastung", "bettenauslastung",
	        		"azNationalitäten", "nationalitäten", "azGenesen", "azGetestet", "azGeimpft" );
	      
	        grid.getColumns().forEach(col -> col.setAutoWidth(true));
	        grid.setMaxHeight("240px");
			
	        //-------------------------------------------------------------------- 

	        
		} else if(idSelect.isEmpty()) {
	        grid.addClassName("overview-grid");
	        try {
				grid.setItems(CombinedOverviewService.getCombinedOverviewByCategoryAndMonth(katSelect.getValue(), monthSelect.getValue(), yearSelect.getValue()));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        grid.setColumns("hotelID", "category", "hotelName", "hotelOwner", "contact", 
	        		"address", "city", "cityCode", "phone", "noRooms", "noBeds", 
	        		"kinderfreundlich", "hundefreundlich", "spa", "fitness",
	        		"hotelID2", "noRooms2", "usedRooms", "noBeds2", "usedBeds",
	        		"year", "month", "zimmerauslastung", "bettenauslastung",
	        		"azNationalitäten", "nationalitäten", "azGenesen", "azGetestet", "azGeimpft" );
	        grid.getColumns().forEach(col -> col.setAutoWidth(true));
	        grid.setMaxHeight("240px");
			
			
		} else {//if(idSelect.isEmpty() &&  monthSelect.isEmpty() && yearSelect.isEmpty() && katSelect.isEmpty()) {
	        grid.addClassName("overview-grid");
	        try { 
	        	 grid.setItems(CombinedOverviewService.getCombinedOverview());
	        } catch (SQLException e) {

				e.printStackTrace();
			}
	     
	       
	        grid.setColumns("hotelID", "category", "hotelName", "hotelOwner", "contact", 
	        		"address", "city", "cityCode", "phone", "noRooms", "noBeds", 
	        		"kinderfreundlich", "hundefreundlich", "spa", "fitness",
	        		"hotelID2", "noRooms2", "usedRooms", "noBeds2", "usedBeds",
	        		"year", "month", "zimmerauslastung", "bettenauslastung",
	        		"azNationalitäten", "nationalitäten", "azGenesen", "azGetestet", "azGeimpft" ); 
	      
	        grid.getColumns().forEach(col -> col.setAutoWidth(true));
	        grid.setMaxHeight("240px");
   
			
			}
		});
		
		verticalLayout.add(horizontalLayout,grid);
		
		grid.getColumns().forEach(col -> col.setAutoWidth(true));
		return verticalLayout;
	}



}
