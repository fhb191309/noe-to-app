package com.noe.to.web.frontend;


import java.util.ArrayList;

import com.noe.to.web.frontend.view.ChartPage;
import com.noe.to.web.frontend.view.HelpPage;
import com.noe.to.web.frontend.view.HomePage;
import com.noe.to.web.frontend.view.MasterData;
import com.noe.to.web.frontend.view.TransactionalData;
import com.noe.to.web.frontend.view.logins.LoginPageSeniorUser;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class FrontendUtils {


	/**
	 * erstellt die buttons Home, Stammdaten, Transaktionsdaten, Histogram, hilfe und Logout und erstellt cklicklistener auf diese Buttons
	 * @return
	 */
	public static Component setNavButtons() {
		Button home = new Button("Home");
		home.setClassName("hilfeButtons");
		Button stamm = new Button("Stammdaten");
		stamm.setClassName("hilfeButtons");
		Button trans = new Button("Transaktionsdaten");
		trans.setClassName("hilfeButtons");
		Button histo = new Button("Histogramm");
		histo.setClassName("hilfeButtons");
		
		Button help = new Button("Hilfe");
		help.setClassName("hilfeButtons");
		Button logout = new Button("Abmelden");
		logout.setClassName("hilfeButtons");
		
      
		//help.getElement().getStyle().set("margin-left", "auto");
		HorizontalLayout hori2 = new HorizontalLayout(help, logout);
		hori2.getElement().getStyle().set("margin-left", "auto");

		
        HorizontalLayout hori = new HorizontalLayout(home, stamm, trans, histo, hori2);
        hori.addClassName("toolbar");
        hori.setWidthFull();
        hori.setAlignItems(FlexComponent.Alignment.CENTER);
       
        
        home.addClickListener(event -> {
        	
        	UI.getCurrent().navigate(HomePage.class);
        });
        
        stamm.addClickListener(event -> {
        	UI.getCurrent().navigate(MasterData.class);
        });
        
        trans.addClickListener(event -> {
        	UI.getCurrent().navigate(TransactionalData.class);
        });
        
        histo.addClickListener(event -> {
        	UI.getCurrent().navigate(ChartPage.class);
        });
        
        help.addClickListener(event -> {
        	UI.getCurrent().navigate(HelpPage.class);
       });
        
        logout.addClickListener(event -> {
        	UI.getCurrent().navigate(LoginPageSeniorUser.class);
        });
        return hori;
		
	}
	static ArrayList <Integer> HotelOwnerID = new ArrayList<>();
	
	public static void setSessionID (int ID) {
		HotelOwnerID.add(ID);
	}
	
	public static int getSessionID () {
		int sessionId = 0;
		sessionId = HotelOwnerID.get(HotelOwnerID.size()-1);
		return sessionId; 
	}
	
}
