package com.noe.to.web.frontend.view.HotelOwnerViews;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

@CssImport("./styles/shared-styles.css")

public class MainLayoutOwner extends AppLayout{
	
	/**
	 * Erstellt die NavBar, damit diese am Anang jeder Seite angezeigt werden kann
	 * in der NavBar enthalten sind die Überschrift, das Bild und die Buttons von FrontendUtils
	 */
	public MainLayoutOwner() {
		

		H1 NoTo = new H1("Niederösterreichische Touristeninformation");
		NoTo.addClassName("ueberschrift");
		
		Image image = new Image("images/2021-Logo.png", "Logo");
		image.setHeight("55px");
		
				
		 HorizontalLayout header = new HorizontalLayout(image, NoTo);
	        header.addClassName("ueberschrift");
	        header.setWidth("100%");
	        header.expand(NoTo);
		
		VerticalLayout verticalLayout = new VerticalLayout(header, FrontendUtilsOwner.setNavButtons());
		addToNavbar(verticalLayout);
	
	}
}

	

