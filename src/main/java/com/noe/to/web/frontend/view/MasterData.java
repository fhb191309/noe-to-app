package com.noe.to.web.frontend.view;

import java.awt.List;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.WeakHashMap;

import javax.mail.MessagingException;

import com.noe.to.web.backend.entity.Hotel;
import com.noe.to.web.backend.entity.MasterDataOverview;
import com.noe.to.web.backend.service.EmailService;
import com.noe.to.web.backend.service.HotelService;
import com.noe.to.web.backend.service.MasterDataService;
import com.noe.to.web.backend.service.OccupancyService;
import com.noe.to.web.frontend.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;



@PageTitle("Masterdata") 
@Route(value = "Masterdata", layout = MainLayout.class)


public class MasterData extends VerticalLayout{
	
	Grid<MasterDataOverview> masterDataOverviewGrid = new Grid<>(MasterDataOverview.class);
	Grid<Hotel> grid = new Grid<>(Hotel.class);
	HotelService hotelService = new HotelService();
	MasterDataService masterDataService = new MasterDataService();
	
	Grid<Hotel> table = new Grid<>();
	ArrayList<Hotel> hotels = hotelService.getAllHotels();
	
	Binder<Hotel> binder = new Binder<>(Hotel.class);
	Editor<Hotel> editor = grid.getEditor();
	

	/**
	 * Konstruktor der Klasse MasterData, fügt alle Komponenten der Seite hinzu
	 * @throws SQLException
	 */
	public MasterData() throws SQLException {
	
		
		add(getTitle());
		 
		add(generalOverviewTitle());
		
		add(configureHotelOverviewGrid());
		
		add(getSubTitle());
		
		Div content = new Div(grid);
        content.addClassName("content");
        content.setSizeFull();
		add(configureHotelGrid());
		
		getDefaultHorizontalComponentAlignment();
		
		
		add(getButtons());
		

	}
	/**
	 * erstellt die Überschrift H1 und den Button Backup 
	 * @return
	 */
	//Add the titel and two buttons
	private Component getTitle() {
		H1 TStamm = new H1("Stammdaten");
		
		
		HorizontalLayout hori = new HorizontalLayout(TStamm);
        hori.addClassName("hotel_ue");
        hori.setWidthFull();

        hori.setAlignItems(FlexComponent.Alignment.CENTER);
   
        return hori;
		
	
	}
	
	/**
	 * erstellt die zweite überschrift H2
	 * @return
	 */
	private Component getSubTitle() {
		
		H2 title = new H2("Hotelübersicht");
		
		return title;
	}

	/**
	 * erstellt den Titel für die allgemeine Übersicht und den Button Stammdaten senden
	 * @return
	 */
	//adds the title for the overview
	private Component generalOverviewTitle() {
		H2 allg = new H2("Allgemeine Übersicht");
		
		Button sendStammButton = new Button("Stammdaten senden");
		Dialog StammSenden = new Dialog();
		
		Button cancel = new Button("Abbrechen", e -> {
		StammSenden.close();
		Notification.show("Daten wurden nicht gesendet");
		});
		
		Button confirm = new Button ("Bestätigen", e1 -> {
		StammSenden.close();
		//hier senden von Mail im Backend
		Notification.show("Daten gesendet");
		});
		
		Button sendKategorieButton = new Button("Unterkünfte senden");
		
		sendKategorieButton.addClickListener( Event -> {
			Unterkunftesenden();
		});
		
		HorizontalLayout hori1 = new HorizontalLayout(confirm,cancel);
		
		StammSenden.add(new Text("Wollen Sie die Daten wirklich senden?"));
		StammSenden.add(hori1);
		
		sendStammButton.addClickListener(event -> {
			StammSenden.open();
		});
		
		sendStammButton.setClassName("hilfeButtons");
		sendKategorieButton.setClassName("hilfeButtons");
		
		HorizontalLayout hori = new HorizontalLayout(sendStammButton, sendKategorieButton);
//		        hori.addClassName("h2");
		        hori.setWidthFull();
		        hori.setAlignItems(FlexComponent.Alignment.CENTER);
		        
		
		        VerticalLayout verticalLayout = new VerticalLayout(allg,hori);
		        
		      return verticalLayout;
		}
	
	private void Unterkunftesenden() {
		Dialog dialog = new Dialog();
		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);
		Span message = new Span();
		
		dialog.open();
		EmailField emailField = new EmailField("Empfänger");
		
		Select<String> katListBox = new Select<>();
		katListBox.setItems("*", "**", "***", "****", "*****");
		katListBox.setLabel("Kategorie");
		
		Checkbox kinderCheckBox = new Checkbox();
		kinderCheckBox.setLabel("Kinderfreundlich");
		kinderCheckBox.setValue(false);
		
		Checkbox hundeCheckbox = new Checkbox();
		hundeCheckbox.setLabel("Hundefreundlich");
		hundeCheckbox.setValue(false);
		
		Checkbox spaCheckbox = new Checkbox();
		spaCheckbox.setLabel("Spa");
		spaCheckbox.setValue(false);
		
		Checkbox fitnessCheckbox = new Checkbox();
		fitnessCheckbox.setLabel("Fitness");
		fitnessCheckbox.setValue(false);
		
		
		VerticalLayout verticalLayout = new VerticalLayout(emailField, katListBox, kinderCheckBox,hundeCheckbox,spaCheckbox,fitnessCheckbox);
		dialog.add(verticalLayout);
		
		Button abbreButton = new Button("Abbrechen");
		abbreButton.setClassName("hilfeButtons");
		abbreButton.addClickListener(event -> {
			dialog.close();
		});
		
		Button bestaetigen = new Button("Datein senden");
		bestaetigen.setClassName("hilfeButtons");
		bestaetigen.addClickListener(event -> {
			
			int kinderfreundlich = 0;
			if (kinderCheckBox.getValue() == null) {
				 kinderfreundlich = 0;
			    } else {
			     kinderfreundlich = 1;
			    }
			 
			int hundefreundlich = 0;
			if (hundeCheckbox.getValue() == null) {
				 hundefreundlich = 0;
			    } else {
			     hundefreundlich = 1;
			    }
			 
			int spa = 0;
			if (spaCheckbox.getValue() == null) {
				 spa = 0;
			    } else {
			     spa = 1;
			    }
			
			int fitness = 0;
			if (fitnessCheckbox.getValue() == null) {
				 fitness = 0;
			    } else {
			     fitness = 1;
			    }
			try {
				dialog.close();
				Notification.show("Daten gesendet!");
				EmailService.emailAnTouristOffices(emailField.getValue(), katListBox.getValue(), kinderfreundlich, hundefreundlich, spa, fitness);
			} catch (MessagingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			dialog.close();
		
		});
		VerticalLayout verticalLayout1 = new VerticalLayout(bestaetigen, abbreButton);
		dialog.add(verticalLayout1);
		
	}
	/**
	 * erstellt die allgemeine Übericht als Grid
	 * @return
	 * @throws SQLException
	 */
    private  Component configureHotelOverviewGrid() throws SQLException {
    	
        masterDataOverviewGrid.addClassName("overview-grid");
        masterDataOverviewGrid.setItems(masterDataService.getMasterDataOverview());
//        grid.setColumns( "Kategorie", "Anzahl Betriebe", "Anzahl Zimmer", "Anzahl Betten", "kinderfreundich", 
//        		"Hundefreundlich", "Spa", "fitness");
     
        masterDataOverviewGrid.setColumns( "kategorie", "azBetriebe", "azZimmer", "azBetten", "kinderfreundlich", 
        		"hundefreundlich", "spa", "fitness");
        //   masterDataOverviewGrid.getColumn("kategorie")
        masterDataOverviewGrid.getColumns().forEach(col -> col.setAutoWidth(true));
        masterDataOverviewGrid.setMaxHeight("240px");
        masterDataOverviewGrid.setHeightByRows(true);
      return masterDataOverviewGrid;
    }

    /**
     * erstellt das Grid mit allen Hotels, damit diese bearbeitbar sind
     * @return
     */
    private Component configureHotelGrid() {
    
    	table.setItems(hotels);
    	
    	table.setVerticalScrollingEnabled(true);
    	
    	Grid.Column<Hotel> id = table.addColumn(Hotel::getHotelID).setHeader("ID");
    	Grid.Column<Hotel> category = table.addColumn(Hotel::getCategory).setHeader("Kategorie");
    	Grid.Column<Hotel> name = table.addColumn(Hotel::getHotelName).setHeader("Name");
    	Grid.Column<Hotel> owner = table.addColumn(Hotel::getHotelOwner).setHeader("Besitzer");
    	Grid.Column<Hotel> contact = table.addColumn(Hotel::getContact).setHeader("Kontakt");
    	Grid.Column<Hotel> address = table.addColumn(Hotel::getAddress).setHeader("Adresse");
    	Grid.Column<Hotel> city = table.addColumn(Hotel::getCity).setHeader("Ort");
    	Grid.Column<Hotel> citycode = table.addColumn(Hotel::getCityCode).setHeader("PLZ");
    	Grid.Column<Hotel> phone = table.addColumn(Hotel::getPhone).setHeader("Telefon");
    	Grid.Column<Hotel> noRooms = table.addColumn(Hotel::getNoRooms).setHeader("Anz. Zimmer");
     	Grid.Column<Hotel> noBeds = table.addColumn(Hotel::getNoBeds).setHeader("Anz. Bettem");
     	Grid.Column<Hotel> kinderfreundlich = table.addColumn(Hotel::getKinderfreundlich).setHeader("kinderfreundlich");
     	Grid.Column<Hotel> hundefreundlich = table.addColumn(Hotel::getHundefreundlich).setHeader("Hundefreundlich");
     	Grid.Column<Hotel> spa = table.addColumn(Hotel::getSpa).setHeader("Spa");
     	Grid.Column<Hotel> fitness = table.addColumn(Hotel::getFitness).setHeader("Fitness");
     
     	id.setSortable(true);
     	category.setSortable(true);
     	name.setSortable(true);
     	owner.setSortable(true);
     	contact.setSortable(true); 
     	address.setSortable(true);
     	city.setSortable(true);
     	citycode.setSortable(true);
     	phone.setSortable(true);  
     	noRooms.setSortable(true); 
     	noBeds.setSortable(true);
     	kinderfreundlich.setSortable(true);
     	hundefreundlich.setSortable(true);
     	spa.setSortable(true); 
     	fitness.setSortable(true); 
     	
     	
    	Binder<Hotel> binder = new Binder<>(Hotel.class);
    	Editor<Hotel> editor = table.getEditor();
    	editor.setBinder(binder);
    	editor.setBuffered(true);
    	
        table.getColumns().forEach(col -> col.setAutoWidth(true));

    	Div validationStatus = new Div();
    	validationStatus.setId("validation");


    	TextField catField = new TextField();
    	binder.forField(catField).withStatusLabel(validationStatus).bind("category");
    	category.setEditorComponent(catField);
    	
    	TextField nameField = new TextField();
    	binder.forField(nameField).withStatusLabel(validationStatus).bind("hotelName");
    	name.setEditorComponent(nameField);
    	
    	TextField ownerField = new TextField();
    	binder.forField(ownerField).withStatusLabel(validationStatus).bind("hotelOwner");
    	owner.setEditorComponent(ownerField);
    	
    	TextField contactField = new TextField();
    	binder.forField(contactField).withStatusLabel(validationStatus).bind("contact");
    	contact.setEditorComponent(contactField);
    	
    	TextField addressField = new TextField();
    	binder.forField(addressField).withStatusLabel(validationStatus).bind("address");
    	address.setEditorComponent(addressField);
    	
    	TextField ortField = new TextField();
    	binder.forField(ortField).withStatusLabel(validationStatus).bind("city");
    	city.setEditorComponent(ortField);
    	
    	TextField codeField = new TextField();
    	binder.forField(codeField).withStatusLabel(validationStatus).bind("cityCode");
    	citycode.setEditorComponent(codeField);
    	
    	TextField phoneField = new TextField();
    	binder.forField(phoneField).withStatusLabel(validationStatus).bind("phone");
    	phone.setEditorComponent(phoneField);
    	
    	IntegerField noRoomsField = new IntegerField();
    	binder.forField(noRoomsField).withStatusLabel(validationStatus).bind("noRooms");
    	noRooms.setEditorComponent(noRoomsField);
    	
    	IntegerField noBedsField = new IntegerField();
    	binder.forField(noBedsField).withStatusLabel(validationStatus).bind("noBeds");
    	noBeds.setEditorComponent(noBedsField);
    	
    	TextField kinderField = new TextField();
    	binder.forField(kinderField).withStatusLabel(validationStatus).bind("kinderfreundlich");
    	kinderfreundlich.setEditorComponent(kinderField);

    	TextField hundeField = new TextField();
    	binder.forField(hundeField).withStatusLabel(validationStatus).bind("hundefreundlich");
    	hundefreundlich.setEditorComponent(hundeField);

    	TextField spaField = new TextField();
    	binder.forField(spaField).withStatusLabel(validationStatus).bind("spa");
    	spa.setEditorComponent(spaField);

    	TextField fitnessField = new TextField();
    	binder.forField(fitnessField).withStatusLabel(validationStatus).bind("fitness");
    	fitness.setEditorComponent(fitnessField);
    	
    	
    	Collection<Button> editButtons = Collections
    	        .newSetFromMap(new WeakHashMap<>());

    	Grid.Column<Hotel> editorColumn = table.addComponentColumn(event -> {
    	    Button edit = new Button("Edit");
    	    edit.setClassName("hilfeButtons");
    	    edit.addClassName("edit");
    	    edit.addClickListener(e -> {
    	        editor.editItem(event);
    	       // idField.focus();
    	    });
    	    edit.setEnabled(!editor.isOpen());
    	    editButtons.add(edit);
    	    return edit;
    	});

    	editor.addOpenListener(e -> editButtons.stream()
    	        .forEach(button -> button.setEnabled(!editor.isOpen())));
    	editor.addCloseListener(e -> editButtons.stream()
    	        .forEach(button -> button.setEnabled(!editor.isOpen())));

    	Button save = new Button("Save", e -> {
    		
    		int ID_ = -1;
			try {
				ID_ = HotelService.getHotelIdbyName(nameField.getValue());
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    		
			Hotel hotel = new Hotel(ID_, catField.getValue(), nameField.getValue(), ownerField.getValue(), contactField.getValue(),
					addressField.getValue(), ortField.getValue(), codeField.getValue(),phoneField.getValue(), noRoomsField.getValue(), noBedsField.getValue(),
					kinderField.getValue(), hundeField.getValue(), spaField.getValue(), fitnessField.getValue());


			System.out.println(hotel);
			
			
			HotelService.updateHotelByID(ID_, hotel);
			
			editor.save();
			
			Notification.show("Hotel gespeichert");
    	});
    	save.setClassName("hilfeButtons");

    	Button cancel = new Button("Cancel", e -> editor.cancel());
    	cancel.addClassName("cancel");
    	cancel.setClassName("hilfeButtons");

    	table.getElement().addEventListener("keyup", event -> editor.cancel())
    	        .setFilter("event.key === 'Escape' || event.key === 'Esc'");

    	Div buttons = new Div(save, cancel);
    	editorColumn.setEditorComponent(buttons);

    
    	add(validationStatus, table);
    	
		return table;
    }

    /**
     * erstellt die buttons hotel hinzufügen, Hotel bearbeiten und hotel löschen
     * belegt diese Buttons mit Clicklisteners
     * @return
     */
	private HorizontalLayout getButtons() {

		Button addHButton = new Button("Hotel hinzufügen");
		addHButton.setClassName("hilfeButtons");
		Button editHButton = new Button("Hotel bearbeiten");
		editHButton.setClassName("hilfeButtons");
		Button deleteHButton = new Button("Hotel löschen");
		deleteHButton.setClassName("hilfeButtons");
	
        HorizontalLayout hori = new HorizontalLayout(addHButton, editHButton, deleteHButton);
        hori.addClassName("buttons");
       
    	hori.getElement().getStyle().set("margin", "auto");
        
    	deleteHButton.addClickListener(Event -> {
    		
    		try {
				deleteHotel();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    		
    	});
		
		addHButton.addClickListener(Click -> {
			try {
				addHotel();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
		
		editHButton.addClickListener(Click -> {
			try {
				editHotel();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
    	
        return hori;
    }
	
	/**
	 * Methode die ausgeführt wird wenn der Hotel löschen Button geclickt wird
	 * öffnet ein dialog Window um ein hotel zu löschen
	 * @return
	 * @throws SQLException
	 */
	private Component deleteHotel() throws SQLException {
		Dialog dialog = new Dialog();
		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);
		Span message = new Span();
		
		dialog.open();
		
		VerticalLayout verticalLayout = new VerticalLayout(); 
		
		ComboBox<Integer> hotelIDBox = new ComboBox<>();
		hotelIDBox.setLabel("Hotel ID");
		hotelIDBox.setPlaceholder("Hotel ID");
	
		ArrayList<Integer> ids = HotelService.getHotelID();
		Collections.sort(ids);
		
		hotelIDBox.setItems(ids);
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();

		
		TextField HotelName = new TextField("Hotel Name");

	
		horizontalLayout.add(hotelIDBox);

	
		hotelIDBox.addValueChangeListener( event -> {
		try {
			int StorageID = hotelIDBox.getValue();
			horizontalLayout.removeAll();
			horizontalLayout.add(hotelIDBox, TransactionalData.getHotelName(StorageID));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		});
	
		Button confirm = new Button("bestätigen");
		confirm.setClassName("hilfeButtons");
		Button close = new Button("abbrechen");
		close.setClassName("hilfeButtons");
		
		confirm.addClickListener(Event -> {
	
			HotelService.deleteHotelByID(hotelIDBox.getValue());
			OccupancyService.deleteOccupancyByID(hotelIDBox.getValue());
			dialog.close();
			Notification.show("Hotel gelöscht");
		});
		
		
		close.addClickListener(e -> {
			dialog.close();
			Notification.show("Abgebrochen");
		});
		
		HorizontalLayout horizontalLayout2 = new HorizontalLayout();
		
		horizontalLayout2.add(confirm, close);
		
		verticalLayout.add(horizontalLayout, horizontalLayout2);
		
		dialog.add(verticalLayout);
		
		return dialog;
	}

	/**
	 * 	 * Methode die ausgeführt wird wenn der Hotel hinzufügen Button geclickt wird
	 * öffnet ein dialog Window um ein hotel hinzuzufügen
	 * @return
	 * @throws SQLException
	 */
	//adds a dialog window to the addHotel button
	public Component addHotel() throws SQLException{
		
		Dialog dialog = new Dialog();
		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);
		Span message = new Span();
		
		dialog.open();
		
		VerticalLayout verticalLayout = new VerticalLayout();
		
		HorizontalLayout hori3 = new HorizontalLayout();
		
		TextField nameTextField = new TextField("Hotel Name");
		
		TextField ownerTextField = new TextField("Hotel Owner");
		
		hori3.add(nameTextField, ownerTextField);
		verticalLayout.add(hori3);
	
		
		Select<String> katListBox = new Select<>();
		katListBox.setItems("*", "**", "***", "****", "*****");
		katListBox.setLabel("Kategorie");
		
		
		TextField kontakTextField = new TextField("Kontakt");
		
		HorizontalLayout hori5 = new HorizontalLayout();
		
		hori5.add(katListBox, kontakTextField);
		verticalLayout.add(hori5);
		
		HorizontalLayout hori1 = new HorizontalLayout();
		
		TextField streetTextField = new TextField("Adresse");
		
		NumberField telephoneField = new NumberField("Telefonnummer");
		
		hori1.add(streetTextField, telephoneField);
		verticalLayout.add(hori1);
		
		HorizontalLayout hori2 = new HorizontalLayout();
		
		TextField ortTextField = new TextField("Ort");
		NumberField plzTextField = new NumberField("PLZ");
		hori2.add(ortTextField, plzTextField);
		verticalLayout.add(hori2);
		
		
		NumberField anzRTextField = new NumberField("Anzahl Räume");
		verticalLayout.add(anzRTextField);
		
		NumberField anzBetTextField = new NumberField("Anzahl Betten");
		verticalLayout.add(anzBetTextField);
		
		HorizontalLayout hori6 = new HorizontalLayout();

		Checkbox kinderCheckBox = new Checkbox();
		kinderCheckBox.setLabel("Kinderfreundlich");
		kinderCheckBox.setValue(false);
		
		Checkbox hundeCheckbox = new Checkbox();
		hundeCheckbox.setLabel("Hundefreundlich");
		hundeCheckbox.setValue(false);
		
		Checkbox spaCheckbox = new Checkbox();
		spaCheckbox.setLabel("Spa");
		spaCheckbox.setValue(false);
		
		Checkbox fitnessCheckbox = new Checkbox();
		fitnessCheckbox.setLabel("Fitness");
		fitnessCheckbox.setValue(false);
		
		hori6.add(kinderCheckBox, hundeCheckbox, spaCheckbox, fitnessCheckbox);
		verticalLayout.add(hori6);
		
		HorizontalLayout hori4 = new HorizontalLayout();
		Button addButton = new Button("Hotel hinzufügen", Event -> {
	
			boolean check = false;
			int counter = 0;
			
			String ausgabe = "";
			
			
			if(!(nameTextField.isEmpty())) {
				String name = nameTextField.getValue();
				}
				else {
					counter ++;
					ausgabe += "Name ausfüllen! \n";
				}
			if(!(ownerTextField.isEmpty())) {
				String owner = ownerTextField.getValue();
				}
				else {
					counter ++;
					ausgabe += "Besitzer ausfüllen! \n";
				}
				
			if(katListBox.getValue() !=null) {
				String kat = katListBox.getValue();
				}
				else {
					counter++;
					ausgabe += "Kategorie ausfüllen! \n";
				}
			
			if(!(kontakTextField.isEmpty())) {
				String kontakt = kontakTextField.getValue();
				}
				else {
					counter ++;
					ausgabe += "Kontakt ausfüllen! \n";
				}
				
			if(!(streetTextField.isEmpty())) {
				String adresse = streetTextField.getValue();
			 	}	
				else {
					counter++;
					ausgabe += "Adresse ausfüllen! \n";
				}
			
			String telephone = null;
			
			if(telephoneField.getValue() != null) {
				double telephoneDouble = telephoneField.getValue();
				telephone = "" + (int) telephoneDouble;
				}
				else {
					counter++;
					ausgabe += "Telefonnummer ausfüllen! \n";
				}
			
			if(!(ortTextField.isEmpty())) {
				String ort = ortTextField.getValue();
				}
				else {
					counter++;
					ausgabe += "Ort ausfüllen! \n";
				}
			
			String plz = null;
			
			if(plzTextField.getValue() != null) {
				double plzDouble = plzTextField.getValue();
				plz = "" + (int) plzDouble;
				}
				else {
					counter++;
					ausgabe += "Postleitzahl ausfüllen! \n";
				}
			
			int anzR = 0;
			int anzB = 0;
			
			if(anzRTextField.getValue() != null) {
				double anzRDouble = anzRTextField.getValue();
				anzR = (int) anzRDouble;
				}
				else {
					counter++;
					ausgabe += "Räume ausfüllen! \n";
				}
			if(anzBetTextField.getValue() != null) {
				double anzBDouble = anzBetTextField.getValue();
				anzB = (int) anzBDouble;
				
				}
				else {
					counter++;
					ausgabe += "Betten ausfüllen! \n";
				}
			
			String kinderfreundlich;
			String hundefreundlich;
			String spa;
			String fitness;
			
			if (kinderCheckBox.getValue() == null) {
				 
				 kinderfreundlich = "0";
				
			    } else {
			    	
			     kinderfreundlich = "1";
			       
			    }
			 
			if (hundeCheckbox.getValue() == null) {
			      
				 hundefreundlich = "0";
				 
			    } else {
			    	
			     hundefreundlich = "1";
			       
			    }
			 
			if (spaCheckbox.getValue() == null) {
			      
				 spa = "0";
				 
			    } else {
			    	
			     spa = "1";
			       
			    }
			 
			if (fitnessCheckbox.getValue() == null) {
			      
				 fitness = "0";
				 
			    } else {
			    	
			     fitness = "1";
			       
			    }
												
			if (counter ==0)	{
				check = true;
				ausgabe = "";
			}
			

			if(check) {
			
				Hotel hotel = new Hotel(katListBox.getValue(), nameTextField.getValue(), ownerTextField.getValue(), kontakTextField.getValue(),
						streetTextField.getValue(), ortTextField.getValue(), plz,telephone, anzR, anzB,kinderfreundlich,
					hundefreundlich, spa, fitness);

				try {
					HotelService.insertNewHotel(hotel);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
			Notification.show("Hotel hinzugefügt");
						dialog.close();
						
			} else if(!check) {
				
				Notification.show("Bitte alle Daten ausfüllen");
				Notification.show(ausgabe);
				ausgabe = "";
			}
		});
		
		addButton.setClassName("hilfeButtons");
		Button closeButton = new Button("Abbrechen", Event -> {
 
				Notification.show("Abgebrochen");
				dialog.close();
		});
		closeButton.setClassName("hilfeButtons");

		hori4.add(addButton, closeButton);
		verticalLayout.add(hori4);
		
		dialog.add(verticalLayout);
		return dialog;
	}
	

	public Component editHotel() throws SQLException {
		Dialog dialog = new Dialog();
		dialog.setCloseOnEsc(false);
		dialog.setCloseOnOutsideClick(false);
		Span message = new Span();
		
		dialog.open();
		
		VerticalLayout verticalLayout = new VerticalLayout();
		HorizontalLayout hori1 = new HorizontalLayout();
		HorizontalLayout hori2 = new HorizontalLayout();
		HorizontalLayout hori3 = new HorizontalLayout();
		HorizontalLayout hori4 = new HorizontalLayout();
		HorizontalLayout hori5 = new HorizontalLayout();
		HorizontalLayout hori6 = new HorizontalLayout();

		
		ComboBox<Integer> hotelIDBox = new ComboBox<>();
		hotelIDBox.setLabel("Hotel ID");
		ArrayList<Integer> ids = HotelService.getHotelID();
		Collections.sort(ids);
		hotelIDBox.setItems(ids);
		
		TextField nameBox = new TextField("Hotel Name");
		
		TextField ownerTextField = new TextField("Hotel Owner");
		
		Select<String> katListBox = new Select<>();
		katListBox.setItems("*", "**", "***", "****", "*****");
		katListBox.setLabel("Kategorie");
		
		TextField kontakTextField = new TextField("Kontakt");

		TextField streetTextField = new TextField("Adresse");
		
		TextField telephoneField = new TextField("Telefonnummer");
				
		TextField ortTextField = new TextField("Ort");
		TextField plzTextField = new TextField("PLZ");

		NumberField anzRTextField = new NumberField("Anzahl Räume");
		
		NumberField anzBetTextField = new NumberField("Anzahl Betten");
		
		Checkbox kinderCheckBox = new Checkbox();
		kinderCheckBox.setLabel("Kinderfreundlich");
		kinderCheckBox.setValue(false);
		
		Checkbox hundeCheckbox = new Checkbox();
		hundeCheckbox.setLabel("Hundefreundlich");
		hundeCheckbox.setValue(false);
		
		Checkbox spaCheckbox = new Checkbox();
		spaCheckbox.setLabel("Spa");
		spaCheckbox.setValue(false);
		
		Checkbox fitnessCheckbox = new Checkbox();
		fitnessCheckbox.setLabel("Fitness");
		fitnessCheckbox.setValue(false);
		
		hori1.add(hotelIDBox, nameBox, ownerTextField);
		hori2.add(katListBox, kontakTextField);
		hori3.add(streetTextField, telephoneField);
		hori4.add(ortTextField, plzTextField);
		hori5.add(anzBetTextField, anzRTextField);
		hori6.add(kinderCheckBox, hundeCheckbox, spaCheckbox, fitnessCheckbox);
		
		verticalLayout.add(hori1, hori2, hori3, hori4, hori5, hori6);
		
		hotelIDBox.addValueChangeListener( event -> {
			
			if(hotelIDBox.getValue() != null) {
				try {
				int StorageID = hotelIDBox.getValue();
//				hori1.removeAll();
//				hori1.add(hotelIDBox, nameBox, ownerTextField);
				Hotel hotelNEW = HotelService.getHotelById(hotelIDBox.getValue());
				nameBox.setValue( hotelNEW.getHotelName());
				ownerTextField.setValue( hotelNEW.getHotelOwner());
				katListBox.setValue( hotelNEW.getCategory());
				kontakTextField.setValue( hotelNEW.getContact());
				streetTextField.setValue( hotelNEW.getAddress());
				telephoneField.setValue( hotelNEW.getPhone());
				ortTextField.setValue(hotelNEW.getCity());
				plzTextField.setValue(hotelNEW.getCityCode());
				anzBetTextField.setValue((double) hotelNEW.getNoBeds());
				anzRTextField.setValue((double) hotelNEW.getNoRooms());
				
				if(hotelNEW.getKinderfreundlich().equals("1")) {
					kinderCheckBox.setValue(true);
				} else {
					kinderCheckBox.setValue(false);
				}
				if(hotelNEW.getHundefreundlich().equals("1")) {
					hundeCheckbox.setValue(true);
				} else {
					hundeCheckbox.setValue(false);
				}
				if(hotelNEW.getSpa().equals("1")) {
					spaCheckbox.setValue(true);
				} else {
					spaCheckbox.setValue(false);
				}
				if(hotelNEW.getFitness().equals("1")) {
					fitnessCheckbox.setValue(true);
				} else {
					fitnessCheckbox.setValue(false);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			} else {
				Notification.show("Bitte Jahr und Monat ausfüllen");
			}}
		);
		
		HorizontalLayout hori7 = new HorizontalLayout();
		Button addButton = new Button("Änderungen speichern", Event -> {
	
			boolean check = false;
			int counter = 0;
			
			String ausgabe = "";
			
			
			if(!(nameBox.isEmpty())) {
				String name = nameBox.getValue();
				}
				else {
					counter ++;
					ausgabe += "Name ausfüllen! \n";
				}
			if(!(ownerTextField.isEmpty())) {
				String owner = ownerTextField.getValue();
				}
				else {
					counter ++;
					ausgabe += "Besitzer ausfüllen! \n";
				}
				
			if(katListBox.getValue() !=null) {
				String kat = katListBox.getValue();
				}
				else {
					counter++;
					ausgabe += "Kategorie ausfüllen! \n";
				}
			
			if(!(kontakTextField.isEmpty())) {
				String kontakt = kontakTextField.getValue();
				}
				else {
					counter ++;
					ausgabe += "Kontakt ausfüllen! \n";
				}
				
			if(!(streetTextField.isEmpty())) {
				String adresse = streetTextField.getValue();
			 	}	
				else {
					counter++;
					ausgabe += "Adresse ausfüllen! \n";
				}
			
			String telephone = null;
			
			if(telephoneField.getValue() != null) {
				String telephone1 = telephoneField.getValue();
				}
				else {
					counter++;
					ausgabe += "Telefonnummer ausfüllen! \n";
				}
			
			if(!(ortTextField.isEmpty())) {
				String ort = ortTextField.getValue();
				}
				else {
					counter++;
					ausgabe += "Ort ausfüllen! \n";
				}
			
			String plz = null;
			
			if(plzTextField.getValue() != null) {
				plz = plzTextField.getValue();
				}
				else {
					counter++;
					ausgabe += "Postleitzahl ausfüllen! \n";
				}
			
			int anzR = 0;
			int anzB = 0;
			
			if(anzRTextField.getValue() != null) {
				double anzRDouble = anzRTextField.getValue();
				anzR = (int) anzRDouble;
				}
				else {
					counter++;
					ausgabe += "Räume ausfüllen! \n";
				}
			if(anzBetTextField.getValue() != null) {
				double anzBDouble = anzBetTextField.getValue();
				anzB = (int) anzBDouble;
				
				}
				else {
					counter++;
					ausgabe += "Betten ausfüllen! \n";
				}
			
			String kinderfreundlich;
			String hundefreundlich;
			String spa;
			String fitness;
			
			if (kinderCheckBox.getValue() == null) {
				 
				 kinderfreundlich = "0";
				
			    } else {
			    	
			     kinderfreundlich = "1";
			       
			    }
			 
			if (hundeCheckbox.getValue() == null) {
			      
				 hundefreundlich = "0";
				 
			    } else {
			    	
			     hundefreundlich = "1";
			       
			    }
			 
			if (spaCheckbox.getValue() == null) {
			      
				 spa = "0";
				 
			    } else {
			    	
			     spa = "1";
			       
			    }
			 
			if (fitnessCheckbox.getValue() == null) {
			      
				 fitness = "0";
				 
			    } else {
			    	
			     fitness = "1";
			       
			    }
												
			if (counter ==0)	{
				check = true;
				ausgabe = "";
			}
			

			if(check) {
			
				Hotel hotel = new Hotel(katListBox.getValue(), nameBox.getValue(), ownerTextField.getValue(), kontakTextField.getValue(),
						streetTextField.getValue(), ortTextField.getValue(), plz,telephoneField.getValue(), anzR, anzB,kinderfreundlich,
					hundefreundlich, spa, fitness);

				HotelService.updateHotelByID(hotelIDBox.getValue(), hotel);
			
			Notification.show("Datein geändert");
						dialog.close();
						
			} else if(!check) {
				
				Notification.show("Bitte alle Daten ausfüllen");
				Notification.show(ausgabe);
				ausgabe = "";
			}
		});
		addButton.setClassName("hilfeButtons");
		Button closeButton = new Button("Abbrechen", Event -> {

				Notification.show("Abgebrochen");
				dialog.close();
		});
		closeButton.setClassName("hilfeButtons");

		hori7.add(addButton, closeButton);
		verticalLayout.add(hori7);
		
		dialog.add(verticalLayout);
		return dialog;
	}
}

	



