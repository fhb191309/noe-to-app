package com.noe.to.web.frontend.view.HotelOwnerViews;

import com.noe.to.web.frontend.view.logins.LoginPageSeniorUser;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class FrontendUtilsOwner {

	/**
	 * erstellt die buttons Home, Stammdaten, Transaktionsdaten, Histogram, hilfe und Logout und erstellt cklicklistener auf diese Buttons
	 * @return
	 */
	public static Component setNavButtons() {
		Button home = new Button("Home");
		home.setClassName("hilfeButtons");
		Button stamm = new Button("Stammdaten");
		stamm.setClassName("hilfeButtons");
		Button trans = new Button("Transaktionsdaten");
		trans.setClassName("hilfeButtons");
		
		Button logout = new Button("Abmelden");
		logout.setClassName("hilfeButtons");
		
      
		//help.getElement().getStyle().set("margin-left", "auto");
		HorizontalLayout hori2 = new HorizontalLayout(logout);
		hori2.getElement().getStyle().set("margin-left", "auto");

		
        HorizontalLayout hori = new HorizontalLayout(home, stamm, trans, hori2);
        hori.addClassName("toolbar");
        hori.setWidthFull();
       
        
        home.addClickListener(event -> {
        	
        	UI.getCurrent().navigate(HomePageOwner.class);
        });
        
        stamm.addClickListener(event -> {
        	UI.getCurrent().navigate(MasterDataOwner.class);
        });
        
        trans.addClickListener(event -> {
        	UI.getCurrent().navigate(TransactionalDataOwner.class);
        });
        
        
         logout.addClickListener(event -> {
        	UI.getCurrent().navigate(LoginPageSeniorUser.class);
        });
        return hori;
		
	}
}
