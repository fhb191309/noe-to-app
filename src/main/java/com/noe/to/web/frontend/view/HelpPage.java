package com.noe.to.web.frontend.view;

import javax.swing.text.html.parser.ContentModel;

import com.noe.to.web.frontend.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("helpPage")
@Route(value = "HelpPage", layout = MainLayout.class)

public class HelpPage extends VerticalLayout {  
	
	public HelpPage() {
		add(getHelpTitel());
		HorizontalLayout helpHori = new HorizontalLayout (getHelpImage(), getLinks());
		add(helpHori);

	}
	// Seitentitel festlegen
	private Component getHelpTitel() {
		H1 helpUeberschrift = new H1("Bedienungsanleitung");
		return helpUeberschrift;
	}
	//Fügt die Ablaufgraphik hinzu
	private Component getHelpImage() {
		Image helpImage = new Image("images/Senior_Help.png", "Foto SeniorAnleitung");
		helpImage.setHeight("1000px");
		helpImage.setWidth("1000px");
		
		return helpImage;
	}
	
	//Hilfebuttons auf der Hilfeseite werden hier erzeugt
	private Component getLinks() {
		Button homeLink = new Button ("Homepage", Event -> {
			homeExplain();});
		homeLink.setClassName("hilfeButtons");
		
		Button transLink = new Button ("Transaktionsdaten", Event -> {
				transExplain();});
		transLink.setClassName("hilfeButtons");
		
		Button stammLink = new Button ("Stammdaten", Event -> {
				 stammExplain();} );
		stammLink.setClassName("hilfeButtons");
		
		Button histoLink = new Button ("Histogramm", Event -> {
				histoExplain();});
		histoLink.setClassName("hilfeButtons");
		
		Button seniorLink = new Button ("Senior User hinzufügen", Event -> {
				seniorExplain();});
		seniorLink.setClassName("hilfeButtons");
		
		Button hilfeLink = new Button ("Hilfe", Event -> {
			 	hilfeExplain();});
		hilfeLink.setClassName("hilfeButtons");
		
		VerticalLayout links = new VerticalLayout(homeLink,transLink,stammLink,histoLink,seniorLink,hilfeLink);
		return links;
				
	}
//Homepage Hilfebutton
	private Component homeExplain () {
		
		Dialog homeExplain = new Dialog();
		homeExplain.open();

		Label home = new Label();

		home.getElement().setProperty("innerHTML", "Die Homepage bietet Ihnen mehrere Möglichkeiten: "+ 
										"<ul> " +
										"<li> Navigieren zu den Stammdatentabellen </li> "+
										"<li> Navigieren zu den Transaktionstabellen </li>"+
										"<li> Navigieren zu dem Histogramm </li>"+
										"<li> Navigieren zu der Hilfepage </li>"+
										"<li> Abmelden </li>"+
										"<li> Kombinierte Ansicht der Transaktionsdaten und Stammdaten in einer Tabelle </li>"+
										" </ul>"+
										"<p> <b> Navigation: </b> </p>"+
										"<p>Wenn Sie von der Homepage zu den oben angeführten Seiten gebracht werden wollen,"+
										" können Sie einfach die Buttons mit der richtigen Beschriftung anklicken und "+
										"werden automatisch zu der neuen Seite geführt"+
										"Diese Buttons befinden sich entweder unter der großen Überschrift, die sich "+
										"ganz oben auf der Website befindet oder unter dem Einleitungstext der Homepage. </p>"+

										"<p> <b> Abmelden: </b> <p/>"+
										"Der Abmeldebutton befindet sich immer in der rechten oberen Ecke der Webseite."+
										" Zum Abmelden einfach auf diesen Button klicken."+
										" "+
										"<p> <b> Kombinierte Ansicht:  </b> <p/>"+
										" <p>Die Tabelle, welche Sie auf der Homepage vorfinden, ist eine kombinierte Ansicht "+
										"der Tabellen von Transaktions-, und Stammdaten. Wenn Sie also Ihre Dateneingaben"+
										"überprüfen wollen, können Sie dies hier mit einer einfachen Filterfunktion tun."+
										" Zum Filtern, wählen Sie die gewünschten Daten aus und drücken anschließend auf "+
										"<b>'Suchen'</b>. Wenn Sie das Filtern rückgängig machen wollen, klicken Sie auf <b>'Zurücksetzen'.</b>"+
										" Beachten Sie, dass die Eingabe eines Monats nur unter Eingabe eines Jahres möglich ist. <p/>");
		
		
		Button closeButton = new Button("Schließen", Event -> {
			homeExplain.close();
	});
		closeButton.setClassName("hilfeButtons");
		Button homeButton = new Button("Zur Homepage", Event -> {
			homeExplain.close();
			UI.getCurrent().navigate(HomePage.class);
	});
		homeButton.setClassName("hilfeButtons");
		homeExplain.add(home);
		homeExplain.add(closeButton, homeButton);
		
		return homeExplain;
	}


//Stammdaten Hilfebutton
private Component stammExplain () {
		
		Dialog stammExplain = new Dialog();
		stammExplain.open();

		Label stamm = new Label();

		stamm.getElement().setProperty("innerHTML", "Die Stammdaten bieten Ihnen mehrere Möglichkeiten: "+ 
										"<ul> " +
										"<li> Allgemeine Übersicht ansehen </li> "+
										"<li> Hotelübersicht ansehen </li>"+
										"<li> Hotels hinzufügen </li>"+
										"<li> Hotels bearbeiten </li>"+
										"<li> Hotels löschen </li>"+
										"<li> Stammdaten senden </li>"+
										" </ul>"+
										"<p> <b> Übersicht ansehen: </b> </p>"+
										"<p> Die Übersichten bieten Ihnen die Möglichkeit bei der Betätigung der kleinen Pfeile neben dem Spaltennamen die jeweilige Spalte ab-, oder aufsteigend zu sortieren. </p>"+

										"<p> <b> Hotel hinzufügen: </b> <p/>"+
										"<p> Um ein Hotel hinzuzufügen, klicken Sie am Ende der Seite auf den Button „Hotel hinzufügen“. Ein Dialogfenster öffnet sich und Sie können in einem Formular Ihr neues Hotel eintragen. Bitte beachten Sie, dass alle Felder ausgefüllt sein müssen. Um das Hotel hinzuzufügen, klicken Sie auf den Button „Hotel hinzufügen“. Falls Sie ein Feld vergessen haben, wird Ihnen dies in der linken unteren Ecke des Bildschirms aufgezeigt.  </p>" +
										"<p> <b> Hotel bearbeiten:  </b> <p/>"+
										"<p> Um ein Hotel zu bearbeiten haben Sie zwei Möglichkeiten: \r\n </p>"
										+ "<p>1. Sie finden am Ende der Seite den Button „Hotel bearbeiten“ . Die Durchführung der Hotelveränderung funktioniert wie im Punkt „Hotel hinzufügen beschrieben“ durch öffnung eines Dialogfensters. \r\n </p>"
										+ "<p>2.Sie bewegen den Cursor in der Tabelle „Hotelübersicht“ ganz nach rechts, bis Sie am Ende der Tabelle angelangt sind. Dort finden Sie einen Button „Edit“. Wenn Sie diesen klicken, öffnen sich alle Werte in der Zeile und sind frei bearbeitbar. Nachdem Sie alle Werte nach Belieben geändert haben, müssen Sie noch ganz rechts in der Tabelle den „Save“ Button klicken.</p>"
										+"<p> <b> Hotel löschen: </b> <p/>"+
										"<p>Um ein Hotel zu löschen, müssen sie am Ende der Seite auf den Button „Hotel löschen“ klicken. Es öffnet sich dadurch ein Dialogfenster, in welchem Sie die gewünschte Hotel ID auswählen müssen. Der Hotelname wird Ihnen somit automatisch ausgegeben. Wenn Sie nun auf „bestätigen“ klicken, wird das Hotel samt der Transaktionsdaten aus dem Datenbestand gelöscht. <p/>"
										+"<p> <b> Stammdaten senden: </b> <p/>"+
										"<p>Wenn Sie auf den Button „Stammdaten senden“ klicken, werden Ihnen die Stammdaten automatisch auf Ihre bei der Anmeldung hinterlegte E-Mail-Adresse gesendet. Falls Sie Ihre Mail-Adresse ändern möchten, wenden Sie sich bitte an den IT-Support. </p>"
										);
		
		
		Button closeButton = new Button("Schließen", Event -> {
			stammExplain.close();
	});
		closeButton.setClassName("hilfeButtons");
		Button stammButton = new Button("Zu den Stammdaten", Event -> {
			stammExplain.close();
			UI.getCurrent().navigate(MasterData.class);
	});
		stammButton.setClassName("hilfeButtons");
		stammExplain.add(stamm);
		stammExplain.add(closeButton, stammButton);
		
		return stammExplain;
		
	}
//Transdaten Hilfebutton
private Component transExplain () {
	
	Dialog transExplain = new Dialog();
	transExplain.open();

	Label trans = new Label();

	trans.getElement().setProperty("innerHTML", "Die Transaktionsdaten bieten Ihnen mehrere Möglichkeiten: "+ 
									"<ul> " +
									"<li> Allgemeine Übersicht ansehen </li> "+
									"<li> Allgemeine Übersicht sortieren </li>"+
									"<li> Hotelübersicht ansehen </li>"+
									"<li> Hotelübersicht sortieren</li>"+
									"<li> Transaktionsdaten hinzufügen </li>"+
									"<li> Transaktionsdaten bearbeiten </li>"+
									" </ul>"+
									"<p> <b> Sortieren: </b> </p>"+
									"<p> Zum Sortieren (egal ob Sie die Allgemeine Übersicht oder die Hotelübersicht sortieren möchten) wählen Sie die gewünschten  Werte aus den Dropdown Feldern aus und klicken danach auf „Suchen“. Nun werden nur mehr ihre gewünschten Datensätze ausgegeben. Um wieder auf den ursprünglichen Stand zurück zu kommen, klicken Sie auf „Zurücksetzen“. </p>"+
									
									"<p> <b> Transaktionsdaten hinzufügen: </b> </p>"+
									"<p> Am Ende der Seite finden Sie einen Button „Daten hinzufügen“, wenn Sie diesen betätigen müssen Sie Monat, Jahr und die gewünschte Hotel ID auswählen. Wenn Sie die Hotel ID auswählen, wird Ihnen automatisch der Hotelname daneben angezeigt. Alle Werte, die Sie haben, können Sie eintragen und dann auf „Daten hinzufügen“ klicken. Beachten Sie, dass die Anz.Genesene + Anz.Getestet+ Anz.Geimpft gesamt die Zahl der belegten Betten sein sollte, ansonsten wurden Daten nicht richtig erhoben. </p>"+
									
									"<p> <b> Transaktionsdaten bearbeiten </b> </p>"+
									"<p> Um schon bestehende Transaktionsdaten zu bearbeiten gibt es zwei Möglichkeiten: </p>"
									+ "<p>1. Sie finden am Ende der Seite den Button „Daten bearbeiten“ . Die Durchführung der Datenveränderung funktioniert ähnlich wie im Punkt „Transaktionsdaten hinzufügen beschrieben“</p>"
									+ "<p>2.Sie bewegen den Cursor in der Tabelle „Hotelübersicht“ ganz nach rechts, bis Sie am Ende der Tabelle angelangt sind. Dort finden Sie einen Button „Edit“. Wenn Sie diesen klicken, öffnen sich alle Werte in der Zeile und sind frei bearbeitbar. Nachdem Sie alle Werte nach belieben geändert haben, müssen Sie noch ganz rechts in der Tabelle den „Save“ Button klicken.</p>");

	
	
	Button closeButton = new Button("Schließen", Event -> {
		transExplain.close();
});
	closeButton.setClassName("hilfeButtons");
	Button transButton = new Button("Zu den Transaktionsdaten", Event -> {
		transExplain.close();
		UI.getCurrent().navigate(TransactionalData.class);
});
	transButton.setClassName("hilfeButtons");
	transExplain.add(trans);
	transExplain.add(closeButton, transButton);
	
	return transExplain;
}


//Histo Hilfebutton
	private Component histoExplain() {
		
		Dialog histoExplain = new Dialog();
		histoExplain.open();

		Label histo = new Label();

		histo.getElement().setProperty("innerHTML", "<p> <b> Histogramm: </b> </p>"+ 

										"<p> Auf der Histogrammseite müssen Sie ein Monat und ein Jahr auswählen, damit Ihnen die Betten-, und Zimmerauslastung per Monat angezeigt werden. Wenn Sie das Monat oder das Jahr ändern wollen, wählen Sie einfach einen neuen Wert in dem Dropdown aus.</p>"
		);
		
		Button closeButton = new Button("Schließen", Event -> {
			histoExplain.close();
	});
		closeButton.setClassName("hilfeButtons");
		Button histoButton = new Button("Zum Histogramm", Event -> {
			histoExplain.close();
			UI.getCurrent().navigate(ChartPage.class);
	});
		histoButton.setClassName("hilfeButtons");
		histoExplain.add(histo);
		histoExplain.add(closeButton, histoButton);
		
		return histoExplain;
	}

//Senior Hilfebutton
		private Component seniorExplain () {
			
			Dialog seniorExplain = new Dialog();
			seniorExplain.open();

			Label senior = new Label();

			
			senior.getElement().setProperty("innerHTML", "<p> <b> Senior User hinzufügen: </b> </p>"+ 

										"<p>Um einen neuen Senior User hinzuzufügen, melden Sie sich bitte beim IT-Support. </p>"
		);
			
			Button closeButton = new Button("Schließen", Event -> {
				seniorExplain.close();
		});
			closeButton.setClassName("hilfeButtons");
			seniorExplain.add(senior);
			seniorExplain.add(closeButton);
			
			return seniorExplain;
		}
		
//Hilfe Hilfebutton
		private Component hilfeExplain () {
			
			Dialog hilfeExplain = new Dialog();
			hilfeExplain.open();

			Label hilfe = new Label();

			hilfe.getElement().setProperty("innerHTML", "<p> <b> Hilfe: </b> </p>"+ 

										"<p>Auf dieser Seite befinden Sie sich momentan. Wenn Sie in einem der Bereiche, welche als Buttons links neben der Graphik angezeigt werden, Hilfe benötigen, klicken Sie ganz einfach auf den Button und in dem sich öffnenden Dialogfenster wird alles ins Detail erklärt. Um aus dem Dialogfenster zu kommen, drücken Sie „abbrechen“ oder außerhalb des Fensters. </p>"
		);
			
			Button closeButton = new Button("Schließen", Event -> {
				hilfeExplain.close();
		});
			closeButton.setClassName("hilfeButtons");
			Button hilfeButton = new Button("Zur Hilfepage", Event -> {
				hilfeExplain.close();
				UI.getCurrent().navigate(HelpPage.class);
		});
			hilfeButton.setClassName("hilfeButtons");
			hilfeExplain.add(hilfe);
			hilfeExplain.add(closeButton, hilfeButton);
			
			return hilfeExplain;
		}
}
