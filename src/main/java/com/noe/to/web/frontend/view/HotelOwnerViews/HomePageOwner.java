package com.noe.to.web.frontend.view.HotelOwnerViews;

import java.sql.SQLException;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;


@PageTitle("HomePageOwner")
@Route(value = "HomePageOwner", layout = MainLayoutOwner.class)

public class HomePageOwner extends VerticalLayout{ 
	/**
	 * Konstuktor der Class HomePage
	 * fügt alle Komponenten der Seite hinzu
	 * @throws SQLException 
	 */
	public HomePageOwner() throws SQLException{
		
		addClassName("HomePage");
		
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);

		add(getImage());
		
		add(getnews());
	
		Paragraph willkommenField = new Paragraph("Willkommen auf der Webseite der Niederösterreichischen "
				+ " Touristeninformation! Dem Land Niederösterreich ist es sehr wichtig, den Tourismus am Laufen"
				+ " zu halten. Hierfür nimmt es sich statistische Daten zur Hilfe, um mögliche Veränderungen "
				+ " vorauszusagen und aus der Vergangenheit Informationen zu sammeln, um die Zukunft zu verbessern."
				+ " Im Namen des Landes wird allen NutzerInnen gedankt, dass Sie dabei helfen Daten zu sammeln.");
		add(willkommenField);
		
		add(addButtons());
		
	}	
	
	/**
	 * erstellt das Bild für die HomePage
	 * @return
	 */
	private Component getImage() {
		Image image1 = new Image("images/Weingarten_1.png", "Foto Weingarten");
		image1.setHeight("200px");
		
		return image1;
	}
	/**
	 * erstellt eine zweite Überschrift
	 * @return
	 */
	private VerticalLayout getnews() {
		H2 news = new H2("Neuigkeiten");
		
		 VerticalLayout vert = new VerticalLayout(news);
	        vert.addClassName("news");
	        return vert;
	}
	
	//Adds Buttons
	/**
	 * erstellt die buttons Stammdaten, Transaktionsdaen, Histogram und Recht vergeben
	 * fügt jeden dieser Buttons einen ClickListener hinzu, die auf die jeweilige Seite führen
	 * @return
	 */
	private HorizontalLayout addButtons() {

		Button BStamm = new Button("Stammdaten");
		BStamm.setClassName("hilfeButtons");
		Button BTrans = new Button("Transaktionsdaten");
		BTrans.setClassName("hilfeButtons");
		
		HorizontalLayout hori = new HorizontalLayout(BStamm, BTrans);
		
		hori.addClassName("toolbar");
		
	     BStamm.addClickListener(event -> {
	        	UI.getCurrent().navigate(MasterDataOwner.class);
	        });
	     
	     BTrans.addClickListener(event -> {
	        	UI.getCurrent().navigate(TransactionalDataOwner.class);
	        });
	     
		return hori;
    }
}
