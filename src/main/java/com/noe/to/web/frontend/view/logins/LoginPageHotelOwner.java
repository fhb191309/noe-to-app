package com.noe.to.web.frontend.view.logins;

import java.sql.SQLException;

import org.springframework.core.annotation.Order;
import org.springframework.security.access.annotation.Secured;

import com.noe.to.web.backend.BackendUtils;
import com.noe.to.web.backend.service.AppUserService;
import com.noe.to.web.frontend.FrontendUtils;
import com.noe.to.web.frontend.view.HotelOwnerViews.HomePageOwner;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route("login-hotelOwner")
@PageTitle("Login | NÖ-TO | Hotel Owner")
//@Secured("ADMIN")
//@Order(1)

public class LoginPageHotelOwner extends VerticalLayout{


	public  LoginPageHotelOwner() {
		H1 NoTo = new H1("Niederösterreichische Touristeninformation");
		NoTo.addClassName("ueberschrift");
		
		Image image = new Image("images/2021-Logo.png", "Logo");
		image.setHeight("55px");
		Image image2 = new Image("images/2021-Logo.png", "Logo");
		image2.setHeight("55px");
		
				
		 HorizontalLayout header = new HorizontalLayout(image, NoTo, image2);
	        header.addClassName("ueberschrift1");
	        header.setWidth("100%");
	        header.expand(NoTo);
	        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
	        
	    add(header);
		
		setJustifyContentMode(JustifyContentMode.CENTER);
        setAlignItems(Alignment.CENTER);
		

		add(getH2());
				
		add(loginform());
	
	}
	

	
	public Component getH2() {
		
		H2 h2 = new H2("Hotelbesitzer - Log in");
		h2.setClassName("loginSchriften");
		return h2;
	}
	
	public Component loginform() {
		VerticalLayout verticalLayout = new VerticalLayout();
		
//		String usernameString = null;
//		String pwString = null;
		
		TextField usernameField = new TextField("User Name");
		usernameField.setPlaceholder("User Name");
		
		PasswordField pwField = new PasswordField("Passwort");
		pwField.setPlaceholder("Passwort");
		
		Button loginButton = new Button("Log in");
		loginButton.setWidth("193px");
		loginButton.setClassName("hilfeButtons");
		
		Button seniorUserButton = new Button("Senior User Login");
		seniorUserButton.setClassName("hilfeButtons");
		seniorUserButton.setWidth("193px");

		seniorUserButton.addClickListener(Event -> {
			UI.getCurrent().navigate(LoginPageSeniorUser.class);

		});

		
		
		loginButton.addClickListener(Event -> {
			
			//daten übergeben mit:
			
			String usernameString = usernameField.getValue();
			String pwString = pwField.getValue();
			
			//Vielleicht gibt dann die angesprochene Methode true oder false zum anmelden zurück
			//und dann kann man sich anmelden ca. so:
			

			boolean b;
			int i = -1;
			try {
				b = AppUserService.checkLoginInfoHotelOwner(usernameString, pwString);
				i = AppUserService.getAppUser(usernameString, pwString);
				FrontendUtils.setSessionID(i);
				if(b == true) {
					
					UI.getCurrent().navigate(HomePageOwner.class);
				} else {
					Notification.show("Username oder Passwort falsch");
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			
		});
		
		
		verticalLayout.add(usernameField, pwField, loginButton,seniorUserButton);
		verticalLayout.setHorizontalComponentAlignment(Alignment.CENTER, usernameField);
		verticalLayout.setHorizontalComponentAlignment(Alignment.CENTER, pwField);
		verticalLayout.setHorizontalComponentAlignment(Alignment.CENTER, loginButton);
		verticalLayout.setHorizontalComponentAlignment(Alignment.CENTER, seniorUserButton);
	return verticalLayout;
	
	}
}
