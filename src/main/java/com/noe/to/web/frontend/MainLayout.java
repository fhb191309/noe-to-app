package com.noe.to.web.frontend;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;



@CssImport("./styles/shared-styles.css")

public class MainLayout extends AppLayout{
	
	/**
	 * Erstellt die NavBar, damit diese am Anang jeder Seite angezeigt werden kann
	 * in der NavBar enthalten sind die Überschrift, das Bild und die Buttons von FrontendUtils
	 */
	public MainLayout() {
		

		H1 NoTo = new H1("Niederösterreichische Touristeninformation");
		NoTo.addClassName("ueberschrift");
		
		Image image = new Image("images/2021-Logo.png", "Logo");
		image.setHeight("55px");
		
				
		 HorizontalLayout header = new HorizontalLayout(image, NoTo);
	        header.addClassName("ueberschrift");
	        header.setWidth("100%");
	        header.expand(NoTo);
	        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);

		
		VerticalLayout verticalLayout = new VerticalLayout(header, FrontendUtils.setNavButtons());
		addToNavbar(verticalLayout);
	
	}
}

	

