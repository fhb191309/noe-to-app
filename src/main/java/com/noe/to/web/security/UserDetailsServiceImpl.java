package com.noe.to.web.security;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.noe.to.web.backend.entity.AppUser;
import com.noe.to.web.backend.service.AppUserService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	AppUserService appUserService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUser appUser = null;
		
		try {
			appUser = AppUserService.getAppUserLoginInfo(username);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(appUser == null) {
			
			throw new UsernameNotFoundException(username);
		}
		return appUser;
	}

}
