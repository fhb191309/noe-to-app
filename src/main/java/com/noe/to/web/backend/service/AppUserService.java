package com.noe.to.web.backend.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.springframework.stereotype.Service;

import com.helger.css.decl.shorthand.CSSShortHandDescriptorWithAlignment;
import com.noe.to.web.backend.database.DatabaseConnect;
import com.noe.to.web.backend.entity.AppUser;
import com.noe.to.web.backend.repository.AppUserRepository;
import com.noe.to.web.backend.repository.HotelRepository;
import com.vaadin.flow.component.notification.Notification;



@Service
public class AppUserService {


	/**
	 * CRUDs for the AppUser Class
	 * C..Create
	 * R..Read
	 * U..Update
	 * D..Delete
	 */
	
	/**
	 * Hier wird der User mit seiner ID gesucht und gelöscht werden
	 * Vom Frontend MUSS ein Integer übergeben werden
	 * @param id
	 */
	public static void deleteAppUserByID(int id) {

		String DELETE_STATEMENT = "delete from AppUser where id = ?";

		try (Connection connection = DatabaseConnect.getConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_STATEMENT)) {

		
			statement.setInt(1, id);

			int rows = statement.executeUpdate();
			System.out.println(rows);

			if (rows == 0) {
				
				Notification.show("No AppUser was deleted");
			}

			System.out.println();

			System.out.println("done");

		} catch (SQLException e) {
			System.out.println(e.getErrorCode());

		}

	}
	

	/**
	 * Hier wird ein neuer User zur Datenbank hinzugefügt
	 * Vom Frontend MUSS ein Objekt der Klasse AppUser übergeben werden
	 * @param appUser
	 * @throws SQLException
	 */
	public static void insertNewAppUser(AppUser appUser) throws SQLException {

		Connection con = null;
		PreparedStatement queryStatement = null;

		String statement = "INSERT INTO AppUser values (?,?,?,?,?)";

		System.out.println("Executing query: " + statement);

		con = DatabaseConnect.getConnection();
		queryStatement = con.prepareStatement(statement);

		queryStatement.setInt(1, appUser.getHotelID());
		queryStatement.setString(2, appUser.getEigentuemer());
		queryStatement.setString(3, appUser.getUsername());
		queryStatement.setString(4, appUser.getEmail());
		queryStatement.setString(5, appUser.getPwd());
		queryStatement.setString(6, appUser.getIsHotelbesitzer());
		

		int countOfRows = queryStatement.executeUpdate();

		ResultSet rs = queryStatement.getGeneratedKeys();

		System.out.println("AppUser saved " + countOfRows);

	}
	

	/**
	 * Hier werden alle User aus der Datenbank ausgegeben
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<AppUser> getAllAppUsers() throws SQLException {
		ArrayList<AppUser> allAppUsers = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select * from AppUser";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {

			int id = result.getInt("id");
			String eigentuemer = result.getString("eigentuemer");
			String username = result.getString("username");
			String email = result.getString("email");
			String pwd = result.getString("pwd");
			String isHotelbesitzer = result.getString("isHotelbesitzer");
			

			allAppUsers.add(new AppUser(id, eigentuemer, username, email, pwd, isHotelbesitzer));

		}

		return allAppUsers;
	}
	
	
	/**
	 * Hier wird der User mit seiner ID gesucht
	 * Vom Frontend MUSS ein Integer übergeben werden
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static AppUser getAppUserById(int id) throws SQLException {

		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select * from AppUser where id = " + id;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		AppUser m = null;
		while (result.next()) {
			
			int id2 = result.getInt("id");
			String eigentuemer = result.getString("eigentuemer");
			String username = result.getString("username");
			String email = result.getString("email");
			String pwd = result.getString("pwd");
			String isHotelbesitzer = result.getString("isHotelbesitzer");
			
			m = new AppUser(id, eigentuemer, username, email, pwd, isHotelbesitzer);

		}

		return m;
	}
	
	
	public static AppUser getAppUserLoginInfo(String username) throws SQLException {

		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select * from AppUser "
						+ "where username like '" + username + "'";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		AppUser m = null;
		while (result.next()) {
			
			int id = result.getInt("id");
			String eigentuemer = result.getString("eigentuemer");
			String _username = result.getString("username");
			String email = result.getString("email");
			String _pwd = result.getString("pwd");
			String isHotelbesitzer = result.getString("isHotelbesitzer");
			
			m = new AppUser(id, eigentuemer, _username, email, _pwd, isHotelbesitzer);

		}

		return m;
	}
	
	
	public static Boolean checkLoginInfoSeniorUser(String username, String password) throws SQLException {

		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select username, pwd "
							+ "from AppUser "
							+ "where username like '" + username + "' and pwd like '" + password + "'";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		Boolean checkAppuser = null;
		
		while (result.next()) {
			
			String _username = result.getString("username");
			String _pwd = result.getString("pwd");
			
			if (_username.equals("dbo") && _pwd.equals("password")) {
				
				checkAppuser = true;
				System.out.println("Benutzer ist vorhanden");
				
			}else {
				
				checkAppuser = false;
				System.out.println("Benutzer ist nicht vorhanden. Bitte versuchen Sie es erneut");
				
			}

		}

		return checkAppuser;
	}
	
	
	public static Boolean checkLoginInfoHotelOwner(String username, String password) throws SQLException {

		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select username, pwd "
							+ "from AppUser "
							+ "where username like '" + username + "' and pwd like '" + password + "'";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		Boolean checkAppuser = null;
		
		while (result.next()) {
			
			String _username = result.getString("username");
			String _pwd = result.getString("pwd");
			
			if (_username.equals("dbo") && _pwd.equals("password")) {
				
				checkAppuser = true;
				System.out.println("Ungültiger Benutzer");
				
			}else if(_username.equals(username) && _pwd.equals(password)) {
				
				checkAppuser = true;
				System.out.println("Benutzer ist vorhanden");
				
			}else {
				
				checkAppuser = false;
				System.out.println("Benutzer ist nicht vorhanden. Bitte versuchen Sie es erneut");
				
			}

		}

		return checkAppuser;
	}
	
	
	
	public static Integer getAppUser(String username, String password) throws SQLException {

		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select hotelID "
							+ "from AppUser "
							+ "where username like '" + username + "' and pwd like '" + password + "'";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		int appUserID = 0; 
		
		while (result.next()) {
			
			int id = result.getInt("hotelID");
			
			appUserID = id;

		}

		return appUserID;
	}
	
	
	
}
