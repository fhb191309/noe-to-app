package com.noe.to.web.backend.entity;

public class Email {

	/**
	 * Eigenschaften für die Klasse
	 */
	private int nr;
	private String betreff;
	private String absender;
	private String inhalt;
	
	/**
	 * Konstruktor für die Klasse Email
	 * @param nr
	 * @param betreff
	 * @param absender
	 * @param inhalt
	 */
	public Email(int nr, String betreff, String absender, String inhalt) {
	
		this.nr = nr;
		this.betreff = betreff;
		this.absender = absender;
		this.inhalt = inhalt;
	}


	public int getNr() {
		return nr;
	}


	public String getBetreff() {
		return betreff;
	}


	public String getAbsender() {
		return absender;
	}


	public String getInhalt() {
		return inhalt;
	}


	@Override
	public String toString() {
		return "Email [nr=" + nr + ", betreff=" + betreff + ", absender=" + absender + ", inhalt=" + inhalt + "]";
	}
	
}
