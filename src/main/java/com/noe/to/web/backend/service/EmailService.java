package com.noe.to.web.backend.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


import javax.mail.MessagingException;

/**
 * cronmaker for scheduling from URL: http://www.cronmaker.com/;jsessionid=node0cxezzq7e2m0w1euusn5q4enlf658051.node0?0 
 * @author ugouw
 *
 */
//Send Email with a File

@Service
public class EmailService extends Authenticator {
	
	@Autowired
	private static JavaMailSender mailSender;
	
	public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }
	
	/**
	 * In dieser Methode wird eine Email an den Head von Nö-TO gesendet
	 * 
	 * Die möglichkeit Emails automatish zu versenden wird noch eingebaut
	 * @param empfaenger
	 * @throws MessagingException
	 * @throws IOException
	 */
	@Scheduled(cron = "0 0 8 1 1/1 ?") //Email wird am 1. im Monat an den vordefinierten Empfänger gesendet
	public static void emailAnNoeToHead() throws MessagingException, IOException{

		final String username = "emailversenden@gmx.at";
		final String password = "emailversenden";
		String sender = "emailversenden@gmx.at";
		String recipient = "noetoe-head@gmx.at";
		String recipientCC = "emailversenden@gmx.at"; // Kopie an sich selbst zb Archivzwecke
		
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "mail.gmx.net");
		properties.put("mail.smtp.port", "587");
		
		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});


		MimeMessage msg = new MimeMessage(session);
		
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		
		try {
			/**
			 * Absender der Email
			 * Empfänger der Email
			 */
			helper.setFrom(new InternetAddress(sender));
			helper.addTo(new InternetAddress(recipient));
			helper.addCc(new InternetAddress(recipientCC));

			/**
			 * Das Datum an dem die Email abgeschickt wurde
			 */
			helper.setSentDate(new Date());

			String name = "";

			try {
				InetAddress host = InetAddress.getLocalHost();
				name = host.getHostName();

			} catch (UnknownHostException e1) {
				name = "Unbekannt";
			}

			/**
			 * Der Betreff der Email
			 */
			helper.setSubject("Sie haben Post von " + name);

			String einleitung = "Sehr geehrte Damen und Herren!\n\n";
			String haupteil = "Anbei die monatliche Statistik.\n\n";
			String schluss = "Mit freunlichen Grüßen\nNÖ-TO";
			helper.setText(einleitung + haupteil + schluss);

			/**
			 *  Anhang
			 */
			/**
			 *  Muss noch angepasst werden, so dass die pdf Datein hier reingespielt werden können
			 */
			File file = null;
			File file2 = null;
			
			try {
				file = PrintService.sendMonthlyStatistics();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			try {
				file2 = PrintService.sendMonthlyStatistics();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			/**
			 *  ausgabe der Datei muss noch angepasst werden
			 */
			helper.addAttachment("", file);
			helper.addAttachment("", file2);
		
			System.out.println("Anhang konnte nicht eingebunden werden, die Gründe sind nicht näher spezifiziert");

			/**
			 * Email wird hier abgeschickt
			 */
			mailSender.send(msg);
			System.out.println("Nachricht wurde erfolgreich verschickt!");
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		System.out.println("Methodenaufruf nun zu Ende");
	}
	
	
	/**
	 * Mit dieser Methode können Mitarbeiter sich selber eine Email schicken
	 * @param empfaenger
	 * @throws MessagingException
	 * @throws IOException
	 */
	@Scheduled(cron = "0 0 8 1 1/1 ?") //Email wird am 1. im Monat an den vordefinierten Empfänger gesendet
	public static void emailAnNoeToSenior() throws MessagingException, IOException{

		final String username = "emailversenden@gmx.at";
		final String password = "emailversenden";
		String sender = "emailversenden@gmx.at";
		String recipient = "predefined@email.com";
		String recipientCC = "emailversenden@gmx.at"; // Kopie an sich selbst zb Archivzwecke
		
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "mail.gmx.net");
		properties.put("mail.smtp.port", "587");


		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});


		MimeMessage msg = new MimeMessage(session);
		
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		
		try {
			/**
			 * Absender der Email
			 * Empfänger der Email
			 */
			helper.setFrom(new InternetAddress(sender));
			helper.addTo(new InternetAddress(recipient));
			helper.addCc(new InternetAddress(recipientCC));
			
			/**
			 * Das Datum an dem die Email abgeschickt wurde
			 */
			helper.setSentDate(new Date());

			String name = "";

			try {
				InetAddress host = InetAddress.getLocalHost();
				name = host.getHostName();

			} catch (UnknownHostException e1) {
				name = "Unbekannt";
			}

			/**
			 * Der Betreff der Email
			 */
			helper.setSubject("Sie haben Post von " + name);

			String einleitung = "Sehr geehrte Damen und Herren!\n\n";
			String haupteil = "Anbei die gewünschte Datei.\n\n";
			String schluss = "Mit freunlichen Grüßen\nNÖ-TO";
			helper.setText(einleitung + haupteil + schluss);

			/**
			 *  Anhang
			 */
			/**
			 *  Muss noch angepasst werden, so dass die pdf Datein hier reingespielt werden können
			 */
			File file = null;
			
			try {
				file = PrintService.sendMonthlyStatistics();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			/**
			 *  ausgabe der Datei muss noch angepasst werden
			 */
			helper.addAttachment("", file);
		
			System.out.println("Anhang konnte nicht eingebunden werden, die Gründe sind nicht näher spezifiziert");

			/**
			 * Email wird hier abgeschickt
			 */
			mailSender.send(msg);
			System.out.println("Nachricht wurde erfolgreich verschickt!");
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		System.out.println("Methodenaufruf nun zu Ende");
		
	}
	
	public static void emailAnTouristOffices(String empfänger, String kategorie, int kinderfreundlich, 
			int hundefreundlich, int spa, int fitness) throws MessagingException, IOException{

		final String username = "emailversenden@gmx.at";
		final String password = "emailversenden";
		String sender = "emailversenden@gmx.at";
		String recipient = empfänger;
		String recipientCC = "emailversenden@gmx.at"; // Kopie an sich selbst zb Archivzwecke
		
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "mail.gmx.net");
		properties.put("mail.smtp.port", "587");


		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});


		MimeMessage msg = new MimeMessage(session);
		
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		
		try {
			/**
			 * Absender der Email
			 * Empfänger der Email
			 */
			helper.setFrom(new InternetAddress(sender));
			helper.addTo(new InternetAddress(recipient));
			helper.addCc(new InternetAddress(recipientCC));
			
			/**
			 * Das Datum an dem die Email abgeschickt wurde
			 */
			helper.setSentDate(new Date());

			String name = "";

			try {
				InetAddress host = InetAddress.getLocalHost();
				name = host.getHostName();

			} catch (UnknownHostException e1) {
				name = "Unbekannt";
			}

			/**
			 * Der Betreff der Email
			 */
			helper.setSubject("Sie haben Post von " + name);

			String einleitung = "Sehr geehrte Damen und Herren!\n\n";
			String haupteil = "Anbei die gewünschte Auswahl an Hotels.\n\n";
			String schluss = "Mit freunlichen Grüßen\nNÖ-TO";
			helper.setText(einleitung + haupteil + schluss);

			/**
			 *  Anhang
			 */
			/**
			 *  Muss noch angepasst werden, so dass die pdf Datein hier reingespielt werden können
			 */
			File file = null;
			
			try {
				file = PrintService.sendOutputList(kategorie, kinderfreundlich, hundefreundlich, spa, fitness);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			/**
			 *  ausgabe der Datei muss noch angepasst werden
			 */
			helper.addAttachment("", file);
		
			System.out.println("Anhang konnte nicht eingebunden werden, die Gründe sind nicht näher spezifiziert");

			/**
			 * Email wird hier abgeschickt
			 */
			mailSender.send(msg);
			System.out.println("Nachricht wurde erfolgreich verschickt!");
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		System.out.println("Methodenaufruf nun zu Ende");
		
	}

}
