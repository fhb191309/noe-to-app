package com.noe.to.web.backend.entity;

public class MasterDataOverview {

	/**
	 * Eigenschaften der Klasse MasterDataOverview
	 */
	String kategorie;
	int azBetriebe;
	int azZimmer;
	int azBetten;
	String kinderfreundlich;
	String hundefreundlich;
	String spa;
	String fitness;
	
	/**
	 * Konstruktor für die Klasse MasterDataOverview
	 * @param kategorie
	 * @param azBetriebe
	 * @param azZimmer
	 * @param azBetten
	 * @param kinderfreundlich
	 * @param hundefreundlich
	 * @param spa
	 * @param fitness
	 */
	public MasterDataOverview(String kategorie, int azBetriebe, int azZimmer, int azBetten, String kinderfreundlich,
			String hundefreundlich, String spa, String fitness) {
		
		this.kategorie = kategorie;
		this.azBetriebe = azBetriebe;
		this.azZimmer = azZimmer;
		this.azBetten = azBetten;
		this.kinderfreundlich = kinderfreundlich;
		this.hundefreundlich = hundefreundlich;
		this.spa = spa;
		this.fitness = fitness;
	}
	
	@Override
	public String toString() {
		return "MasterDataOverview [kategorie=" + kategorie + ", azBetriebe=" + azBetriebe + ", azZimmer=" + azZimmer
				+ ", azBetten=" + azBetten + ", kinderfreundlich=" + kinderfreundlich + ", hundefreundlich="
				+ hundefreundlich + ", spa=" + spa + ", fitness=" + fitness + "]";
	}

	public String getKategorie() {
		return kategorie;
	}


	public void setKategorie(String kategorie) {
		this.kategorie = kategorie;
	}


	public int getazBetriebe() {
		return azBetriebe;
	}


	public void setazBetriebe(int azBetriebe) {
		this.azBetriebe = azBetriebe;
	}


	public int getazZimmer() {
		return azZimmer;
	}


	public void setazZimmer(int azZimmer) {
		this.azZimmer = azZimmer;
	}


	public int getazBetten() {
		return azBetten;
	}


	public void setazBetten(int azBetten) {
		this.azBetten = azBetten;
	}


	public String getKinderfreundlich() {
		return kinderfreundlich;
	}


	public void setKinderfreundlich(String kinderfreundlich) {
		this.kinderfreundlich = kinderfreundlich;
	}


	public String getHundefreundlich() {
		return hundefreundlich;
	}


	public void setHundefreundlich(String hundefreundlich) {
		this.hundefreundlich = hundefreundlich;
	}


	public String getSpa() {
		return spa;
	}


	public void setSpa(String spa) {
		this.spa = spa;
	}


	public String getFitness() {
		return fitness;
	}


	public void setFitness(String fitness) {
		this.fitness = fitness;
	}
	
	
	
	

}
