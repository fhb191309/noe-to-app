package com.noe.to.web.backend;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.JTextField;

import com.noe.to.web.backend.database.DatabaseConnect;

public class BackendUtils {
	
	/**
	 * Hier wird eine Fehlernachricht erstellt
	 * @param msg
	 */
	private static void error(String msg) {
		  throw new IllegalArgumentException("Fehler: "+msg+"!");
	  }
	  
	/**
	 * Hier wird überprüft, ob ein leerer String eingegeben wurde
	 * Wenn nicht wird eine Fehlernachricht ausgegeben
	 * @param s
	 * @param msg
	 */
	  public static void checkStringNotEmpty(String s, String msg) {
		  if (s == null || s.isEmpty())
			  error(msg);
	  }
	  
	  /**
	   * Hier wird überprüft, ob ein Integ positiv ist
	   * Wenn nicht wird eine Fehlernachricht ausgegeben
	   * @param i
	   * @param msg
	   */
	  public static void checkIntPositive(int i, String msg) {
		  if (i<0)
			  error(msg);
	  }
	  
	  /**
	   * Hier wird überprüft ob das eingebene Jahr größer als 2014 ist
	   * Wenn nicht wird eine Fehlernachricht ausgegeben
	   * @param i
	   * @param msg
	   */
	  public static void checkYear(int i, String msg) {
		  if (i<2014)
			  error(msg);
	  }
	  
	  /**
	   * Hier wird überprüft ob ein Name bereits vorhanden ist
	   * Wenn ja, wird eine Fehlernachricht ausgegeben
	   * @param name
	   * @param allNames
	   * @param msg
	   */
	  public static void checkUniqueName(String name, Set<String> allNames, String msg) {
		  if (allNames.contains(name))
			  error(msg);
	  }
	  
	  /**
	   * Hier wird ein Integerwert aus einem Textfeld ausgelesen
	   * Ist es kein Integer, wird eine Fehlernachricht ausgeben
	   * @param txt
	   * @param msg
	   * @return
	   */
	  public static int readInteger(JTextField txt, String msg) {
		  try {
				return Integer.valueOf(txt.getText());
		  }
		  catch (NumberFormatException ex) {
				error(msg);
		  }
		  return 0; //cannot reach this line
	  }
	  
	  /**
	   * Hier wird die höchste/größte ID aus der Datenbank ausgelesen
	   * @return
	   * @throws SQLException
	   */
	  public static Integer getMaxId() throws SQLException {
			
		  int maxId = 0;

			Connection con = null;
			Statement statement;
			String statementSQL;

			ResultSet result;

			con = DatabaseConnect.getConnection();
			statement = con.createStatement();

			statementSQL = "Select MAX(id) from Hotels";

			System.out.println("Executing the following query: " + statementSQL);

			result = statement.executeQuery(statementSQL);

			while (result.next()) {

				int id = result.getInt(1);
				
				maxId = id;

			}

			return maxId;
		}
}
