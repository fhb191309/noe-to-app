package com.noe.to.web.backend.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.noe.to.web.backend.database.DatabaseConnect;
import com.noe.to.web.backend.entity.HotelOutputList;
import com.noe.to.web.backend.entity.MasterDataOverview;

@Service
public class MasterDataService {

	
	/**
	 * Hier wird eine Zusammenfassung der Daten aus der Tabelle 'Hotel' ausgegeben
	 * Die Tabelle 'Hotels' wird nach der Kategorie, Anzahl der Betriebe, Anzahl der Zimmer,
	 * Anzahl der Betten und den verfügbaren Kategorien geordnet
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<MasterDataOverview> getMasterDataOverview() throws SQLException {
		ArrayList<MasterDataOverview> allMasterData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT kategorie, COUNT(id) AS 'Az. Betriebe', SUM(azZimmer) AS 'Az. Zimmer',"
							+ "SUM(azBetten) AS 'Az. Betten', COUNT(NULLIF(kinderfreundlich,0)) AS 'kinderfreundlich',"
							+ "COUNT(NULLIF(hundefreundlich,0)) AS 'hundefreundlich', COUNT(NULLIF(spa,0)) AS 'spa',"
							+ "COUNT(NULLIF(fitness,0)) AS 'fitness'"
						+ "FROM Hotels "
						+ "GROUP BY kategorie"; 
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			String kategorie = result.getString("kategorie");
			int azBetriebe  = result.getInt("Az. Betriebe");
			int azZimmer = result.getInt("Az. Zimmer");
			int azBetten  = result.getInt("Az. Betten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");
			
						
			allMasterData.add(new MasterDataOverview(kategorie, azBetriebe, azZimmer, azBetten, kinderfreundlich, 
					hundefreundlich, spa, fitness));

		}
		
		return allMasterData;
		
	}
	
	public ArrayList<MasterDataOverview> getMasterDataOverviewByID(int id) throws SQLException {
		ArrayList<MasterDataOverview> allMasterData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT kategorie, COUNT(id) AS 'Az. Betriebe', SUM(azZimmer) AS 'Az. Zimmer',"
							+ "SUM(azBetten) AS 'Az. Betten', COUNT(NULLIF(kinderfreundlich,0)) AS 'kinderfreundlich',"
							+ "COUNT(NULLIF(hundefreundlich,0)) AS 'hundefreundlich', COUNT(NULLIF(spa,0)) AS 'spa',"
							+ "COUNT(NULLIF(fitness,0)) AS 'fitness'"
						+ "FROM Hotels "
						+ "Where id = " + id
						+ "GROUP BY kategorie"; 
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			String kategorie = result.getString("kategorie");
			int azBetriebe  = result.getInt("Az. Betriebe");
			int azZimmer = result.getInt("Az. Zimmer");
			int azBetten  = result.getInt("Az. Betten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");
			
						
			allMasterData.add(new MasterDataOverview(kategorie, azBetriebe, azZimmer, azBetten, kinderfreundlich, 
					hundefreundlich, spa, fitness));

		}
		
		return allMasterData;
		
	}
	
	public ArrayList<HotelOutputList> getOutputListsByCategory(String kategorie, int kinderfreundlich, 
			int hundefreundlich, int spa, int fitness) throws SQLException {
		ArrayList<HotelOutputList> allOutputData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection(); 
		statement = con.createStatement();

		statementSQL = "SELECT id, kategorie, name, adresse, stadt, plz, azZimmer, azBetten, telefon "
						+ "FROM Hotels "
						+ "Where kategorie Like '" + kategorie + "' and hundefreundlich = " + kinderfreundlich
						+ " and hundefreundlich =" + hundefreundlich + " and spa = " + spa + " and fitness = " + fitness; 
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			
			int hotelID = result.getInt("id");
			String hotelName = result.getString("name");
			String adresse = result.getString("adresse");
			String stadt = result.getString("stadt");
			String plz = result.getString("plz");
			String telefon = result.getString("telefon");
			int noRooms = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			
						
			allOutputData.add(new HotelOutputList(hotelID, hotelName, adresse, stadt, plz, telefon, noRooms, noBeds));

		}
		
		return allOutputData;
		
	}
	
}
