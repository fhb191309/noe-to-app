package com.noe.to.web.backend.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Service;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.noe.to.web.backend.database.DatabaseConnect;

@Service
public class PrintService {

	
//	public static void main(String[] args)  {
//
//		
//		try {
//			
//			System.out.println("Creating PDF...");
//			
//			try {
//				sendMonthlyStatistics();
//			} catch (FileNotFoundException e) {
//				e.printStackTrace();
//			}
//			
//			System.out.println("Done");
//		} catch (SQLException e) {
//			System.err.println(e.getMessage());
//		}
//
//	}
	/**
	 * Hier werden alle Transaktionsdaten aus der Tabelle 'Belegungen'ausgegeben
	 * und dann als PDF Dokument gedruckt
	 * @return 
	 * @throws SQLException
	 */
	public static File sendMonthlyStatistics() throws SQLException, FileNotFoundException {
		
		File file = new File("monthlyStatistics.pdf");
		
		//Create  Document instance
		PdfDocument pdfDocument = new PdfDocument(new PdfWriter(file));
		Document doc = new Document(pdfDocument);
		
		//Create a Cell Object
		Cell cell;
		
		//Creat a Table Object
		float [] pointColumnWidths = {10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F}; 
		Table table = new Table(pointColumnWidths);

		//Connecting to the Database
		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT h.kategorie, SUM(h.azZimmer) AS 'Az. Zimmer', AVG(b.Zimmerauslastung) AS 'Zimmerauslastung', "
							+ "SUM(h.azBetten) AS 'Az. Betten', AVG(b.Bettenauslastung) AS 'Bettenauslastung', "
							+ "SUM(b.azNationalitäten) AS 'Az. Nationalitäten', "
							+ "SUM(b.azGenesen) AS 'Az. Genesen', SUM(b.azGetestet) AS 'Az. Getestet', SUM(b.azGeimpft) AS 'Az. Geimpft'"
						+ " FROM Belegungen b JOIN Hotels h ON (b.id = h.id) "
						+ " Group By h.kategorie "
						+ " Order By h.kategorie ASC";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);
		
		
		
		String kategorie;
		String azZimmer;
		String zimmerauslastung;
		String azBetten;
		String bettenauslastung;
		String azNationalitäten;
		String azGenesen;
		String azGetestet;
		String azGeimpft;
		
		try {
			pdfDocument = new PdfDocument(new PdfWriter(file));
			
			//add Header
			table.addCell(new Cell().add(new Paragraph("Kategorie")));
			table.addCell(new Cell().add(new Paragraph("Az. Zimmer")));
			table.addCell(new Cell().add(new Paragraph("Zimmerauslastung")));
			table.addCell(new Cell().add(new Paragraph("Az. Betten")));
			table.addCell(new Cell().add(new Paragraph("Bettenauslastung")));
			table.addCell(new Cell().add(new Paragraph("Az. Nationalitäten")));
			table.addCell(new Cell().add(new Paragraph("Az. Genesen")));
			table.addCell(new Cell().add(new Paragraph("Az. Getestet")));
			table.addCell(new Cell().add(new Paragraph("Az. Geimpft")));
			
			//add query results to the table			
			while (result.next()) {


				kategorie = result.getString("kategorie");
				table.addCell(new Cell().add(new Paragraph(kategorie)));
				
				azZimmer = result.getString("Az. Zimmer");
				table.addCell(new Cell().add(new Paragraph(azZimmer)));
				
				zimmerauslastung = result.getString("Zimmerauslastung");
				table.addCell(new Cell().add(new Paragraph(zimmerauslastung)));
				
				azBetten = result.getString("Az. Betten");
				table.addCell(new Cell().add(new Paragraph(azBetten)));
				
				bettenauslastung = result.getString("Bettenauslastung");
				table.addCell(new Cell().add(new Paragraph(bettenauslastung)));
				
				azNationalitäten = result.getString("Az. Nationalitäten");
				table.addCell(new Cell().add(new Paragraph(azNationalitäten)));
			    
			    azGenesen = result.getString("Az. Genesen");
			    table.addCell(new Cell().add(new Paragraph(azGenesen)));
			    
			    azGetestet = result.getString("Az. Getestet");
			    table.addCell(new Cell().add(new Paragraph(azGetestet)));
			    
			    azGeimpft = result.getString("Az. Geimpft");
			    table.addCell(new Cell().add(new Paragraph(azGeimpft)));
			   
			    
			}
			
			//add Table to the document
			doc.add(table);
			
			doc.close();
			
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
			
		}
		
		return file;
	
	}
	
	public static File sendOutputList(String kategorie, int kinderfreundlich, 
			int hundefreundlich, int spa, int fitness) throws SQLException, FileNotFoundException {
		
		File file = new File("OutputList.pdf");
		
		//Create  Document instance
		PdfDocument pdfDocument = new PdfDocument(new PdfWriter(file));
		Document doc = new Document(pdfDocument);
		
		//Create a Cell Object
		Cell cell;
		
		//Creat a Table Object
		float [] pointColumnWidths = {10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F, 10F}; 
		Table table = new Table(pointColumnWidths);

		//Connecting to the Database
		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT id, kategorie, name, adresse, stadt, plz, azZimmer, azBetten, telefon "
							+ "FROM Hotels "
							+ "Where kategorie Like '" + kategorie + "' and hundefreundlich = " + kinderfreundlich
							+ " and hundefreundlich = " + hundefreundlich + " and spa = " + spa + " and fitness = " + fitness; 

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);
		
		
		
		String id;
		String kategorie2;
		String name;
		String adresse;
		String stadt;
		String plz;
		String telefon;
		String azZimmer;
		String azBetten;
		
		try {
			pdfDocument = new PdfDocument(new PdfWriter(file));
			
			//add Header
			table.addCell(new Cell().add(new Paragraph("HotelID")));
			table.addCell(new Cell().add(new Paragraph("Kategorie")));
			table.addCell(new Cell().add(new Paragraph("Hotelname")));
			table.addCell(new Cell().add(new Paragraph("Adresse")));
			table.addCell(new Cell().add(new Paragraph("Stadt")));
			table.addCell(new Cell().add(new Paragraph("PLZ")));
			table.addCell(new Cell().add(new Paragraph("Telefon")));
			table.addCell(new Cell().add(new Paragraph("Az. Zimmer")));
			table.addCell(new Cell().add(new Paragraph("Az. Betten")));
			
			//add query results to the table			
			while (result.next()) {


				id = result.getString("kategorie");
				table.addCell(new Cell().add(new Paragraph(id)));
				
				kategorie2 = result.getString("kategorie2");
				table.addCell(new Cell().add(new Paragraph(kategorie2)));
				
				name = result.getString("name");
				table.addCell(new Cell().add(new Paragraph(name)));
				
				adresse = result.getString("adresse");
				table.addCell(new Cell().add(new Paragraph(adresse)));
				
				stadt = result.getString("kinderfreundlich");
				table.addCell(new Cell().add(new Paragraph(stadt)));
				
				plz = result.getString("plz");
				table.addCell(new Cell().add(new Paragraph(plz)));
				
				telefon = result.getString("telefon");
				table.addCell(new Cell().add(new Paragraph(telefon)));
			    
				azZimmer = result.getString("azZimmer");
			    table.addCell(new Cell().add(new Paragraph(azZimmer)));
			    
			    azBetten = result.getString("azBetten");
			    table.addCell(new Cell().add(new Paragraph(azBetten)));
			    
			    
			}
			
			//add Table to the document
			doc.add(table);
			
			doc.close();
			
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
			
		}
		
		return file;
	
	}
}
