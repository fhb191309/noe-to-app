package com.noe.to.web.backend.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.noe.to.web.backend.database.DatabaseConnect;
import com.noe.to.web.backend.entity.CombinedOverview;


public class CombinedOverviewService {

	/**
	 * Allgemeiner JOIN der Tabellen Hotels und Belegungen
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<CombinedOverview> getCombinedOverview() throws SQLException {
		ArrayList<CombinedOverview> allCombinedViewData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT * "
						+ "FROM Hotels h JOIN Belegungen b ON (h.id = b.id)"; 
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			/**
			 * Werte aus der Tabelle Hotels
			 */
			int id = result.getInt("id");
			String category = result.getString("kategorie");
			String hotelName = result.getString("name");
			String hotelOwner = result.getString("eigentuemer");
			String contact = result.getString("kontakt");
			String address = result.getString("adresse");
			String city = result.getString("stadt");
			String cityCode = result.getString("plz");
			String phone = result.getString("telefon");
			int noRooms  = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");
			
			/**
			 * Werte aus der Tabelle Belegungen
			 */
			int id2 = result.getInt("id");
			int rooms = result.getInt("azZimmer");
			int usedRooms = result.getInt("azBelegteZimmer");
			int beds = result.getInt("azBetten");
			int usedBeds = result.getInt("azBelegteBetten");
			int year = result.getInt("jahr");
			int month = result.getInt("monat");
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			double bettenauslastung = result.getDouble("bettenauslastung");
			int azNationalitäten = result.getInt("azNationalitäten");
		    String nationalitäten = result.getString("Nationalitäten");
		    int azGenesen = result.getInt("azGenesen");
		    int azGetestet = result.getInt("azGetestet");
		    int azGeimpft = result.getInt("azGeimpft");
			
						
			allCombinedViewData.add(new CombinedOverview(id, category, hotelName, hotelOwner, contact, address, city,
					cityCode, phone, noRooms, noBeds,kinderfreundlich, hundefreundlich, spa, fitness, id2, rooms, usedRooms,
					beds, usedBeds, year, month, zimmerauslastung, bettenauslastung, azNationalitäten, nationalitäten, 
					azGenesen, azGetestet, azGeimpft));

		}
		
		return allCombinedViewData;
		
	}
	
	/**
	 * Allgmeine Übersicht mit der Auswahl der ID
	 * 
	 * Vom Frontend muss ein Integer mit der ID übergeben werden
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<CombinedOverview> getCombinedOverviewByID(int id) throws SQLException {
		ArrayList<CombinedOverview> allCombinedViewData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT * "
						+ "FROM Hotels h JOIN Belegungen b ON (h.id = b.id) "
						+ "WHERE h.id = " + id; 
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			/**
			 * Werte aus der Tabelle Hotels
			 */
			int _id = result.getInt("id");
			String category = result.getString("kategorie");
			String hotelName = result.getString("name");
			String hotelOwner = result.getString("eigentuemer");
			String contact = result.getString("kontakt");
			String address = result.getString("adresse");
			String city = result.getString("stadt");
			String cityCode = result.getString("plz");
			String phone = result.getString("telefon");
			int noRooms  = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");
			
			/**
			 * Werte aus der Tabelle Belegungen
			 */
			int id2 = result.getInt("id");
			int rooms = result.getInt("azZimmer");
			int usedRooms = result.getInt("azBelegteZimmer");
			int beds = result.getInt("azBetten");
			int usedBeds = result.getInt("azBelegteBetten");
			int year = result.getInt("jahr");
			int month = result.getInt("monat");
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			double bettenauslastung = result.getDouble("bettenauslastung");
			int azNationalitäten = result.getInt("azNationalitäten");
		    String nationalitäten = result.getString("Nationalitäten");
		    int azGenesen = result.getInt("azGenesen");
		    int azGetestet = result.getInt("azGetestet");
		    int azGeimpft = result.getInt("azGeimpft");
			
						
			allCombinedViewData.add(new CombinedOverview(_id, category, hotelName, hotelOwner, contact, address, city,
					cityCode, phone, noRooms, noBeds,kinderfreundlich, hundefreundlich, spa, fitness, id2, rooms, usedRooms,
					beds, usedBeds, year, month, zimmerauslastung, bettenauslastung, azNationalitäten, nationalitäten, 
					azGenesen, azGetestet, azGeimpft));

		}
		
		return allCombinedViewData;
		
	}
	
	/**
	 * Allgmeine Übersicht mit der Auswahl der Kategorie
	 * 
	 * Vom Frontend muss ein String mit der Kategorie übergeben werden
	 * @param kategorie
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<CombinedOverview> getCombinedOverviewByCategory(String kategorie) throws SQLException {
		ArrayList<CombinedOverview> allCombinedViewData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT * "
						+ "FROM Hotels h JOIN Belegungen b ON (h.id = b.id) "
						+ "WHERE kategorie LIKE '" + kategorie + "'"; 
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			/**
			 * Werte aus der Tabelle Hotels
			 */
			int id = result.getInt("id");
			String category = result.getString("kategorie");
			String hotelName = result.getString("name");
			String hotelOwner = result.getString("eigentuemer");
			String contact = result.getString("kontakt");
			String address = result.getString("adresse");
			String city = result.getString("stadt");
			String cityCode = result.getString("plz");
			String phone = result.getString("telefon");
			int noRooms  = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");
			
			/**
			 * Werte aus der Tabelle Belegungen
			 */
			int id2 = result.getInt("id");
			int rooms = result.getInt("azZimmer");
			int usedRooms = result.getInt("azBelegteZimmer");
			int beds = result.getInt("azBetten");
			int usedBeds = result.getInt("azBelegteBetten");
			int year = result.getInt("jahr");
			int month = result.getInt("monat");
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			double bettenauslastung = result.getDouble("bettenauslastung");
			int azNationalitäten = result.getInt("azNationalitäten");
		    String nationalitäten = result.getString("Nationalitäten");
		    int azGenesen = result.getInt("azGenesen");
		    int azGetestet = result.getInt("azGetestet");
		    int azGeimpft = result.getInt("azGeimpft");
			
						
			allCombinedViewData.add(new CombinedOverview(id, category, hotelName, hotelOwner, contact, address, city,
					cityCode, phone, noRooms, noBeds,kinderfreundlich, hundefreundlich, spa, fitness, id2, rooms, usedRooms,
					beds, usedBeds, year, month, zimmerauslastung, bettenauslastung, azNationalitäten, nationalitäten, 
					azGenesen, azGetestet, azGeimpft));

		}
		
		return allCombinedViewData;
		
	}
	
	/**
	 * Allgmeine Übersicht mit der Auswahl des Jahres
	 * 
	 * Vom Frontend muss ein Integer mit dem Jahr übergeben werden
	 * @param jahr
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<CombinedOverview> getCombinedOverviewByYear(int jahr) throws SQLException {
		ArrayList<CombinedOverview> allCombinedViewData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT * "
						+ "FROM Hotels h JOIN Belegungen b ON (h.id = b.id) "
						+ "WHERE jahr = " + jahr; 
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			/**
			 * Werte aus der Tabelle Hotels
			 */
			int id = result.getInt("id");
			String category = result.getString("kategorie");
			String hotelName = result.getString("name");
			String hotelOwner = result.getString("eigentuemer");
			String contact = result.getString("kontakt");
			String address = result.getString("adresse");
			String city = result.getString("stadt");
			String cityCode = result.getString("plz");
			String phone = result.getString("telefon");
			int noRooms  = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");
			
			/**
			 * Werte aus der Tabelle Belegungen
			 */
			int id2 = result.getInt("id");
			int rooms = result.getInt("azZimmer");
			int usedRooms = result.getInt("azBelegteZimmer");
			int beds = result.getInt("azBetten");
			int usedBeds = result.getInt("azBelegteBetten");
			int year = result.getInt("jahr");
			int month = result.getInt("monat");
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			double bettenauslastung = result.getDouble("bettenauslastung");
			int azNationalitäten = result.getInt("azNationalitäten");
		    String nationalitäten = result.getString("Nationalitäten");
		    int azGenesen = result.getInt("azGenesen");
		    int azGetestet = result.getInt("azGetestet");
		    int azGeimpft = result.getInt("azGeimpft");
			
						
			allCombinedViewData.add(new CombinedOverview(id, category, hotelName, hotelOwner, contact, address, city,
					cityCode, phone, noRooms, noBeds,kinderfreundlich, hundefreundlich, spa, fitness, id2, rooms, usedRooms,
					beds, usedBeds, year, month, zimmerauslastung, bettenauslastung, azNationalitäten, nationalitäten, 
					azGenesen, azGetestet, azGeimpft));

		}
		
		return allCombinedViewData;
		
	}
	
	/**
	 * Allgmeine Übersicht mit der Auswahl der ID und dem Jahr
	 * 
	 * Vom Frontend müssen Integer mit der ID und dem Jahr übergeben werden
	 * @param id
	 * @param jahr
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<CombinedOverview> getCombinedOverviewByIdAndYear(int id, int jahr) throws SQLException {
		ArrayList<CombinedOverview> allCombinedViewData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT * "
						+ "FROM Hotels h JOIN Belegungen b ON (h.id = b.id) "
						+ "WHERE h.id = " + id + " and jahr = " + jahr; 
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			/**
			 * Werte aus der Tabelle Hotels
			 */
			int _id = result.getInt("id");
			String category = result.getString("kategorie");
			String hotelName = result.getString("name");
			String hotelOwner = result.getString("eigentuemer");
			String contact = result.getString("kontakt");
			String address = result.getString("adresse");
			String city = result.getString("stadt");
			String cityCode = result.getString("plz");
			String phone = result.getString("telefon");
			int noRooms  = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");
			
			/**
			 * Werte aus der Tabelle Belegungen
			 */
			int id2 = result.getInt("id");
			int rooms = result.getInt("azZimmer");
			int usedRooms = result.getInt("azBelegteZimmer");
			int beds = result.getInt("azBetten");
			int usedBeds = result.getInt("azBelegteBetten");
			int year = result.getInt("jahr");
			int month = result.getInt("monat");
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			double bettenauslastung = result.getDouble("bettenauslastung");
			int azNationalitäten = result.getInt("azNationalitäten");
		    String nationalitäten = result.getString("Nationalitäten");
		    int azGenesen = result.getInt("azGenesen");
		    int azGetestet = result.getInt("azGetestet");
		    int azGeimpft = result.getInt("azGeimpft");
			
						
			allCombinedViewData.add(new CombinedOverview(_id, category, hotelName, hotelOwner, contact, address, city,
					cityCode, phone, noRooms, noBeds,kinderfreundlich, hundefreundlich, spa, fitness, id2, rooms, usedRooms,
					beds, usedBeds, year, month, zimmerauslastung, bettenauslastung, azNationalitäten, nationalitäten, 
					azGenesen, azGetestet, azGeimpft));

		}
		
		return allCombinedViewData;
		
	}
	
	/**
	 * Allgmeine Übersicht mit der Auswahl der Kategorie und dem Jahr
	 * 
	 * Vom Frontend muss ein String mit der Kategorie und ein Integer mit dem Jahr übergeben werden
	 * @param kategorie
	 * @param jahr
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<CombinedOverview> getCombinedOverviewByCategoryAndYear(String kategorie, int jahr) throws SQLException {
		ArrayList<CombinedOverview> allCombinedViewData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT * "
						+ "FROM Hotels h JOIN Belegungen b ON (h.id = b.id) "
						+ "WHERE kategorie LIKE '" + kategorie + "' and jahr = " + jahr; 
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			/**
			 * Werte aus der Tabelle Hotels
			 */
			int id = result.getInt("id");
			String category = result.getString("kategorie");
			String hotelName = result.getString("name");
			String hotelOwner = result.getString("eigentuemer");
			String contact = result.getString("kontakt");
			String address = result.getString("adresse");
			String city = result.getString("stadt");
			String cityCode = result.getString("plz");
			String phone = result.getString("telefon");
			int noRooms  = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");
			
			/**
			 * Werte aus der Tabelle Belegungen
			 */
			int id2 = result.getInt("id");
			int rooms = result.getInt("azZimmer");
			int usedRooms = result.getInt("azBelegteZimmer");
			int beds = result.getInt("azBetten");
			int usedBeds = result.getInt("azBelegteBetten");
			int year = result.getInt("jahr");
			int month = result.getInt("monat");
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			double bettenauslastung = result.getDouble("bettenauslastung");
			int azNationalitäten = result.getInt("azNationalitäten");
		    String nationalitäten = result.getString("Nationalitäten");
		    int azGenesen = result.getInt("azGenesen");
		    int azGetestet = result.getInt("azGetestet");
		    int azGeimpft = result.getInt("azGeimpft");
			
						
			allCombinedViewData.add(new CombinedOverview(id, category, hotelName, hotelOwner, contact, address, city,
					cityCode, phone, noRooms, noBeds,kinderfreundlich, hundefreundlich, spa, fitness, id2, rooms, usedRooms,
					beds, usedBeds, year, month, zimmerauslastung, bettenauslastung, azNationalitäten, nationalitäten, 
					azGenesen, azGetestet, azGeimpft));

		}
		
		return allCombinedViewData;
		
	}
	
	/**
	 * Allgmeine Übersicht mit der Auswahl der ID, Monat und dem Jahr
	 * 
	 * Vom Frontend müssen Integer mit der ID, Monat und dem Jahr übergeben werden
	 * @param id
	 * @param monat
	 * @param jahr
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<CombinedOverview> getCombinedOverviewByIdAndMonth(int id, int monat, int jahr) throws SQLException {
		ArrayList<CombinedOverview> allCombinedViewData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT * "
						+ "FROM Hotels h JOIN Belegungen b ON (h.id = b.id) "
						+ "WHERE h.id = " + id + " and monat = " + monat + " and jahr = " + jahr; 
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			/**
			 * Werte aus der Tabelle Hotels
			 */
			int _id = result.getInt("id");
			String category = result.getString("kategorie");
			String hotelName = result.getString("name");
			String hotelOwner = result.getString("eigentuemer");
			String contact = result.getString("kontakt");
			String address = result.getString("adresse");
			String city = result.getString("stadt");
			String cityCode = result.getString("plz");
			String phone = result.getString("telefon");
			int noRooms  = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");
			
			/**
			 * Werte aus der Tabelle Belegungen
			 */
			int id2 = result.getInt("id");
			int rooms = result.getInt("azZimmer");
			int usedRooms = result.getInt("azBelegteZimmer");
			int beds = result.getInt("azBetten");
			int usedBeds = result.getInt("azBelegteBetten");
			int year = result.getInt("jahr");
			int month = result.getInt("monat");
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			double bettenauslastung = result.getDouble("bettenauslastung");
			int azNationalitäten = result.getInt("azNationalitäten");
		    String nationalitäten = result.getString("Nationalitäten");
		    int azGenesen = result.getInt("azGenesen");
		    int azGetestet = result.getInt("azGetestet");
		    int azGeimpft = result.getInt("azGeimpft");
			
						
			allCombinedViewData.add(new CombinedOverview(_id, category, hotelName, hotelOwner, contact, address, city,
					cityCode, phone, noRooms, noBeds,kinderfreundlich, hundefreundlich, spa, fitness, id2, rooms, usedRooms,
					beds, usedBeds, year, month, zimmerauslastung, bettenauslastung, azNationalitäten, nationalitäten, 
					azGenesen, azGetestet, azGeimpft));

		}
		
		return allCombinedViewData;
		
	}
	
	/**
	 * Allgmeine Übersicht mit der Auswahl der Kategorie, Monat und dem Jahr
	 * 
	 * Vom Frontend muss ein String mit der Kategorie und Integer mit dem Monat und Jahr übergeben werden
	 * @param kategorie
	 * @param monat
	 * @param jahr
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<CombinedOverview> getCombinedOverviewByCategoryAndMonth(String kategorie, int monat, int jahr) throws SQLException {
		ArrayList<CombinedOverview> allCombinedViewData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT * "
						+ "FROM Hotels h JOIN Belegungen b ON (h.id = b.id) "
						+ "WHERE kategorie LIKE '" + kategorie + "' and monat = " + monat + " and jahr = " + jahr; 
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			/**
			 * Werte aus der Tabelle Hotels
			 */
			int id = result.getInt("id");
			String category = result.getString("kategorie");
			String hotelName = result.getString("name");
			String hotelOwner = result.getString("eigentuemer");
			String contact = result.getString("kontakt");
			String address = result.getString("adresse");
			String city = result.getString("stadt");
			String cityCode = result.getString("plz");
			String phone = result.getString("telefon");
			int noRooms  = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");
			
			/**
			 * Werte aus der Tabelle Belegungen
			 */
			int id2 = result.getInt("id");
			int rooms = result.getInt("azZimmer");
			int usedRooms = result.getInt("azBelegteZimmer");
			int beds = result.getInt("azBetten");
			int usedBeds = result.getInt("azBelegteBetten");
			int year = result.getInt("jahr");
			int month = result.getInt("monat");
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			double bettenauslastung = result.getDouble("bettenauslastung");
			int azNationalitäten = result.getInt("azNationalitäten");
		    String nationalitäten = result.getString("Nationalitäten");
		    int azGenesen = result.getInt("azGenesen");
		    int azGetestet = result.getInt("azGetestet");
		    int azGeimpft = result.getInt("azGeimpft");
			
						
			allCombinedViewData.add(new CombinedOverview(id, category, hotelName, hotelOwner, contact, address, city,
					cityCode, phone, noRooms, noBeds,kinderfreundlich, hundefreundlich, spa, fitness, id2, rooms, usedRooms,
					beds, usedBeds, year, month, zimmerauslastung, bettenauslastung, azNationalitäten, nationalitäten, 
					azGenesen, azGetestet, azGeimpft));

		}
		
		return allCombinedViewData;
		
	}
}
