package com.noe.to.web.backend.entity;

public class TransDataOverview {

	/**
	 * Eigenschaften für die Klasse TransDataOverview
	 */
	String kategorie;
	int azZimmer;
	double zimmerauslastung;
	int azBetten;
	double bettenauslastung;
	int azNationalitäten;
	int azGenesen;
	int azGetestet;
	int azGeimpft;
	
	/**
	 * Optionale Eigenschaften
	 */
	int jahr;
	int monat;
	int id; 
	String name;
	
	/**
	 * Konstruktor für die Klasse TransDataOverview
	 * In mit Hilfe dieser Konstruktoren, werden die SQL Abfragen abgebildet
	 * 
	 * @param kategorie
	 * @param azZimmer
	 * @param zimmerauslastung
	 * @param azBetten
	 * @param bettenauslastung
	 * @param azNationalitäten
	 * @param azGenesen
	 * @param azGetestet
	 * @param azGeimpft
	 */
	public TransDataOverview(String kategorie, int azZimmer, double zimmerauslastung, int azBetten, double bettenauslastung,
			int azNationalitäten, int azGenesen, int azGetestet, int azGeimpft) {
		
		this.kategorie = kategorie;
		this.azZimmer = azZimmer;
		this.zimmerauslastung = zimmerauslastung;
		this.azBetten = azBetten;
		this.bettenauslastung = bettenauslastung;
		this.azNationalitäten = azNationalitäten;
		this.azGenesen = azGenesen;
		this.azGetestet = azGetestet;
		this.azGeimpft = azGeimpft;
	}
	
	/**
	 * Für die Abfragen pro Hotel
	 * 
	 * @param kategorie
	 * @param id
	 * @param name
	 * @param azZimmer
	 * @param zimmerauslastung
	 * @param azBetten
	 * @param bettenauslastung
	 * @param azNationalitäten
	 * @param azGenesen
	 * @param azGetestet
	 * @param azGeimpft
	 */
	public TransDataOverview(String kategorie, int id, String name, int azZimmer, double zimmerauslastung, int azBetten, double bettenauslastung,
			int azNationalitäten, int azGenesen, int azGetestet, int azGeimpft) {
		
		this.kategorie = kategorie;
		this.id = id;
		this.name = name;
		this.azZimmer = azZimmer;
		this.zimmerauslastung = zimmerauslastung;
		this.azBetten = azBetten;
		this.bettenauslastung = bettenauslastung;
		this.azNationalitäten = azNationalitäten;
		this.azGenesen = azGenesen;
		this.azGetestet = azGetestet;
		this.azGeimpft = azGeimpft;
	}
	
	/**
	 * Für die Abfrage pro Jahr
	 * 
	 * @param kategorie
	 * @param jahr
	 * @param azZimmer
	 * @param zimmerauslastung
	 * @param azBetten
	 * @param bettenauslastung
	 * @param azNationalitäten
	 * @param azGenesen
	 * @param azGetestet
	 * @param azGeimpft
	 */
	public TransDataOverview(String kategorie, int jahr, int azZimmer, double zimmerauslastung, int azBetten, double bettenauslastung,
			int azNationalitäten, int azGenesen, int azGetestet, int azGeimpft) {
		
		this.kategorie = kategorie;
		this.jahr = jahr;
		this.azZimmer = azZimmer;
		this.zimmerauslastung = zimmerauslastung;
		this.azBetten = azBetten;
		this.bettenauslastung = bettenauslastung;
		this.azNationalitäten = azNationalitäten;
		this.azGenesen = azGenesen;
		this.azGetestet = azGetestet;
		this.azGeimpft = azGeimpft;
	}
	
	/**
	 * Für die Abfrage pro Jahr und Hotel
	 * 
	 * @param kategorie
	 * @param jahr
	 * @param id
	 * @param name
	 * @param azZimmer
	 * @param zimmerauslastung
	 * @param azBetten
	 * @param bettenauslastung
	 * @param azNationalitäten
	 * @param azGenesen
	 * @param azGetestet
	 * @param azGeimpft
	 */
	public TransDataOverview(String kategorie, int jahr, int id, String name, int azZimmer, double zimmerauslastung, int azBetten, double bettenauslastung,
			int azNationalitäten, int azGenesen, int azGetestet, int azGeimpft) {
		
		this.kategorie = kategorie;
		this.jahr = jahr;
		this.id = id;
		this.name = name;
		this.azZimmer = azZimmer;
		this.zimmerauslastung = zimmerauslastung;
		this.azBetten = azBetten;
		this.bettenauslastung = bettenauslastung;
		this.azNationalitäten = azNationalitäten;
		this.azGenesen = azGenesen;
		this.azGetestet = azGetestet;
		this.azGeimpft = azGeimpft;
	}
	
	/**
	 * Für die Abfrage pro Jahr, pro monat und Hotel
	 * 
	 * @param kategorie
	 * @param jahr
	 * @param monat
	 * @param id
	 * @param name
	 * @param azZimmer
	 * @param zimmerauslastung
	 * @param azBetten
	 * @param bettenauslastung
	 * @param azNationalitäten
	 * @param azGenesen
	 * @param azGetestet
	 * @param azGeimpft
	 */
	public TransDataOverview(String kategorie, int jahr, int monat, int id, String name, int azZimmer, double zimmerauslastung, int azBetten, double bettenauslastung,
			int azNationalitäten, int azGenesen, int azGetestet, int azGeimpft) {
		
		this.kategorie = kategorie;
		this.jahr = jahr;
		this.monat = monat;
		this.id = id;
		this.name = name;
		this.azZimmer = azZimmer;
		this.zimmerauslastung = zimmerauslastung;
		this.azBetten = azBetten;
		this.bettenauslastung = bettenauslastung;
		this.azNationalitäten = azNationalitäten;
		this.azGenesen = azGenesen;
		this.azGetestet = azGetestet;
		this.azGeimpft = azGeimpft;
	}
	
	
	/**
	 * Für die Abfrage pro monat
	 * 
	 * @param kategorie
	 * @param jahr
	 * @param monat
	 * @param azZimmer
	 * @param zimmerauslastung
	 * @param azBetten
	 * @param bettenauslastung
	 * @param azNationalitäten
	 * @param azGenesen
	 * @param azGetestet
	 * @param azGeimpft
	 */
	public TransDataOverview(String kategorie, int jahr, int monat, int azZimmer, double zimmerauslastung, int azBetten, double bettenauslastung,
			int azNationalitäten, int azGenesen, int azGetestet, int azGeimpft) {
		
		this.kategorie = kategorie;
		this.jahr = jahr;
		this.monat = monat;
		this.azZimmer = azZimmer;
		this.zimmerauslastung = zimmerauslastung;
		this.azBetten = azBetten;
		this.bettenauslastung = bettenauslastung;
		this.azNationalitäten = azNationalitäten;
		this.azGenesen = azGenesen;
		this.azGetestet = azGetestet;
		this.azGeimpft = azGeimpft;
	}



	@Override
	public String toString() {
		return "TransDataOverview [kategorie=" + kategorie + ", azZimmer=" + azZimmer + ", zimmerauslastung="
				+ zimmerauslastung + ", azBetten=" + azBetten + ", bettenauslastung=" + bettenauslastung
				+ ", azNationalitäten=" + azNationalitäten + ", azGenesen=" + azGenesen + ", azGetestet=" + azGetestet
				+ ", azGeimpft=" + azGeimpft + ", jahr=" + jahr + ", monat=" + monat + ", id=" + id + ", name=" + name
				+ "]";
	}

	public String getKategorie() {
		return kategorie;
	}

	public void setKategorie(String kategorie) {
		this.kategorie = kategorie;
	}

	public int getAzZimmer() {
		return azZimmer;
	}

	public void setAzZimmer(int azZimmer) {
		this.azZimmer = azZimmer;
	}

	public Double getZimmerauslastung() {
		return zimmerauslastung;
	}

	public void setZimmerauslastung(int zimmerauslastung) {
		this.zimmerauslastung = zimmerauslastung;
	}

	public int getAzBetten() {
		return azBetten;
	}

	public void setAzBetten(int azBetten) {
		this.azBetten = azBetten;
	}

	public Double getBettenauslastung() {
		return bettenauslastung;
	}

	public void setBettenauslastung(int bettenauslastung) {
		this.bettenauslastung = bettenauslastung;
	}

	public int getAzNationalitäten() {
		return azNationalitäten;
	}

	public void setAzNationalitäten(int azNationalitäten) {
		this.azNationalitäten = azNationalitäten;
	}

	public int getAzGenesen() {
		return azGenesen;
	}

	public void setAzGenesen(int azGenesen) {
		this.azGenesen = azGenesen;
	}

	public int getAzGetestet() {
		return azGetestet;
	}

	public void setAzGetestet(int azGetestet) {
		this.azGetestet = azGetestet;
	}

	public int getAzGeimpft() {
		return azGeimpft;
	}

	public void setAzGeimpft(int azGeimpft) {
		this.azGeimpft = azGeimpft;
	}

	

}
