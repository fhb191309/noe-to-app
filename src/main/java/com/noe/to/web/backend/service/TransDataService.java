package com.noe.to.web.backend.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.noe.to.web.backend.database.DatabaseConnect;
import com.noe.to.web.backend.entity.TransDataOverview;

@Service
public class TransDataService {

	/**
	 * Allgemeine Übersicht der Transaktionsdaten
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<TransDataOverview> getTransDataOverview() throws SQLException {
		ArrayList<TransDataOverview> allTransData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT h.kategorie, SUM(h.azZimmer) AS 'Az. Zimmer', AVG(b.Zimmerauslastung) AS 'Zimmerauslastung', "
								+ "SUM(h.azBetten) AS 'Az. Betten', AVG(b.Bettenauslastung) AS 'Bettenauslastung', "
								+ "SUM(b.azNationalitäten) AS 'Az. Nationalitäten', "
								+ "SUM(b.azGenesen) AS 'Az. Genesen', SUM(b.azGetestet) AS 'Az. Getestet', SUM(b.azGeimpft) AS 'Az. Geimpft'"
							+ "FROM Belegungen b JOIN Hotels h ON (b.id = h.id) "
							+ "Group By h.kategorie "
							+ "Order By h.kategorie ASC";
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			String kategorie = result.getString(1);
			int azZimmer = result.getInt(2);
			double zimmerauslastung = result.getDouble(3);
			int azBetten = result.getInt(4);
			double bettenauslastung = result.getDouble(5);
			int azNationalitäten = result.getInt(6);
			int azGenesen = result.getInt(7);
			int azGetestet = result.getInt(8);
			int azGeimpft = result.getInt(9);
			
						
			allTransData.add(new TransDataOverview(kategorie, azZimmer, zimmerauslastung, azBetten, bettenauslastung, 
					azNationalitäten, azGenesen, azGetestet, azGeimpft));

		}
	
		return allTransData;
	}
	
	/**
	 * Allgemeine Übersicht der Transaktionsdaten pro Kategorie
	 * @param kategorie
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<TransDataOverview> getTransDataOverviewByCategory(String kategorie) throws SQLException {
		ArrayList<TransDataOverview> allTransData = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT h.kategorie, SUM(h.azZimmer) AS 'Az. Zimmer', AVG(b.Zimmerauslastung) AS 'Zimmerauslastung', "
								+ "SUM(h.azBetten) AS 'Az. Betten', AVG(b.Bettenauslastung) AS 'Bettenauslastung', "
								+ "SUM(b.azNationalitäten) AS 'Az. Nationalitäten', "
								+ "SUM(b.azGenesen) AS 'Az. Genesen', SUM(b.azGetestet) AS 'Az. Getestet', SUM(b.azGeimpft) AS 'Az. Geimpft'"
							+ "FROM Belegungen b JOIN Hotels h ON (b.id = h.id) "
							+ "Where kategorie Like '" + kategorie + "'"
							+ "Group By h.kategorie "
							+ "Order By h.kategorie ASC";
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			String kategorie2 = result.getString(1);
			int azZimmer = result.getInt(2);
			double zimmerauslastung = result.getDouble(3);
			int azBetten = result.getInt(4);
			double bettenauslastung = result.getDouble(5);
			int azNationalitäten = result.getInt(6);
			int azGenesen = result.getInt(7);
			int azGetestet = result.getInt(8);
			int azGeimpft = result.getInt(9);
						
			allTransData.add(new TransDataOverview(kategorie2, azZimmer, zimmerauslastung, azBetten, bettenauslastung, 
					azNationalitäten, azGenesen, azGetestet, azGeimpft));

		}
	
		return allTransData;
	}
	
	/**
	 * Allgemeine Übersicht der Transaktionsdaten pro Kategorie pro Monat und pro Jahr
	 * @param kategorie
	 * @param monat
	 * @param jahr
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<TransDataOverview> getTransDataOverviewByCategoryAndByMonth(String kategorie, int monat, int jahr) throws SQLException {
		ArrayList<TransDataOverview> allTransDataByHotel = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT kategorie, jahr, monat, SUM(DISTINCT h.azZimmer) AS 'Az. Zimmer', "
								+ "AVG(b.Zimmerauslastung) AS 'Zimmerauslastung', "
								+ "SUM(DISTINCT h.azBetten) AS 'Az. Betten', AVG(b.Bettenauslastung) AS 'Bettenauslastung', "
								+ "SUM(b.azNationalitäten) AS 'Az. Nationalitäten', "
								+ "SUM(b.azGenesen) AS 'Az. Genesen', SUM(b.azGetestet) AS 'Az. Getestet', SUM(b.azGeimpft) AS 'Az. Geimpft'"
							+ "FROM Belegungen b JOIN Hotels h ON (b.id = h.id) "
							+ "Where kategorie like '" + kategorie + "' and monat = " + monat + " and jahr = " + jahr 
							+ " Group By kategorie, jahr, monat "
							+ " ORDER By kategorie, jahr, monat ASC";
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			String kategorie2 = result.getString(1);
			int jahr2 = result.getInt(2);
			int monat2 = result.getInt(3);
			int azZimmer = result.getInt(4);
			double zimmerauslastung = result.getDouble(5);
			int azBetten = result.getInt(6);
			double bettenauslastung = result.getDouble(7);
			int azNationalitäten = result.getInt(8);
			int azGenesen = result.getInt(9);
			int azGetestet = result.getInt(10);
			int azGeimpft = result.getInt(11);
						
			allTransDataByHotel.add(new TransDataOverview(kategorie2, jahr2, monat2, azZimmer, zimmerauslastung, 
					azBetten, bettenauslastung, azNationalitäten, azGenesen, azGetestet, azGeimpft));

		}
	
		return allTransDataByHotel;
	}
	
	/**
	 * Allgmemeine Übersicht der Transaktionsdaten pro Hotel
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<TransDataOverview> getTransDataOverviewPerHotel() throws SQLException {
		ArrayList<TransDataOverview> allTransDataByHotel = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT kategorie, h.id AS 'id', h.name AS 'name', SUM(h.azZimmer) AS 'Az. Zimmer', "
							+ "AVG(b.Zimmerauslastung) AS 'Zimmerauslastung', "
							+ "SUM(h.azBetten) AS 'Az. Betten', AVG(b.Bettenauslastung) AS 'Bettenauslastung', "
							+ "SUM(b.azNationalitäten) AS 'Az. Nationalitäten' "
							+ "SUM(b.azGenesen) AS 'Az. Genesen', SUM(b.azGetestet) AS 'Az. Getestet', SUM(b.azGeimpft) AS 'Az. Geimpft'"
							+ "FROM Belegungen b JOIN Hotels h ON (b.id = h.id) "
							+ "Group By kategorie, h.id, h.name "
							+ "ORDER By kategorie, h.id ASC";
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			String kategorie = result.getString(1);
			int id = result.getInt(2);
			String name = result.getString(3);
			int azZimmer = result.getInt(4);
			double zimmerauslastung = result.getDouble(5);
			int azBetten = result.getInt(6);
			double bettenauslastung = result.getDouble(7);
			int azNationalitäten = result.getInt(8);
			int azGenesen = result.getInt(9);
			int azGetestet = result.getInt(10);
			int azGeimpft = result.getInt(11);
						
			allTransDataByHotel.add(new TransDataOverview(kategorie, id, name, azZimmer, zimmerauslastung, azBetten, 
					bettenauslastung, azNationalitäten, azGenesen, azGetestet, azGeimpft));

		}
	
		return allTransDataByHotel;
	}
	
	/**
	 * Allgmemeine Übersicht der Transaktionsdaten pro Hotel (pro ID)
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<TransDataOverview> getTransDataOverviewByID(int id) throws SQLException {
		ArrayList<TransDataOverview> allTransDataByHotel = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT h.kategorie, h.id AS 'id', h.name AS 'name', SUM(h.azZimmer) AS 'Az. Zimmer', "
							+ "AVG(b.Zimmerauslastung) AS 'Zimmerauslastung', "
							+ "SUM(h.azBetten) AS 'Az. Betten', AVG(b.Bettenauslastung) AS 'Bettenauslastung', "
							+ "SUM(b.azNationalitäten) AS 'Az. Nationalitäten',"
							+ "SUM(b.azGenesen) AS 'Az. Genesen', SUM(b.azGetestet) AS 'Az. Getestet', SUM(b.azGeimpft) AS 'Az. Geimpft' "
							+ "FROM Belegungen b JOIN Hotels h ON (b.id = h.id) "
							+ "WHERE b.id = " + id + " "
							+ "Group By h.kategorie, h.id, h.name "
							+ "ORDER By h.kategorie, h.id ASC";
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			String kategorie = result.getString(1);
			int id2 = result.getInt(2);
			String name = result.getString(3);
			int azZimmer = result.getInt(4);
			double zimmerauslastung = result.getDouble(5);
			int azBetten = result.getInt(6);
			double bettenauslastung = result.getDouble(7);
			int azNationalitäten = result.getInt(8);
			int azGenesen = result.getInt(9);
			int azGetestet = result.getInt(10);
			int azGeimpft = result.getInt(11);
						
			allTransDataByHotel.add(new TransDataOverview(kategorie, id2, name, azZimmer, zimmerauslastung, azBetten, 
					bettenauslastung, azNationalitäten, azGenesen, azGetestet, azGeimpft));

		}
	
		return allTransDataByHotel;
	}
	
	/**
	 * Allgmemeine Übersicht der Transaktionsdaten pro Hotel und pro jahr (mit Eingabe des Monats und des Jahres)
	 * @param id
	 * @param jahr
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<TransDataOverview> getTransDataOverviewByHotelAndByYears(int id, int jahr) throws SQLException {
		ArrayList<TransDataOverview> allTransDataByHotel = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "SELECT kategorie, jahr, h.id AS 'id', h.name AS 'name', SUM(DISTINCT h.azZimmer) AS 'Az. Zimmer', "
								+ "AVG(b.Zimmerauslastung) AS 'Zimmerauslastung', "
								+ "SUM(DISTINCT h.azBetten) AS 'Az. Betten', AVG(b.Bettenauslastung) AS 'Bettenauslastung', "
								+ "SUM(b.azNationalitäten) AS 'Az. Nationalitäten', "
								+ "SUM(b.azGenesen) AS 'Az. Genesen', SUM(b.azGetestet) AS 'Az. Getestet', SUM(b.azGeimpft) AS 'Az. Geimpft'"
							+ "FROM Belegungen b JOIN Hotels h ON (b.id = h.id) "
							+ "Where h.id = " + id + " and jahr = " + jahr 
							+ "Group By kategorie, jahr, h.id, h.name "
							+ "ORDER By kategorie, jahr, h.id ASC";
		

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			String kategorie = result.getString(1);
			int jahr2 = result.getInt(2);
			int id2 = result.getInt(3);
			String name = result.getString(4);
			int azZimmer = result.getInt(5);
			double zimmerauslastung = result.getDouble(6);
			int azBetten = result.getInt(7);
			double bettenauslastung = result.getDouble(8);
			int azNationalitäten = result.getInt(9);
			int azGenesen = result.getInt(10);
			int azGetestet = result.getInt(11);
			int azGeimpft = result.getInt(12);
						
			allTransDataByHotel.add(new TransDataOverview(kategorie, jahr2, id2, name, azZimmer, zimmerauslastung, azBetten, 
					bettenauslastung, azNationalitäten, azGenesen, azGetestet, azGeimpft));

		}
	
		return allTransDataByHotel;
	}
	
	/**
	 * Allgmemeine Übersicht der Transaktionsdaten pro Hotel und pro jahr
	 * @return
	 * @throws SQLException
	 */
		public ArrayList<TransDataOverview> getTransDataOverviewPerHotelAndYears() throws SQLException {
			ArrayList<TransDataOverview> allTransDataByHotel = new ArrayList<>();

			Connection con = null;
			Statement statement;
			String statementSQL;

			ResultSet result;

			con = DatabaseConnect.getConnection();
			statement = con.createStatement();

			statementSQL = "SELECT kategorie, jahr, h.id AS 'id', h.name AS 'name', SUM(DISTINCT h.azZimmer) AS 'Az. Zimmer', "
									+ "AVG(b.Zimmerauslastung) AS 'Zimmerauslastung', "
									+ "SUM(DISTINCT h.azBetten) AS 'Az. Betten', AVG(b.Bettenauslastung) AS 'Bettenauslastung', "
									+ "SUM(b.azNationalitäten) AS 'Az. Nationalitäten', "
									+ "SUM(b.azGenesen) AS 'Az. Genesen', SUM(b.azGetestet) AS 'Az. Getestet', SUM(b.azGeimpft) AS 'Az. Geimpft'"
								+ "FROM Belegungen b JOIN Hotels h ON (b.id = h.id) "
								+ "Group By kategorie, jahr, h.id, h.name "
								+ "ORDER By kategorie, jahr, h.id ASC";
			

			System.out.println("Executing the following query: " + statementSQL);

			result = statement.executeQuery(statementSQL);

			
			while (result.next()) {

				String kategorie = result.getString(1);
				int jahr = result.getInt(2);
				int id = result.getInt(3);
				String name = result.getString(4);
				int azZimmer = result.getInt(5);
				double zimmerauslastung = result.getDouble(6);
				int azBetten = result.getInt(7);
				double bettenauslastung = result.getDouble(8);
				int azNationalitäten = result.getInt(9);
				int azGenesen = result.getInt(10);
				int azGetestet = result.getInt(11);
				int azGeimpft = result.getInt(12);
							
				allTransDataByHotel.add(new TransDataOverview(kategorie, jahr, id, name, azZimmer, zimmerauslastung, azBetten, 
						bettenauslastung, azNationalitäten, azGenesen, azGetestet, azGeimpft));

			}
		
			return allTransDataByHotel;
		}
		
		/**
		 * Allgmemeine Übersicht der Transaktionsdaten pro Hotel und pro monat und jahr
		 * @return
		 * @throws SQLException
		 */
		public ArrayList<TransDataOverview> getTransDataOverviewPerHotelAndMonth() throws SQLException {
			ArrayList<TransDataOverview> allTransDataByHotel = new ArrayList<>();

			Connection con = null;
			Statement statement;
			String statementSQL;

			ResultSet result;

			con = DatabaseConnect.getConnection();
			statement = con.createStatement();

			statementSQL = "SELECT kategorie, jahr, monat, h.id AS 'id', h.name AS 'name', SUM(DISTINCT h.azZimmer) AS 'Az. Zimmer', "
									+ "AVG(b.Zimmerauslastung) AS 'Zimmerauslastung', "
									+ "SUM(DISTINCT h.azBetten) AS 'Az. Betten', AVG(b.Bettenauslastung) AS 'Bettenauslastung', "
									+ "SUM(b.azNationalitäten) AS 'Az. Nationalitäten', "
									+ "SUM(b.azGenesen) AS 'Az. Genesen', SUM(b.azGetestet) AS 'Az. Getestet', SUM(b.azGeimpft) AS 'Az. Geimpft'"
								+ "FROM Belegungen b JOIN Hotels h ON (b.id = h.id) "
								+ "Group By kategorie, jahr, monat, h.id, h.name "
								+ "ORDER By kategorie, jahr, monat, h.id ASC";
			

			System.out.println("Executing the following query: " + statementSQL);

			result = statement.executeQuery(statementSQL);

			
			while (result.next()) {

				String kategorie = result.getString(1);
				int jahr = result.getInt(2);
				int monat = result.getInt(3);
				int id = result.getInt(4);
				String name = result.getString(5);
				int azZimmer = result.getInt(6);
				double zimmerauslastung = result.getDouble(7);
				int azBetten = result.getInt(8);
				double bettenauslastung = result.getDouble(9);
				int azNationalitäten = result.getInt(10);
				int azGenesen = result.getInt(11);
				int azGetestet = result.getInt(12);
				int azGeimpft = result.getInt(13);
							
				allTransDataByHotel.add(new TransDataOverview(kategorie, jahr, monat, id, name, azZimmer, zimmerauslastung, 
						azBetten, bettenauslastung, azNationalitäten, azGenesen, azGetestet, azGeimpft));

			}
		
			return allTransDataByHotel;
		}
		
		/**
		 * Allgmemeine Übersicht der Transaktionsdaten pro Hotel und pro jahr (mit Eingabe dr ID, des Monats und des Jahres)
		 * @param id
		 * @param monat
		 * @param jahr
		 * @return
		 * @throws SQLException
		 */
		public static ArrayList<TransDataOverview> getTransDataOverviewByHotelAndByMonth(int id, int monat, int jahr) throws SQLException {
			ArrayList<TransDataOverview> allTransDataByHotel = new ArrayList<>();

			Connection con = null;
			Statement statement;
			String statementSQL;

			ResultSet result;

			con = DatabaseConnect.getConnection();
			statement = con.createStatement();

			statementSQL = "SELECT kategorie, jahr, monat, h.id AS 'id', h.name AS 'name', SUM(DISTINCT h.azZimmer) AS 'Az. Zimmer', "
									+ "AVG(b.Zimmerauslastung) AS 'Zimmerauslastung', "
									+ "SUM(DISTINCT h.azBetten) AS 'Az. Betten', AVG(b.Bettenauslastung) AS 'Bettenauslastung', "
									+ "SUM(b.azNationalitäten) AS 'Az. Nationalitäten', "
									+ "SUM(b.azGenesen) AS 'Az. Genesen', SUM(b.azGetestet) AS 'Az. Getestet', SUM(b.azGeimpft) AS 'Az. Geimpft'"
								+ "FROM Belegungen b JOIN Hotels h ON (b.id = h.id) "
								+ "Where h.id = " + id + " and monat = " + monat + " and jahr = " + jahr 
								+ " Group By kategorie, jahr, monat, h.id, h.name "
								+ " ORDER By jahr, monat, h.id ASC";
			

			System.out.println("Executing the following query: " + statementSQL);

			result = statement.executeQuery(statementSQL);

			
			while (result.next()) {

				String kategorie = result.getString(1);
				int jahr2 = result.getInt(2);
				int monat2 = result.getInt(3);
				int id2 = result.getInt(4);
				String name = result.getString(5);
				int azZimmer = result.getInt(6);
				double zimmerauslastung = result.getDouble(7);
				int azBetten = result.getInt(8);
				double bettenauslastung = result.getDouble(9);
				int azNationalitäten = result.getInt(10);
				int azGenesen = result.getInt(11);
				int azGetestet = result.getInt(12);
				int azGeimpft = result.getInt(13);
							
				allTransDataByHotel.add(new TransDataOverview(kategorie, jahr2, monat2, id2, name, azZimmer, zimmerauslastung, 
						azBetten, bettenauslastung, azNationalitäten, azGenesen, azGetestet, azGeimpft));

			}
		
			return allTransDataByHotel;
		}
		
		
	
}
