package com.noe.to.web.backend.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.springframework.stereotype.Service;

import com.noe.to.web.backend.BackendUtils;
import com.noe.to.web.backend.database.DatabaseConnect;
import com.noe.to.web.backend.entity.Hotel;
import com.noe.to.web.backend.entity.MasterDataOverview;
import com.vaadin.flow.component.notification.Notification;


@Service
public class HotelService {

	
	/**
	 * CRUDs for the Hotel Class
	 * C..Create
	 * R..Read
	 * U..Update
	 * D..Delete
	 */

	/**
	 * Hier wird ein Hotel mit seiner ID gesucht und gelöscht werden
	 * 
	 * Vom Frontend MUSS ein Integer übergeben werden
	 * @param id
	 */
	public static void deleteHotelByID(int id) {

		String DELETE_STATEMENT = "delete from Hotels where id = ?";

		try (Connection connection = DatabaseConnect.getConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_STATEMENT)) {

		
			statement.setInt(1, id);

			int rows = statement.executeUpdate();
			System.out.println(rows);

			if (rows == 0) {

				Notification.show("No Hotel was deleted");
			}

			System.out.println();

			System.out.println("done");

		} catch (SQLException e) {
			System.out.println(e.getErrorCode());

		}

	}
	
	/**
	 * Hier wird ein Hotel mit seiner ID gesucht und upgedatet
	 * 
	 * Vom Frontend MUSS ein Integer und ein Objekt der Klasse Hotel übergeben werden
	 * @param id
	 * @param hotel
	 */
	public static void updateHotelByID(int id, Hotel hotel) {

		String UPDATE_STATEMENT = " update Hotels set kategorie = ?, name = ?, eigentuemer = ?,"
									+ "kontakt = ?, adresse = ?, stadt = ?, plz = ?,"
									+ "telefon = ?, azZimmer = ?, azBetten = ?, kinderfreundlich = ?,"
									+ "hundefreundlich = ?, spa = ?, fitness = ? where id = " + id;

		try (Connection connection = DatabaseConnect.getConnection();
				PreparedStatement statement = connection.prepareStatement(UPDATE_STATEMENT)) {

			
			statement.setString(1, hotel.getCategory().toString());
			statement.setString(2, hotel.getHotelName().toString());
			statement.setString(3, hotel.getHotelOwner().toString());
			statement.setString(4, hotel.getContact().toString());
			statement.setString(5, hotel.getAddress().toString());
			statement.setString(6, hotel.getCity().toString());
			statement.setString(7, hotel.getCityCode().toString());
			statement.setString(8, hotel.getPhone().toString());
			statement.setInt(9, hotel.getNoRooms());
			statement.setInt(10, hotel.getNoBeds());
			statement.setString(11, hotel.getKinderfreundlich().toString());
			statement.setString(12, hotel.getHundefreundlich().toString());
			statement.setString(13, hotel.getSpa().toString());
			statement.setString(14, hotel.getFitness().toString());

			
			int rows = statement.executeUpdate();
			System.out.println(rows);

			if (rows == 0) {
				JOptionPane.showMessageDialog(null, "No Hotel was found, no update at all");

			}

			System.out.println();

			System.out.println("done");

		} catch (SQLException e) {
			System.out.println(e.getErrorCode());

		}

	}

	
	/**
	 * Hier wird ein neues Hotel in die Datenbank hinzugefügt
	 * 
	 * Vom Frontend MUSS ein Objekt der Klasse Hotel übergeben werden
	 * @param hotel
	 * @throws SQLException
	 */
	public static void insertNewHotel(Hotel hotel) throws SQLException {

		Connection con = null;
		PreparedStatement queryStatement = null;

		String statement = "INSERT INTO Hotels values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		System.out.println("Executing query: " + statement);

		con = DatabaseConnect.getConnection();
		queryStatement = con.prepareStatement(statement);

		queryStatement.setInt(1, BackendUtils.getMaxId()+1);
		queryStatement.setString(2, hotel.getCategory().toString());
		queryStatement.setString(3, hotel.getHotelName());
		queryStatement.setString(4, hotel.getHotelOwner());
		queryStatement.setString(5, hotel.getContact());
		queryStatement.setString(6, hotel.getAddress());
		queryStatement.setString(7, hotel.getCity());
		queryStatement.setString(8, hotel.getCityCode());
		queryStatement.setString(9, hotel.getPhone());
		queryStatement.setInt(10, hotel.getNoRooms());
		queryStatement.setInt(11, hotel.getNoBeds());
		queryStatement.setString(12, hotel.getKinderfreundlich().toString());
		queryStatement.setString(13, hotel.getHundefreundlich().toString());
		queryStatement.setString(14, hotel.getSpa().toString());
		queryStatement.setString(15, hotel.getFitness().toString());

		int countOfRows = queryStatement.executeUpdate();

		ResultSet rs = queryStatement.getGeneratedKeys();

		System.out.println("Hotel saved " + countOfRows);

	}
	

	/**
	 * Hier werden alle Hotels aus der Datenbank ausgegeben
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Hotel> getAllHotels() throws SQLException {
		ArrayList<Hotel> allHotels = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select * from Hotels";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		
		while (result.next()) {

			int id = result.getInt("id");
			String category = result.getString("kategorie");
			String hotelName = result.getString("name");
			String hotelOwner = result.getString("eigentuemer");
			String contact = result.getString("kontakt");
			String address = result.getString("adresse");
			String city = result.getString("stadt");
			String cityCode = result.getString("plz");
			String phone = result.getString("telefon");
			int noRooms  = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");
						
			
			allHotels.add(new Hotel(id, category, hotelName, hotelOwner,
					contact, address, city, cityCode, phone, noRooms, noBeds, kinderfreundlich, hundefreundlich, spa, fitness));

		}

		return allHotels;
	}

	
	/**
	 * Hier wird ein Hotel mit seiner ID gesucht 
	 * 
	 * Vom Frontend MUSS ein Integer übergeben werden
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static Hotel getHotelById(int id) throws SQLException {

		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select * from Hotels where id = " + id;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);


		
		Hotel m = null;
		while (result.next()) {

			int id1 = result.getInt("id");
			String category = result.getString("kategorie");
			String hotelName = result.getString("name");
			String hotelOwner = result.getString("eigentuemer");
			String contact = result.getString("kontakt");
			String address = result.getString("adresse");
			String city = result.getString("stadt");
			String cityCode = result.getString("plz");
			String phone = result.getString("telefon");
			int noRooms  = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");

			m = new Hotel(id1, category, hotelName, hotelOwner,
					contact, address, city, cityCode, phone, noRooms, noBeds, kinderfreundlich, hundefreundlich, spa, fitness);

		}
	
		return m;
	}
	

	public static ArrayList<Hotel> getHotelByUserId(int id) throws SQLException {

		ArrayList<Hotel> allHotels = new ArrayList<>();
		
		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select * from Hotels where id = " + id;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);


		
		Hotel m = null;
		while (result.next()) {

			int id1 = result.getInt("id");
			String category = result.getString("kategorie");
			String hotelName = result.getString("name");
			String hotelOwner = result.getString("eigentuemer");
			String contact = result.getString("kontakt");
			String address = result.getString("adresse");
			String city = result.getString("stadt");
			String cityCode = result.getString("plz");
			String phone = result.getString("telefon");
			int noRooms  = result.getInt("azZimmer");
			int noBeds = result.getInt("azBetten");
			String kinderfreundlich = result.getString("kinderfreundlich");
			String hundefreundlich = result.getString("hundefreundlich");
			String spa = result.getString("spa");
			String fitness = result.getString("fitness");

			allHotels.add(new Hotel(id1, category, hotelName, hotelOwner,
					contact, address, city, cityCode, phone, noRooms, noBeds, kinderfreundlich, hundefreundlich, spa, fitness));

		}
	
		return allHotels; 
	}
	
	
	/**
	 * Hier werden alle Hotel Namen aus der Tabelle 'Hotel' ausgeben
	 * und in einer ArrayList gespeichert
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<String> getHotelName() throws SQLException {
		ArrayList<String> allHotelNames = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select name from Hotels";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {

			String nameString = result.getString("name");
	
			allHotelNames.add(nameString);

		}

		return allHotelNames;
	}
	
	/**
	 * Hier werden alle Hotel IDs aus der Tabelle 'Hotel' ausgeben
	 * und in einer ArrayList gespeichert
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Integer> getHotelID() throws SQLException {
		ArrayList<Integer> allHotelNames = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select id from Hotels";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {

			int id = result.getInt("id");
			
			allHotelNames.add(id);

		}

		return allHotelNames;
	}
	
	/**
	 * Hier werden alle Hotel Eigentuemer aus der Tabelle 'Hotel' ausgeben
	 * und in einer ArrayList gespeichert
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<String> getOwnerName() throws SQLException {
		ArrayList<String> allOwnerNames = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select eigentuemer from Hotels";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {


			String OwnerString = result.getString("eigentuemer");
	
			allOwnerNames.add(OwnerString);

		}

		return allOwnerNames;
	}
	
	/**
	 * Hier wird ein Hotel mit seiner ID gesucht  und ausgegeben
	 * 
	 * Vom Frontend muss ein Integer übergeben werden
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static String getHotelNameById(int id) throws SQLException {

		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select name from Hotels where id = " + id;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);


		
		String s = "";
		while (result.next()) {

			
			String hotelName = result.getString("name");
			

			s += hotelName;
		}
	
		return s;
	}
	
	/**
	 * Hier wird ein Hotel mit seinem Namen gesucht  und ausgegeben
	 * 
	 * Vom Frontend muss der Name des Hotels übergeben werden
	 * @param name
	 * @return
	 * @throws SQLException
	 */
	public static int getHotelIdbyName(String name) throws SQLException {

		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select id from Hotels where name = '" + name + "';";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);


		
		int s = -1;
		while (result.next()) {

			
			int hotelID = result.getInt("id");
			

			s = hotelID;
		}
			return s;

	}
	
}
