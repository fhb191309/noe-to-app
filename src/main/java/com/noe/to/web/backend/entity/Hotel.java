package com.noe.to.web.backend.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.noe.to.web.backend.BackendUtils;


public class Hotel implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * Sets und Maps für die Klasse Hotel
	 */
	private static Set<Hotel> hotels = new HashSet<Hotel>();
	private static Map<String,Hotel> hotelsByName = new TreeMap<>();
	private static Map<Integer,Hotel> hotelsByID = new HashMap<>();
	
	/**
	 * Eigenschaften der Klasse Hotel
	 */
	int hotelID;
	String category;
	String hotelName;
	String hotelOwner;
	String contact;
	String address;
	String city;
	String cityCode;
	String phone;
	int noRooms;
	int noBeds;
	String kinderfreundlich;
	String hundefreundlich;
	String spa;
	String fitness;
	
	/**
	 * Konstruktor für die Klasse Hotel OHNE der ID
	 * @param category
	 * @param name
	 * @param owner
	 * @param contact
	 * @param address
	 * @param city
	 * @param cityCode
	 * @param phone
	 * @param noRooms
	 * @param noBeds
	 * @param kinderfreundlich
	 * @param hundefreundlich
	 * @param spa
	 * @param fitness
	 */
	public Hotel(String category, String name, String owner, String contact, String address,
		     String city, String cityCode, String phone, int noRooms, int noBeds, String kinderfreundlich, String hundefreundlich, String spa, String fitness) {
	Hotel.checkData(true, category, name, owner, contact, address, city, cityCode, phone, noRooms, noBeds, kinderfreundlich, hundefreundlich, spa, fitness);
	
	this.category = category;
	this.hotelName = name;
	this.hotelOwner = owner;
	this.contact = contact;
	this.address = address;
	this.city = city;
	this.cityCode = cityCode;
	this.phone = phone;
	this.noRooms = noRooms;
	this.noBeds = noBeds;
	this.kinderfreundlich = kinderfreundlich;
	this.hundefreundlich = hundefreundlich;
	this.spa = spa;
	this.fitness = fitness;
	
	}
	
	
	/**
	 * Weiterer Konstruktor für die Klasse MIT der ID
	 * @param id
	 * @param category
	 * @param name
	 * @param owner
	 * @param contact
	 * @param address
	 * @param city
	 * @param cityCode
	 * @param phone
	 * @param noRooms
	 * @param noBeds
	 * @param kinderfreundlich
	 * @param hundefreundlich
	 * @param spa
	 * @param fitness
	 */
	public Hotel(int id, String category, String name, String owner, String contact, String address,
		     String city, String cityCode, String phone, int noRooms, int noBeds, String kinderfreundlich, String hundefreundlich, String spa, String fitness) {
	Hotel.checkData(true, category, name, owner, contact, address, city, cityCode, phone, noRooms, noBeds, kinderfreundlich, hundefreundlich, spa, fitness);
	
	this.hotelID = id;
	this.category = category;
	this.hotelName = name;
	this.hotelOwner = owner;
	this.contact = contact;
	this.address = address;
	this.city = city;
	this.cityCode = cityCode;
	this.phone = phone;
	this.noRooms = noRooms;
	this.noBeds = noBeds;
	this.kinderfreundlich = kinderfreundlich;
	this.hundefreundlich = hundefreundlich;
	this.spa = spa;
	this.fitness = fitness;
	}
	
	
	/**
     * Hier werden die eingegeben Daten auf ihre Richtigkeit und Korrektheit überprüft
     */
	public static final void checkData (Boolean checkForUniqueName, String category, String hotelName, String hotelOwner,
			String contact, String address, String city, String cityCode, String phone, int noRooms, int noBeds, String kinderfreundlich,
			String hundefreundlich, String spa, String fitness) {
	BackendUtils.checkStringNotEmpty(hotelName,"Hotelname is empty");
	BackendUtils.checkStringNotEmpty(hotelOwner,"Hotelowner is empty");
	BackendUtils.checkStringNotEmpty(contact,"Contact is empty");
	BackendUtils.checkStringNotEmpty(address,"Adress is empty");
	BackendUtils.checkStringNotEmpty(city,"City is empty");
	BackendUtils.checkStringNotEmpty(cityCode,"PLZ is empty");
	BackendUtils.checkStringNotEmpty(phone,"Phone number is empty");
	BackendUtils.checkIntPositive(noRooms, "Number of rooms is not positiv");
	BackendUtils.checkIntPositive(noBeds, "Number of beds is not posity");
	

	if (checkForUniqueName)
		BackendUtils.checkUniqueName(hotelName, getHotelNames(), "Hotelname bereits vergeben");
	}
	
	@Override
	public String toString() {
		return "Hotel [hotelID=" + hotelID + ", category=" + category + ", hotelName=" + hotelName + ", hotelOwner="
				+ hotelOwner + ", contact=" + contact + ", address=" + address + ", city=" + city + ", cityCode="
				+ cityCode + ", phone=" + phone + ", noRooms=" + noRooms + ", noBeds=" + noBeds + ", kinderfreundlich=" + kinderfreundlich + ", hundefreundlich=" + hundefreundlich
				+ ", spa=" + spa + ", fitness=" + fitness + "]";
	}
	
	/**
	 * Hier können die Daten geändert werden, die ID kann NICHT geändert werden
	 */
	public void setData(String category, String name, String owner, String contact, String address,
		     String city, String cityCode, String phone, int noRooms, int noBeds, String kinderfreundlich,
				String hundefreundlich, String spa, String fitness) {
		checkData(!this.hotelName.equals(name), category, name, owner, contact, address, city, cityCode, phone, noRooms, noBeds, kinderfreundlich, hundefreundlich, spa, fitness);
		this.category = category;
		this.hotelName = name;
		this.hotelName = owner;
		this.contact = contact;
		this.address = address;
		this.city = city;
		this.cityCode = cityCode;
		this.phone = phone;
		this.noRooms = noRooms;
		this.noBeds = noBeds;
		this.kinderfreundlich = kinderfreundlich;
		this.hundefreundlich = hundefreundlich;
		this.spa = spa;
		this.fitness = fitness;
	}

	public static Set<Hotel> getHotels() {
		return hotels;
	}
	
	public static Set<String> getHotelNames() {
		return hotelsByName.keySet();
	}
	

	public int getHotelID() {
		return hotelID;
	}
	
	public void setHotelID(int hotelID) {
		this.hotelID = hotelID;
	}
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getHotelName() {
		return hotelName;
	}
	
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	
	public String getHotelOwner() {
		return hotelOwner;
	}
	
	public void setHotelOwner(String hotelOwner) {
		this.hotelOwner = hotelOwner;
	}
	
	public String getContact() {
		return contact;
	}
	
	public void setContact(String contact) {
		this.contact = contact;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCityCode() {
		return cityCode;
	}
	
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public int getNoRooms() {
		return noRooms;
	}
	
	public void setNoRooms(int noRooms) {
		this.noRooms = noRooms;
	}
	
	public int getNoBeds() {
		return noBeds;
	}
	
	public void setNoBeds(int noBeds) {
		this.noBeds = noBeds;
	}
	

	public String getKinderfreundlich() {
		return kinderfreundlich;
	}

	public void setKinderfreundlich(String kinderfreundlich) {
		this.kinderfreundlich = kinderfreundlich;
	}

	public String getHundefreundlich() {
		return hundefreundlich;
	}

	public void setHundefreundlich(String hundefreundlich) {
		this.hundefreundlich = hundefreundlich;
	}

	public String getSpa() {
		return spa;
	}

	public void setSpa(String spa) {
		this.spa = spa;
	}

	public String getFitness() {
		return fitness;
	}

	public void setFitness(String fitness) {
		this.fitness = fitness;
	}

	



	
	
}
