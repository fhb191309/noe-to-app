package com.noe.to.web.backend.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.hibernate.boot.model.source.internal.hbm.ManyToOneAttributeColumnsAndFormulasSource;
import org.springframework.stereotype.Service;

import com.noe.to.web.backend.database.DatabaseConnect;
import com.noe.to.web.backend.entity.Hotel;
import com.noe.to.web.backend.entity.MasterDataOverview;
import com.noe.to.web.backend.entity.Occupancy;
import com.noe.to.web.backend.entity.TransDataOverview;
import com.vaadin.flow.component.notification.Notification;


@Service
public class OccupancyService {


	/**
	 * CRUDs for the Occupancy Class
	 * C..Create
	 * R..Read
	 * U..Update
	 * D..Delete
	 */
	

	/**
	 * Hier werden Transaktionsdaten mit ihrer ID gesucht und gelöscht werden
	 * 
	 * Vom Frontend MUSS ein Integer übergeben werden
	 * @param id
	 */
	public static void deleteOccupancyByID(int id) {

		String DELETE_STATEMENT = "delete from Belegungen where id = ?";

		try (Connection connection = DatabaseConnect.getConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_STATEMENT)) {

		
			statement.setInt(1, id);

			int rows = statement.executeUpdate();
			System.out.println(rows);

			if (rows == 0) {
				//JOptionPane.showMessageDialog(null, "No person was deleted");
				Notification.show("No Occupancy was deleted");
			}

			System.out.println();

			System.out.println("done");

		} catch (SQLException e) {
			System.out.println(e.getErrorCode());

		}

	}
	
	/**
	 * Hier werden Transaktionsdaten mit dem Primary key der Tabelle (id, monat und jahr) gesucht und upgedatet werden
	 * 
	 * Vom Frontend müssen die id, monat, jahr und ein Objekt der Klasse Occupancy übergeben werden
	 * @param id
	 * @param monat
	 * @param jahr
	 * @param occupancy
	 */
	public static void updateOccupancyByID(int id, int monat, int jahr, Occupancy occupancy) throws SQLException {

		String UPDATE_STATEMENT = " update Belegungen set azZimmer = ?, azBelegteZimmer = ?, azBetten = ?,"
									+ " azBelegteBetten = ?, azNationalitäten = ?, Nationalitäten = ?, "
									+ " azGenesen = ?, azGetestet = ?, azGeimpft = ? where id = " + id + " and jahr = " + jahr + " and monat = " + monat;

		try (Connection connection = DatabaseConnect.getConnection();
				PreparedStatement statement = connection.prepareStatement(UPDATE_STATEMENT)) {

			//statement.setInt(1, occupancy.getHotelID());
			statement.setInt(1, occupancy.getNoRooms());
			statement.setInt(2, occupancy.getUsedRooms());
			statement.setInt(3, occupancy.getNoBeds());
			statement.setInt(4, occupancy.getUsedBeds());
			//statement.setInt(6, occupancy.getYear());
			//statement.setInt(7, occupancy.getMonth());
			statement.setInt(5, occupancy.getAzNationalitäten());
			statement.setString(6, occupancy.getNationalitäten().toString());
			statement.setInt(7, occupancy.getAzGenesen());
			statement.setInt(8, occupancy.getAzGetestet());
			statement.setInt(9, occupancy.getAzGeimpft());

			int rows = statement.executeUpdate();
			System.out.println(rows);

			if (rows == 0) {
				JOptionPane.showMessageDialog(null, "No Hotel was found, no update at all");

			}

			System.out.println();

			System.out.println("done");

		} catch (SQLException e) {
			System.out.println(e.getErrorCode());

		}

	}
	

	/**
	 * Hier werden neue Transaktionsdaten in die Tabelle 'Belegungen' hinzugefügt
	 * 
	 * Vom Frontend MUSS Objekt der Klasse Occupancy übergeben werden
	 * @param occupancy
	 * @throws SQLException
	 */
	public static void insertNewOccupancy(Occupancy occupancy) throws SQLException {

		Connection con = null;
		PreparedStatement queryStatement = null;

		String statement = "INSERT INTO Belegungen values (?,?,?,?,?,?,?,?,?,?,?,?)";

		System.out.println("Executing query: " + statement);

		con = DatabaseConnect.getConnection();
		queryStatement = con.prepareStatement(statement);

		queryStatement.setInt(1, occupancy.getHotelID());
		queryStatement.setInt(2, occupancy.getNoRooms());
		queryStatement.setInt(3, occupancy.getUsedRooms());
		queryStatement.setInt(4, occupancy.getNoBeds());
		queryStatement.setInt(5, occupancy.getUsedBeds());
		queryStatement.setInt(6, occupancy.getYear());
		queryStatement.setInt(7, occupancy.getMonth());
//		queryStatement.setDouble(8, occupancy.getZimmerauslastung());
//		queryStatement.setDouble(9, occupancy.getBettenauslastung());
		queryStatement.setInt(8, occupancy.getAzNationalitäten());
		queryStatement.setString(9, occupancy.getNationalitäten().toString());
		queryStatement.setInt(10, occupancy.getAzGenesen());
		queryStatement.setInt(11, occupancy.getAzGetestet());
		queryStatement.setInt(12, occupancy.getAzGeimpft());

		int countOfRows = queryStatement.executeUpdate();

		//ResultSet rs = queryStatement.getGeneratedKeys();

		System.out.println("Occupancy saved " + countOfRows);

	}
	
	/**
	 * Hier werden alle Transaktionsdaten aus der Tabelle 'Belegungen'ausgegeben
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Occupancy> getAllOccupancies() throws SQLException {
		ArrayList<Occupancy> allOccupancies = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select * from Belegungen";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {

			int id = result.getInt("id");
			int rooms = result.getInt("azZimmer");
			int usedRooms = result.getInt("azBelegteZimmer");
			int beds = result.getInt("azBetten");
			int usedBeds = result.getInt("azBelegteBetten");
			int year = result.getInt("jahr");
			int month = result.getInt("monat");
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			double bettenauslastung = result.getDouble("bettenauslastung");
			int azNationalitäten = result.getInt("azNationalitäten");
		    String nationalitäten = result.getString("Nationalitäten");
		    int azGenesen = result.getInt("azGenesen");
		    int azGetestet = result.getInt("azGetestet");
		    int azGeimpft = result.getInt("azGeimpft");
			
		    allOccupancies.add(new Occupancy(id, rooms, usedRooms, beds, usedBeds, year, month, zimmerauslastung, 
					bettenauslastung, azNationalitäten, nationalitäten, azGenesen, azGetestet, azGeimpft));
		}

		return allOccupancies;
	}


	/**
	 * Hier werden alle Transaktionsdaten mit ihrer ID gesucht und ausgegeben
	 * 
	 * Vom Frontend MUSS ein Integer übergeben werden
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Occupancy> getOccupancyById(int id) throws SQLException {

		ArrayList<Occupancy> allOccupancies = new ArrayList<>();
		
		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select * from Belegungen where id = " + id;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {
			
			int _id = result.getInt("id");
			int rooms = result.getInt("azZimmer");
			int usedRooms = result.getInt("azBelegteZimmer");
			int beds = result.getInt("azBetten");
			int usedBeds = result.getInt("azBelegteBetten");
			int year = result.getInt("jahr");
			int month = result.getInt("monat");
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			double bettenauslastung = result.getDouble("bettenauslastung");
			int azNationalitäten = result.getInt("azNationalitäten");
		    String nationalitäten = result.getString("Nationalitäten");
		    int azGenesen = result.getInt("azGenesen");
		    int azGetestet = result.getInt("azGetestet");
		    int azGeimpft = result.getInt("azGeimpft");
						
			allOccupancies.add(new Occupancy(_id, rooms, usedRooms, beds, usedBeds, year, month, zimmerauslastung, 
					bettenauslastung, azNationalitäten, nationalitäten, azGenesen, azGetestet, azGeimpft));

		}

		return allOccupancies;
	}
	
	/**
	 * Hier werden alle Transaktionsdaten mit ihrer ID, monat und jahr gesucht und ausgegeben
	 * 
	 * Vom Frontend MUSS ein Integer mit der ID, monat und jahr übergeben werden
	 * @param id
	 * @param monat
	 * @param jahr
	 * @return
	 * @throws SQLException
	 */
	public static Occupancy OccupancyById(int id, int monat, int jahr) throws SQLException {

		Occupancy occupancy = null;
		
		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select * from Belegungen where id = " + id + " and monat = " + monat + " and jahr = " + jahr;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {
			
			int _id = result.getInt("id");
			int rooms = result.getInt("azZimmer");
			int usedRooms = result.getInt("azBelegteZimmer");
			int beds = result.getInt("azBetten");
			int usedBeds = result.getInt("azBelegteBetten");
			int year = result.getInt("jahr");
			int month = result.getInt("monat");
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			double bettenauslastung = result.getDouble("bettenauslastung");
			int azNationalitäten = result.getInt("azNationalitäten");
		    String nationalitäten = result.getString("Nationalitäten");
		    int azGenesen = result.getInt("azGenesen");
		    int azGetestet = result.getInt("azGetestet");
		    int azGeimpft = result.getInt("azGeimpft");
						
			occupancy = new Occupancy(_id, rooms, usedRooms, beds, usedBeds, year, month, zimmerauslastung, 
					bettenauslastung, azNationalitäten, nationalitäten, azGenesen, azGetestet, azGeimpft);

		}

		return occupancy;
	}
	

	/**
	 * Hier werden alle IDs der Transaktionsdaten aus der Tabelle 'Belegungen'
	 * gesammelt und ausgegeben
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Integer> GetOccupancyID() throws SQLException {
		ArrayList<Integer> allOccupancies = new ArrayList<>();

		Connection con = null;
		Statement statement;
		String statementSQL;

		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select id from Belegungen";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {

			int id = result.getInt("id");
			
			allOccupancies.add(id);

		}

		return allOccupancies;
	}
	
	/**
	 * Hier werden alle Jahre aus der Datenbanktabelle Belegungen ausgegeben
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Integer> getAllOccupancyYears() throws SQLException {

		ArrayList<Integer> allYears = new ArrayList<>();
		
		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select DISTINCT jahr from Belegungen Order By jahr ASC";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {
			
			
			int jahr = result.getInt("jahr");
			
						
			allYears.add(jahr);

		}

		return allYears;
	}
	
	/**
	 * Hier werden alle Monate aus der Datenbanktabelle Belegungen ausgegeben
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Integer> getAllOccupancyMonths() throws SQLException {

		ArrayList<Integer> allMonths = new ArrayList<>();
		
		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select DISTINCT monat from Belegungen Order By monat ASC";

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {
			
			
			int monat = result.getInt("monat");
			
						
			allMonths.add(monat);

		}

		return allMonths;
	}
	
	/**
	 * Hier werden die Zimmerauslastungen aus der Datenbanktabelle Belegungen ausgegeben
	 * @param jahr
	 * @param monat
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Double> getRoomOccupancyByMonth(int jahr, int monat) throws SQLException {

		ArrayList<Double> RoomOccupancies = new ArrayList<>();
		
		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select AVG(zimmerauslastung) From Belegungen Where monat = " + monat + "and jahr = " + jahr;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {
			
			
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			
						
			RoomOccupancies.add(zimmerauslastung);

		}

		return RoomOccupancies;
	}
	
	/**
	 * Hier werden die Zimmerauslastungen pro Jahr aus der Datenbanktabelle Belegungen ausgegeben
	 * 
	 * Vom Frontend muss das Jahr als Integer übergeben werden
	 * @param jahr
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Double> getRoomOccupancyByYear(int jahr) throws SQLException {

		ArrayList<Double> RoomOccupancies = new ArrayList<>();
		
		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select AVG(zimmerauslastung) From Belegungen Where jahr = " + jahr;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {
			
			
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			
						
			RoomOccupancies.add(zimmerauslastung);

		}

		return RoomOccupancies;
	}
	
	/**
	 * Hier werden die Bettenauslastungen pro Monat aus der Datenbanktabelle Belegungen ausgegeben
	 * 
	 * Vom Frontend muss das Jahr und das Monat als Integer übergeben werden
	 * @param jahr
	 * @param monat
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Double> getBedOccupancyByMonth(int jahr, int monat) throws SQLException {

		ArrayList<Double> BedOccupancies = new ArrayList<>();
		
		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select AVG(bettenauslastung) From Belegungen Where monat = " + monat + "and jahr = " + jahr;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {
			
			
			double bettenauslastung = result.getDouble("bettenauslastung");
			
						
			BedOccupancies.add(bettenauslastung);

		}

		return BedOccupancies;
	}
	
	/**
	 * Hier werden eine Bettenauslastung pro Monat aus der Datenbanktabelle Belegungen ausgegeben
	 * 
	 * Vom Frontend muss das Jahr und das Monat als Integer übergeben werden
	 * @param jahr
	 * @param monat
	 * @return
	 * @throws SQLException
	 */
	public static Double getBedOccupancyPerMonth(int jahr, int monat) throws SQLException {

		Double BedOccupancies = null;
		
		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select AVG(bettenauslastung) AS 'bettenauslastung' From Belegungen Where monat = " + monat + "and jahr = " + jahr;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {
			
			
			double bettenauslastung = result.getDouble("bettenauslastung");
			
						
			BedOccupancies = bettenauslastung;

		}

		return BedOccupancies;
	}
	
	/**
	 * Hier werden eine Zimmerauslastung pro Monat aus der Datenbanktabelle Belegungen ausgegeben
	 * 
	 * Vom Frontend muss das Jahr und das Monat als Integer übergeben werden
	 * @param jahr
	 * @param monat
	 * @return
	 * @throws SQLException
	 */
	public static Double getRoomOccupancyPerMonth(int jahr, int monat) throws SQLException {

		Double RoomOccupancies = null;
		
		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select AVG(zimmerauslastung) AS 'zimmerauslastung' From Belegungen Where monat = " + monat + "and jahr = " + jahr;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {
			
			
			double zimmerauslastung = result.getDouble("zimmerauslastung");
			
						
			RoomOccupancies = zimmerauslastung;

		}

		return RoomOccupancies;
	}
	
	/**
	 * Hier werden eine Bettenauslastungen pro Jahr aus der Datenbanktabelle Belegungen ausgegeben
	 * 
	 * Vom Frontend muss das Jahr als Integer übergeben werden
	 * @param jahr
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Double> getBedOccupancyByYear(int jahr) throws SQLException {

		ArrayList<Double> BedOccupancies = new ArrayList<>();
		
		Connection con = null;
		Statement statement;
		String statementSQL;
		ResultSet result;

		con = DatabaseConnect.getConnection();
		statement = con.createStatement();

		statementSQL = "Select AVG(bettenauslastung) From Belegungen Where jahr = " + jahr;

		System.out.println("Executing the following query: " + statementSQL);

		result = statement.executeQuery(statementSQL);

		while (result.next()) {
			
			
			double bettenauslastung = result.getDouble("bettenauslastung");
			
						
			BedOccupancies.add(bettenauslastung);

		}

		return BedOccupancies;
	}

}
