package com.noe.to.web.backend.entity;

public class CombinedOverview {

	/**
	 * Eigenschaften der Klasse Hotel
	 */
	int hotelID;
	String category;
	String hotelName;
	String hotelOwner;
	String contact;
	String address;
	String city;
	String cityCode;
	String phone;
	int noRooms;
	int noBeds;
	String kinderfreundlich;
	String hundefreundlich;
	String spa;
	String fitness;
	
	/**
	 * Eigenschaften für die Klasse Occupancy
	 */
	int hotelID2; 
    int noRooms2;
    int usedRooms;
    int noBeds2;
    int usedBeds;
    int year;
    int month;
    double zimmerauslastung;
    double bettenauslastung;
    int azNationalitäten;
    String nationalitäten;
    int azGenesen;
    int azGetestet;
    int azGeimpft;
    
    
	public CombinedOverview(int hotelID, String category, String hotelName, String hotelOwner, String contact,
			String address, String city, String cityCode, String phone, int noRooms, int noBeds,
			String kinderfreundlich, String hundefreundlich, String spa, String fitness, int hotelID2, int noRooms2,
			int usedRooms, int noBeds2, int usedBeds, int year, int month, double zimmerauslastung,
			double bettenauslastung, int azNationalitäten, String nationalitäten, int azGenesen, int azGetestet,
			int azGeimpft) {
		
		this.hotelID = hotelID;
		this.category = category;
		this.hotelName = hotelName;
		this.hotelOwner = hotelOwner;
		this.contact = contact;
		this.address = address;
		this.city = city;
		this.cityCode = cityCode;
		this.phone = phone;
		this.noRooms = noRooms;
		this.noBeds = noBeds;
		this.kinderfreundlich = kinderfreundlich;
		this.hundefreundlich = hundefreundlich;
		this.spa = spa;
		this.fitness = fitness;
		this.hotelID2 = hotelID2;
		this.noRooms2 = noRooms2;
		this.usedRooms = usedRooms;
		this.noBeds2 = noBeds2;
		this.usedBeds = usedBeds;
		this.year = year;
		this.month = month;
		this.zimmerauslastung = zimmerauslastung;
		this.bettenauslastung = bettenauslastung;
		this.azNationalitäten = azNationalitäten;
		this.nationalitäten = nationalitäten;
		this.azGenesen = azGenesen;
		this.azGetestet = azGetestet;
		this.azGeimpft = azGeimpft;
	}


	@Override
	public String toString() {
		return "CombinedOverview [hotelID=" + hotelID + ", category=" + category + ", hotelName=" + hotelName
				+ ", hotelOwner=" + hotelOwner + ", contact=" + contact + ", address=" + address + ", city=" + city
				+ ", cityCode=" + cityCode + ", phone=" + phone + ", noRooms=" + noRooms + ", noBeds=" + noBeds
				+ ", kinderfreundlich=" + kinderfreundlich + ", hundefreundlich=" + hundefreundlich + ", spa=" + spa
				+ ", fitness=" + fitness + ", hotelID2=" + hotelID2 + ", noRooms2=" + noRooms2 + ", usedRooms="
				+ usedRooms + ", noBeds2=" + noBeds2 + ", usedBeds=" + usedBeds + ", year=" + year + ", month=" + month
				+ ", zimmerauslastung=" + zimmerauslastung + ", bettenauslastung=" + bettenauslastung
				+ ", azNationalitäten=" + azNationalitäten + ", nationalitäten=" + nationalitäten + ", azGenesen="
				+ azGenesen + ", azGetestet=" + azGetestet + ", azGeimpft=" + azGeimpft + "]";
	}


	public int getHotelID() {
		return hotelID;
	}


	public void setHotelID(int hotelID) {
		this.hotelID = hotelID;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getHotelName() {
		return hotelName;
	}


	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}


	public String getHotelOwner() {
		return hotelOwner;
	}


	public void setHotelOwner(String hotelOwner) {
		this.hotelOwner = hotelOwner;
	}


	public String getContact() {
		return contact;
	}


	public void setContact(String contact) {
		this.contact = contact;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getCityCode() {
		return cityCode;
	}


	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public int getNoRooms() {
		return noRooms;
	}


	public void setNoRooms(int noRooms) {
		this.noRooms = noRooms;
	}


	public int getNoBeds() {
		return noBeds;
	}


	public void setNoBeds(int noBeds) {
		this.noBeds = noBeds;
	}


	public String getKinderfreundlich() {
		return kinderfreundlich;
	}


	public void setKinderfreundlich(String kinderfreundlich) {
		this.kinderfreundlich = kinderfreundlich;
	}


	public String getHundefreundlich() {
		return hundefreundlich;
	}


	public void setHundefreundlich(String hundefreundlich) {
		this.hundefreundlich = hundefreundlich;
	}


	public String getSpa() {
		return spa;
	}


	public void setSpa(String spa) {
		this.spa = spa;
	}


	public String getFitness() {
		return fitness;
	}


	public void setFitness(String fitness) {
		this.fitness = fitness;
	}


	public int getHotelID2() {
		return hotelID2;
	}


	public void setHotelID2(int hotelID2) {
		this.hotelID2 = hotelID2;
	}


	public int getNoRooms2() {
		return noRooms2;
	}


	public void setNoRooms2(int noRooms2) {
		this.noRooms2 = noRooms2;
	}


	public int getUsedRooms() {
		return usedRooms;
	}


	public void setUsedRooms(int usedRooms) {
		this.usedRooms = usedRooms;
	}


	public int getNoBeds2() {
		return noBeds2;
	}


	public void setNoBeds2(int noBeds2) {
		this.noBeds2 = noBeds2;
	}


	public int getUsedBeds() {
		return usedBeds;
	}


	public void setUsedBeds(int usedBeds) {
		this.usedBeds = usedBeds;
	}


	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public int getMonth() {
		return month;
	}


	public void setMonth(int month) {
		this.month = month;
	}


	public double getZimmerauslastung() {
		return zimmerauslastung;
	}


	public void setZimmerauslastung(double zimmerauslastung) {
		this.zimmerauslastung = zimmerauslastung;
	}


	public double getBettenauslastung() {
		return bettenauslastung;
	}


	public void setBettenauslastung(double bettenauslastung) {
		this.bettenauslastung = bettenauslastung;
	}


	public int getAzNationalitäten() {
		return azNationalitäten;
	}


	public void setAzNationalitäten(int azNationalitäten) {
		this.azNationalitäten = azNationalitäten;
	}


	public String getNationalitäten() {
		return nationalitäten;
	}


	public void setNationalitäten(String nationalitäten) {
		this.nationalitäten = nationalitäten;
	}


	public int getAzGenesen() {
		return azGenesen;
	}


	public void setAzGenesen(int azGenesen) {
		this.azGenesen = azGenesen;
	}


	public int getAzGetestet() {
		return azGetestet;
	}


	public void setAzGetestet(int azGetestet) {
		this.azGetestet = azGetestet;
	}


	public int getAzGeimpft() {
		return azGeimpft;
	}


	public void setAzGeimpft(int azGeimpft) {
		this.azGeimpft = azGeimpft;
	}
    
    
}
