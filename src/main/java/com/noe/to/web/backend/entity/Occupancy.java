package com.noe.to.web.backend.entity;

import java.io.Serializable;

import com.noe.to.web.backend.BackendUtils;


public class Occupancy implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public static final int maxHistoryYears = 8;
	
	/**
	 * Eigenschaften für die Klasse Occupancy
	 */
	int hotelID; 
    int noRooms;
    int usedRooms;
    int noBeds;
    int usedBeds;
    int year;
    int month;
    double zimmerauslastung;
    double bettenauslastung;
    int azNationalitäten;
    String nationalitäten;
    int azGenesen;
    int azGetestet;
    int azGeimpft;
    
    /**
     * Konstruktor für die Klasse Occupancy
     * @param hotelID
     * @param noRooms
     * @param usedRooms
     * @param noBeds
     * @param usedBeds
     * @param year
     * @param month
     * @param zimmerauslastung
     * @param bettenauslastung
     */
    public Occupancy(int hotelID, int noRooms, int usedRooms, int noBeds, int usedBeds, 
    		     int year, int month, double zimmerauslastung, double bettenauslastung) {
		
    	checkData(hotelID, noRooms, usedRooms, noBeds, usedBeds, year);
    	
    	this.hotelID = hotelID;
		this.noRooms = noRooms;
		this.usedRooms = usedRooms;
		this.noBeds = noBeds;
		this.usedBeds = usedBeds;
		this.year = year;
		this.month = month;
		this.zimmerauslastung = zimmerauslastung;
		this.bettenauslastung = bettenauslastung;
	}
    
    
    
    /**
     * Zweiter Konstruktor mit den Attributen Nationalität und den COIVD Informationen/Stati der Touristen
     * @param hotelID
     * @param noRooms
     * @param usedRooms
     * @param noBeds
     * @param usedBeds
     * @param year
     * @param month
     * @param zimmerauslastung
     * @param bettenauslastung
     * @param azNationalitäten
     * @param nationalitäten
     * @param azGenesen
     * @param azGetestet
     * @param azGeimpft
     */
    public Occupancy(int hotelID, int noRooms, int usedRooms, int noBeds, int usedBeds, int year, int month,
			double zimmerauslastung, double bettenauslastung, int azNationalitäten, String nationalitäten,
			int azGenesen, int azGetestet, int azGeimpft) {
		
    	checkDataWithCovid(hotelID, noRooms, usedRooms, noBeds, usedBeds, year, month,
    			azNationalitäten, nationalitäten, azGenesen, azGetestet, azGeimpft);
    	
		this.hotelID = hotelID;
		this.noRooms = noRooms;
		this.usedRooms = usedRooms;
		this.noBeds = noBeds;
		this.usedBeds = usedBeds;
		this.year = year;
		this.month = month;
		this.zimmerauslastung = zimmerauslastung;
		this.bettenauslastung = bettenauslastung;
		this.azNationalitäten = azNationalitäten;
		this.nationalitäten = nationalitäten;
		this.azGenesen = azGenesen;
		this.azGetestet = azGetestet;
		this.azGeimpft = azGeimpft;
	}

    

	@Override
	public String toString() {
		return "Occupancy [hotelID=" + hotelID + ", noRooms=" + noRooms + ", usedRooms=" + usedRooms + ", noBeds="
				+ noBeds + ", usedBeds=" + usedBeds + ", year=" + year + ", month=" + month + ", zimmerauslastung="
				+ zimmerauslastung + ", bettenauslastung=" + bettenauslastung + ", azNationalitäten=" + azNationalitäten
				+ ", nationalitäten=" + nationalitäten + ", azGenesen=" + azGenesen + ", azGetestet=" + azGetestet
				+ ", azGeimpft=" + azGeimpft + "]";
	}




	/**
	 * Hier werden die eingegeben Daten auf ihre Richtigkeit und Korrektheit überprüft
	 * @param hotelID
	 * @param noRooms
	 * @param usedRooms
	 * @param noBeds
	 * @param usedBeds
	 * @param year
	 */
    public static final void checkData(int hotelID, int noRooms, int usedRooms, 
    									int noBeds, int usedBeds, int year) {
    	
    	BackendUtils.checkIntPositive(hotelID, "HotelID is not positiv");
    	BackendUtils.checkIntPositive(noRooms, "Number of rooms is not positiv");
    	BackendUtils.checkIntPositive(usedRooms, "Number of used rooms is not positiv");
    	BackendUtils.checkIntPositive(noBeds, "Number of beds is not positiv");
    	BackendUtils.checkIntPositive(usedBeds, "Number of used beds is not positiv");
    	BackendUtils.checkYear(year, "Year is lower than 2013");
    	
    }
    
    /**
     * Hier werden die eingegeben Daten auf ihre Richtigkeit und Korrektheit überprüft
     * Es kommen hier noch die Covid Status abfragen dazu
     * @param hotelID
     * @param noRooms
     * @param usedRooms
     * @param noBeds
     * @param usedBeds
     * @param year
     * @param month
     * @param azNationalitäten
     * @param nationalitäten
     * @param azGenesen
     * @param azGetestet
     * @param azGeimpft
     */
    public static final void checkDataWithCovid(int hotelID, int noRooms, int usedRooms, int noBeds, int usedBeds, int year, int month,
			int azNationalitäten, String nationalitäten, int azGenesen, int azGetestet, int azGeimpft) {

		BackendUtils.checkIntPositive(hotelID, "HotelID is not positiv");
		BackendUtils.checkIntPositive(noRooms, "Number of rooms is not positiv");
		BackendUtils.checkIntPositive(usedRooms, "Number of used rooms is not positiv");
		BackendUtils.checkIntPositive(noBeds, "Number of beds is not positiv");
		BackendUtils.checkIntPositive(usedBeds, "Number of used beds is not positiv");
		BackendUtils.checkYear(year, "Year is lower than 2013");
		BackendUtils.checkIntPositive(azNationalitäten, "Number of Nationalities is not positiv");
		//BackendUtils.checkStringNotEmpty(nationalitäten,"Nationality is empty");
		BackendUtils.checkIntPositive(azGenesen, "Number of Tourists, who had COVID is not positiv");
		BackendUtils.checkIntPositive(azGetestet, "Number of tested Tourists is not positiv");
		BackendUtils.checkIntPositive(azGeimpft, "Number of vaccinated Tourists is not positiv");
	
	}


	public int getHotelID() {
		return hotelID;
	}


	public void setHotelID(int hotelID) {
		this.hotelID = hotelID;
	}


	public int getNoRooms() {
		return noRooms;
	}


	public void setNoRooms(int noRooms) {
		this.noRooms = noRooms;
	}


	public int getUsedRooms() {
		return usedRooms;
	}


	public void setUsedRooms(int usedRooms) {
		this.usedRooms = usedRooms;
	}


	public int getNoBeds() {
		return noBeds;
	}


	public void setNoBeds(int noBeds) {
		this.noBeds = noBeds;
	}


	public int getUsedBeds() {
		return usedBeds;
	}


	public void setUsedBeds(int usedBeds) {
		this.usedBeds = usedBeds;
	}


	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public int getMonth() {
		return month;
	}


	public void setMonth(int month) {
		this.month = month;
	}

	public double getZimmerauslastung() {
		return zimmerauslastung;
	}

	public void setZimmerauslastung(double zimmerauslastung) {
		this.zimmerauslastung = zimmerauslastung;
	}

	public double getBettenauslastung() {
		return bettenauslastung;
	}

	public void setBettenauslastung(double bettenauslastung) {
		this.bettenauslastung = bettenauslastung;
	}


	public int getAzNationalitäten() {
		return azNationalitäten;
	}


	public void setAzNationalitäten(int azNationalitäten) {
		this.azNationalitäten = azNationalitäten;
	}


	public String getNationalitäten() {
		return nationalitäten;
	}


	public void setNationalitäten(String nationalitäten) {
		this.nationalitäten = nationalitäten;
	}


	public int getAzGenesen() {
		return azGenesen;
	}


	public void setAzGenesen(int azGenesen) {
		this.azGenesen = azGenesen;
	}


	public int getAzGetestet() {
		return azGetestet;
	}


	public void setAzGetestet(int azGetestet) {
		this.azGetestet = azGetestet;
	}


	public int getAzGeimpft() {
		return azGeimpft;
	}


	public void setAzGeimpft(int azGeimpft) {
		this.azGeimpft = azGeimpft;
	}
	
	

}
