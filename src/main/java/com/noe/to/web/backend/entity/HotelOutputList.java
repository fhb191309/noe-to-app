package com.noe.to.web.backend.entity;

public class HotelOutputList {

	/**
	 * Eigenschaften der Klasse HotelOutputList
	 */
	int hotelID;
	String hotelName;
	String address;
	String city;
	String cityCode;
	String phone;
	int noRooms;
	int noBeds;
	
	
	public HotelOutputList(int hotelID, String hotelName, String address, String city, String cityCode, String phone,
			int noRooms, int noBeds) {
		
		this.hotelID = hotelID;
		this.hotelName = hotelName;
		this.address = address;
		this.city = city;
		this.cityCode = cityCode;
		this.phone = phone;
		this.noRooms = noRooms;
		this.noBeds = noBeds;
	}


	@Override
	public String toString() {
		return "HotelOutputList [hotelID=" + hotelID + ", hotelName=" + hotelName + ", address=" + address + ", city="
				+ city + ", cityCode=" + cityCode + ", phone=" + phone + ", noRooms=" + noRooms + ", noBeds=" + noBeds
				+ "]";
	}


	public int getHotelID() {
		return hotelID;
	}


	public void setHotelID(int hotelID) {
		this.hotelID = hotelID;
	}


	public String getHotelName() {
		return hotelName;
	}


	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getCityCode() {
		return cityCode;
	}


	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public int getNoRooms() {
		return noRooms;
	}


	public void setNoRooms(int noRooms) {
		this.noRooms = noRooms;
	}


	public int getNoBeds() {
		return noBeds;
	}


	public void setNoBeds(int noBeds) {
		this.noBeds = noBeds;
	}
	
	
	
}
