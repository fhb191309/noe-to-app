package com.noe.to.web.backend.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnect {

	/**
	 * Azure SQL Server jdbc authentication:
	 * 
	 * jdbc:sqlserver://noetoserver.database.windows.net:1433;database=noetoDB;user=ebase@noetoserver;password={your_password_here};encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;
	 */
	
	/**
	 * Eigenschaften der Klasse
	 */
	static String url = "jdbc:sqlserver://";
	static String serverName = "noetoserver.database.windows.net";
	static String portNumber = "1433";
	static String dbName = "noetoDB";
	static String userName = "ebase@noetoserver";
	static String pwd = "Elite2021";
	
	
	/**
	 *  In dieser Methode wird die Verbindung zur Datenbank aufgebaut
	 * @param args
	 */
	public static void main(String[] args)  {

				
		try {
			Connection con = getConnection();
			
			System.out.println("Connecting to SQL Server...");
			System.out.println(con);
			
			System.out.println("Done");
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

	}

	/**
	 * Hier kann eine Verbindung zu Datenbank mit der Eingabe vom Benutzernamen und 
	 * dem Passwort hergestellt werden
	 * @param _user
	 * @param _pwd
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection(String _user, String _pwd) throws SQLException {
		
		Connection con = null;
		
		con = DriverManager.getConnection(connectionUrl(), _user, _pwd);
		return con;
	}

	
	/**
	 * Hier wird eine Verbindung zur Datenbank mit den oben definierten 
	 * Benutzernamen und Passwort hergestellt
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException {
		
		Connection con = null;
		
		con = DriverManager.getConnection(connectionUrl(), userName, pwd);
		
		return con;
	}


	/**
	 * Diese Methode verbindet die einzelnen Strings zu einem URL
	 * @return
	 */
	private static String connectionUrl() {
		
		return url 
				+ serverName
				+":"
				+portNumber
				+";database="
				+dbName
				+";user="
				+userName
				+";password="
				+pwd
				+"encrypt=true;trustServerCertificate=false;hostNameInCertificate"
				+ "=*.database.windows.net;loginTimeout=30;";
	}
	
}
