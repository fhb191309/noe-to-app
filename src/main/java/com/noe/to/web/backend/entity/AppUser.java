package com.noe.to.web.backend.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.noe.to.web.backend.BackendUtils;


public class AppUser implements UserDetails{ 

	private static final long serialVersionUID = 1L;
	
	/**
	 * ArrayList zum speichern aller AppUser
	 */
	private static ArrayList<AppUser> allUsers = new ArrayList<>();
	
	/**
	 * Eigenschaften der Klasse
	 */
	int hotelID;
	String eigentuemer;
	String username;
	String email;
	String pwd;
	String isHotelbesitzer;
	Role role;
	

	/**
	 * Konstruktor für die Klasse AppUser
	 * @param hotelID
	 * @param eigentuemer
	 * @param email
	 * @param pwd
	 * @param isHotelbesitzer
	 */
	public AppUser(int hotelID, String eigentuemer, String email, String pwd, String isHotelbesitzer) {
		
		this.hotelID = hotelID;
		this.eigentuemer = eigentuemer;
		this.username = "user" + this.hotelID;
		this.email = email;
		this.pwd = pwd;
		
		checkIsHotelbesitzer(isHotelbesitzer);
		isAccountNonLocked();
		
		allUsers.add(this);
	}


	public AppUser(int hotelID, String eigentuemer, String username, String email, String pwd, String isHotelbesitzer) {
	
		this.hotelID = hotelID;
		this.eigentuemer = eigentuemer;
		this.username = username;
		this.email = email;
		this.pwd = pwd;
		this.isHotelbesitzer = isHotelbesitzer;
	}




	@Override
	public String toString() {
		return "AppUser [hotelID=" + hotelID + ", eigentuemer=" + eigentuemer + ", username=" + username + ", email="
				+ email + ", pwd=" + pwd + ", isHotelbesitzer=" + isHotelbesitzer + "]";
	}



	public int getHotelID() {
		return hotelID;
	}

	public String getEigentuemer() {
		return eigentuemer;
	}

	public void setEigentuemer(String eigentuemer) {
		this.eigentuemer = eigentuemer;
	}

	public String getIsHotelbesitzer() {
		return isHotelbesitzer;
	}

	public void checkIsHotelbesitzer(String isHotelbesitzer) {
		
		if(isHotelbesitzer.equals("0")) {
			
			this.isHotelbesitzer = isHotelbesitzer;
			this.role = Role.ADMIN;
			
		}else if (isHotelbesitzer.equals("1")){
			
			this.isHotelbesitzer = isHotelbesitzer;
			this.role = Role.USER;
			
		}else {
			
			throw new IllegalArgumentException("Eingabe ist nicht gültig, es darf nur '0' oder '1' übergeben werden");
			
		}
		
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	

	public void checkUser() {
		
	}
	

	/*
	 * Hier wird überpürft ob ein vollständiger Name bestehend aus Vornamen und Nachnamen
	 * eingegeben wurde
	 */
	
	public void checkName(String fullName) {
		
		BackendUtils.checkStringNotEmpty(fullName, "Der Name ist leer");
		
	}


	public void setPwd(String pwd) {
			
			this.pwd = pwd;
		
	}

	public String getPwd() {
		return pwd;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.<GrantedAuthority>singletonList(new SimpleGrantedAuthority(role.getClass().getName()));
	}


	@Override
	public String getPassword() {
		return pwd;
	}


	@Override
	public boolean isAccountNonExpired() {
		return true;
	}


	@Override
	public boolean isAccountNonLocked() {
		return true;
	}


	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}


	@Override
	public boolean isEnabled() {
		return false;
	}


	
	
	
	
	
}
