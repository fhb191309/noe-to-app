package com.noe.to.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The entry point of the Spring Boot application
 */


@SpringBootApplication 
@EnableAutoConfiguration(exclude = ErrorMvcAutoConfiguration.class)
// @EnableScheduling
public class NoeToAppApplication extends SpringBootServletInitializer{


	public static void main(String[] args) {
	
		SpringApplication.run(NoeToAppApplication.class, args);
		
	}
	

} 
